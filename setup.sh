#!/bin/bash

# $REPOROOT always contains the full path (except the basename), whereas
# $0 is only the relative path of the script from wherever it was called
REPOROOT="$(CDPATH= cd -- "$(dirname -- "$0")" && pwd)"

# Notify user that the following functions take a little while
echo -n "Gathering system information. Please wait..."

# Import function definitions
. "$REPOROOT/util/init.sh"   # Global vars
initialize "$@"              # Load platform-agnostic variables
. "$REPOROOT/cfg/cfg.sh"     # Global presets
load_platform_cfg $PLATFORM  # Load platform-specific variables
. "$REPOROOT/cfg/echo.sh"    # Configure output preferences
load_echo_cfg                # Stdout verbosity
. "$REPOROOT/util/echo.sh"   # Beautify output
init_echo_prepends           # Prepend categories in output
. "$REPOROOT/util/vm.sh"     # VM creation helper functions
. "$REPOROOT/util/apt.sh"    # Apt configurations
. "$REPOROOT/util/device.sh" # Device operations
. "$REPOROOT/util/fs.sh"     # Filesystem operations
. "$REPOROOT/util/net.sh"    # Networking operations
. "$REPOROOT/util/pkg.sh"    # Read cfg/pkg.csv
. "$REPOROOT/cfg/vm.sh"      # virt-install VM instantiations
. "$REPOROOT/cfg/device.sh"  # Platform-specific configurations

# Performed prior to user input (NO UNPROMPTED SYSTEM MODIFICATIONS)
load_preset                # load configuration profiles
net_survey                 # determine network access
dev_check_want_mounted     # check if each device is intended to be mounted
dev_check_nfs_wanted       # test if any NFS shares are referenced

if ! pkg_installed nfs-common && $NFS_REQUESTED; then
    echo_delete_line # clear the "please wait" line
    echo     "NAS share mount request detected."
    echo     "Filesystem modifications required to display accurate info:"
    if ! pkg_installed nfs-common; then
        echo "  - install nfs-common"
    fi
    press_enter_to_continue "Press enter to continue configuration or Ctrl+C to exit."
    echo     "Continuing."

    if [ -d "$REPOROOT/deps/$ARCH/nfs-common" ]; then
        # TODO: Otherwise get "permission denied" errors...?
        cp -rp "$REPOROOT/deps/$ARCH/nfs-common" /tmp
        pkg="/tmp/nfs-common/*"
    else
        task_update
        pkg=nfs-common
    fi
    # Don't run task_install because we don't want to run postinst script
    sudo apt-get -y install $pkg > /dev/null 2>&1
    if ! pkg_installed nfs-common; then
        echo_failure -e "$_INSTALL Unable to install nfs-common; unable to perform requested operations. Exiting..."
        exit
    fi

    unset pkg
    rm -rf /tmp/nfs-common

    echo ""
fi

dev_check_nbd_wanted       # test if any virtual filesystems are referenced

if ! pkg_installed qemu-utils || ! nbd_is_enabled && $NBD_REQUESTED; then
    echo     "Virtual filesystem request detected."
    echo     "Filesystem modifications required to display accurate info:"
    if ! pkg_installed qemu-utils; then
        echo "  - install qemu-utils"
    fi
    if ! nbd_is_enabled; then
        echo     "  - enable nbd kernel module"
    fi
    press_enter_to_continue "Press enter to continue configuration or Ctrl+C to exit."
    echo     "Continuing."

    if ! pkg_installed qemu-utils; then
        # Require qemu-nbd (included in qemu-utils package)
        if [ -d "$REPOROOT/deps/$ARCH/qemu-utils" ]; then
            # TODO: Otherwise get "permission denied" errors...?
            cp -rp "$REPOROOT/deps/$ARCH/qemu-utils" /tmp
            pkg="/tmp/qemu-utils/*"
        else
            task_update
            pkg=qemu-utils
        fi
        sudo apt-get -y install $pkg > /dev/null 2>&1
        if ! pkg_installed qemu-utils; then
            echo_failure -e "$_INSTALL Unable to install qemu-utils; unable to perform requested operations. Exiting..."
            unset pkg
            exit
        fi
        unset pkg
        rm -rf /tmp/qemu-utils
    fi

    nbd_enable
    echo ""
fi
dev_connect_if_nbd

net_load_cfg               # read input args and assign to networking parameters
dev_check_paths            # get device paths from labels
check_exists GAMES         # set GAMES_EXISTS
check_exists HOME          # set HOME_EXISTS
check_exists REPO          # set REPO_EXISTS
check_exists STEAM_LUN     # set STEAM_LUN_EXISTS
check_exists VM            # set VM_EXISTS
dev_check_mounted          # check that each of the devices are mounted
dev_check_properly_mounted # check if each device is properly mounted
echo_delete_line           # clear the "please wait" line
print_menu                 # print user menu

press_enter_to_continue "Press enter to continue configuration or Ctrl+C to exit."
echo    "Continuing."

# Performed after consent of user

dev_mount                  # mount or re-mount devices at intended paths
check_exists GAMES         # set GAMES_EXISTS
check_exists HOME          # set HOME_EXISTS
check_exists REPO          # set REPO_EXISTS
check_exists STEAM_LUN     # set STEAM_LUN_EXISTS
check_exists VM            # set VM_EXISTS
dev_check_mounted          # check that each of the devices are mounted
dev_check_properly_mounted # update proper mount flags since devices are mounted

if $SW_INSTALL && $INTERNET_CONNECTED; then
    # Synchronize date/time before updating; expired Release files won't update
    task_install $(get_pkglist_from_csv --action=install \
                                        --arch=$ARCH \
                                        --category=time \
                                        --platform=$PLATFORM)
else
    echo_blank -n "$_CONFIG Setting system time based on RTC..."
    sudo hwclock --hctosys
    echo_success -e "$_CONFIG Set system time based on RTC"
fi

if $SW_UPDATE; then
    if $INTERNET_CONNECTED || ( $REPO_WANTMOUNT && $REPO_PROPERMOUNT ); then
        if $SW_INSTALL_KEYS; then
            task_apt_sources # get signing keys and set up mirrors
        fi
        task_update # run apt-get autoclean/update
    else
        echo_failure "$_UPDATE Failed to update software... currently offline"
    fi
else
    echo_skip "$_UPDATE Software not updated... feature not selected"
fi
task_bluetooth
wlan_connect

# TODO: Unsure of where to place this
#network_is_metered   # check if the active SSID is metered
if $SET_METERING; then
    set_network_metering # set metered flag based on active SSID
fi
if [ "$INTERNET_CONNECTION_METHOD" = "ethernet" ]; then
    disable_wireless_radio
fi

if $ADD_HOSTS; then
    add_hosts_from_csv # add entries from cfg/net.csv to /etc/hosts
else
    echo_skip "$_CONFIG /etc/hosts unchanged... feature not selected"
fi

if $PERSISTENT_HOME; then
    task_persistent_home
else
    echo_skip "$_CONFIG Persistent home not established... feature not selected"
fi

disable_automatic_update # Prevent background service from locking apt

if $SW_INSTALL; then
    # TODO: If you do this, you need to separate install/configuration
    #if $INTERNET_CONNECTED || ( $REPO_WANTMOUNT && $REPO_PROPERMOUNT ); then
        if $SW_INSTALL_MIRROR; then
            task_install apt-mirror
        else
            echo_skip "$_INSTALL apt-mirror not installed... feature not selected"
        fi

        # Install CLI+GUI base software
        task_install $(get_pkglist_from_csv --action=install \
                                            --arch=$ARCH \
                                            --category=base \
                                            --platform=$PLATFORM)

        if $SW_INSTALL_DESIGN; then
            task_install $(get_pkglist_from_csv --action=install \
                                                --arch=$ARCH \
                                                --category=design \
                                                --platform=$PLATFORM)
        else
            echo_skip "$_INSTALL Software (design) not installed... feature not selected"
        fi

        if $SW_INSTALL_OFFICE; then
            task_install $(get_pkglist_from_csv --action=install \
                                                --arch=$ARCH \
                                                --category=office \
                                                --platform=$PLATFORM)
        else
            echo_skip "$_INSTALL Software (office) not installed... feature not selected"
        fi

        if $SW_INSTALL_DEV; then
            task_install $(get_pkglist_from_csv --action=install \
                                                --arch=$ARCH \
                                                --category=development \
                                                --platform=$PLATFORM)
        else
            echo_skip "$_INSTALL Software (dev) not installed... feature not selected"
        fi

        if $SW_INSTALL_DOCKER; then
            echo_blank -n "$_INSTALL Installing \"docker.io\"..."
            task_install docker.io
            echo_success -e "$_INSTALL Installed \"docker.io\""
        else
            echo_skip "$_INSTALL Software (docker) not installed... feature not selected"
        fi

        if $SW_INSTALL_GAMES; then
            if $GAMES_PROPERMOUNT; then
                echo task_install $(get_pkglist_from_csv --action=install \
                                                         --arch=$ARCH \
                                                         --category=games \
                                                         --platform=$PLATFORM)
            else
                echo_failure "$_INSTALL Software (games) not installed... partition not mounted"
            fi
        else
            echo_skip "$_INSTALL Software (games) not installed... feature not selected"
        fi

        if $SW_INSTALL_MEDIA; then
            task_install $(get_pkglist_from_csv --action=install \
                                                --arch=$ARCH \
                                                --category=media \
                                                --platform=$PLATFORM)
        else
            echo_skip "$_INSTALL Software (media) not installed... feature not selected"
        fi

        if $SW_INSTALL_SOCIAL; then
            task_install $(get_pkglist_from_csv --action=install \
                                                --arch=$ARCH \
                                                --category=social \
                                                --platform=$PLATFORM)
        else
            echo_skip "$_INSTALL Software (social) not installed... feature not selected"
        fi

        if $SW_INSTALL_STEAM; then
            if $GAMES_PROPERMOUNT; then
                if $INTERNET_CONNECTED; then
                    task_install steam
                else
                    echo_failure "$_INSTALL Software (steam) not installed... no internet connection"
                fi
            else
                echo_failure "$_INSTALL Software (steam) not installed... partition not mounted"
            fi
        else
            echo_skip "$_INSTALL Software (steam) not installed... feature not selected"
        fi

        if $SW_INSTALL_THINCLIENT; then
            task_install $(get_pkglist_from_csv --action=install \
                                                --arch=$ARCH \
                                                --category=lx \
                                                --platform=$PLATFORM)
        else
            echo_skip "$_INSTALL Software (thin client) not installed... feature not selected"
        fi

        if $SW_INSTALL_VM; then
            if $VM_HEADLESS; then
                task_install $(get_pkglist_from_csv --action=install \
                                                    --arch=$ARCH \
                                                    --category=vm \
                                                    --platform=server)
            else
                task_install $(get_pkglist_from_csv --action=install \
                                                    --arch=$ARCH \
                                                    --category=vm \
                                                    --platform=$PLATFORM)
            fi
            if [ "$PRESET" = "server" ]; then
                vm_init     # Start libvirtd service and network
                link_all_vm # Traverse VM list; link VMs with existing storage files
            fi
        else
            echo_skip "$_INSTALL Software (VM) not installed... VM installation not selected"
        fi
    #else
    #    echo_failure "$_INSTALL Failed to install software... currently offline"
    #fi
else
    echo_skip "$_INSTALL No software installed... feature not selected"
fi

if $SW_PURGE; then
    task_uninstall $(get_pkglist_from_csv --action=uninstall \
                                          --arch=$ARCH \
                                          --exclcategory=games \
                                          --platform=$PLATFORM)
    if ! $SW_INSTALL_GAMES; then
        task_uninstall $(get_pkglist_from_csv --action=uninstall \
                                              --arch=$ARCH \
                                              --category=games \
                                              --platform=$PLATFORM)
    fi
else
    echo_skip "$_UNINSTALL Software not uninstalled... feature not selected"
fi

if $SW_UPGRADE; then
    if $INTERNET_CONNECTED || ( $REPO_WANTMOUNT && $REPO_PROPERMOUNT ); then
        #task_upgrade      # normal upgrade procedure
        task_dist_upgrade # upgrade packages normally kept back
    else
        echo_failure "$_UPDATE Failed to upgrade software... currently offline"
    fi
else
    echo_skip "$_UPGRADE Software not upgraded... feature not selected"
fi
enable_automatic_update # Resume background service to enable automated updates

# TODO: Find a more elegant place for this
if [ "$PRESET" = "server" ]; then
    # TODO: Reduce screen brightness (no $DISPLAY since it's a terminal)
    create_custom_command blankscreen

    # In case a laptop is being used as a server
    lid_close_suspend_disable
fi

if $PERSONALIZE; then
    . "$REPOROOT/cfg/gnome.sh" # Configure GNOME desktop preferences
    apply_generic_customizations
    apply_gnome_customizations
else
    echo_skip "$_CONFIG No GNOME customizations applied... feature not selected"
fi

if $CREATE_SWAPFILE; then
    create_swapfile "$SWAPFILEPATH" $SWAPFILESIZE
    enable_swapfile "$SWAPFILEPATH"
else
    echo_skip "$_CONFIG Skipping swapfile... feature not selected"
fi
