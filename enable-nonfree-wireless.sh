#!/bin/bash

# This script is intended to discover, install, and configure proprietary
# wireless device drivers on operating systems that do not include proprietary
# firmware.

# Must be root
if [ $(id -u) -ne 0 ]; then
    echo "Please run as root."
    exit 1
fi

# Configuration
firmwaredir=/media/$USER/home/debian-live/deps/amd64 # Where to place the firmware .deb files

# Download a package and its dependencies; move them to a given destination
# Usage: apt_download <pkg-name> <target-destination>
apt_download() {
    sudo apt-get clean
    sudo apt-get update
    sudo apt-get --download-only --yes reinstall $1
    sudo mkdir -p $2/$1
    sudo mv /var/cache/apt/archives/*.deb $2/$1/
}

# Disable Debian non-free package repositories
nonfree_disable() {
    sed -i '$ d' /etc/apt/sources.list
    apt-get clean
    apt-get update
}

# Enable Debian non-free package repositories
nonfree_enable() {
    echo "deb http://deb.debian.org/debian bullseye main contrib non-free" >> /etc/apt/sources.list
    sudo apt-get clean
    sudo apt-get update
}

# Temporarily add the Debian 11 non-free repo
#nonfree_enable

# Find which driver packages would be used by the hardware

# TODO: Automatically discover Debian firmware package dependencies for a particular wifi kernel module
# Download package dependencies for offline use
# apt_download firmware-ath9k-htc $firmwaredir
# apt_download firmware-atheros $firmwaredir

# Remove the Debian 11 non-free repo
#nonfree_disable

# Install the firmware packages
find $firmwaredir -type f -name "firmware-*.deb" -exec apt-get install {} \;

# Discover the proprietary wireless device kernel module
kmodule=$(find /sys | grep drivers.*$(lspci | grep -i network | cut -d' ' -f1) | cut -d'/' -f6)

# Reload the kernel module
modprobe -r $kmodule
modprobe $kmodule
