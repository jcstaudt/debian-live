# debian-live

### Overview

The purpose of this project is to run an official/vanilla Debian live install environment and configure it such that it is suitable for everyday use.
These tools customize an immutable Operating System (OS) with optional filesystem persistence and numerous quality-of-life improvements.

Boot the OS, run the script, and you're done.
As these tools grow to support additional OS and desktop environments, it becomes easier to run a persistent computing environment that is both (relatively) hardware-agnostic and OS-agnostic.

Use this repository as a starting point and customize it to suit your needs.

### Features

The following is a general summary and not an exhaustive list of features:

- Configuration presets for desktop/laptop and server environments
- Mount devices in configured locations, re-mounting when necessary
- Virtual filesystem support (qcow2)
- Configure persistent storage if configured and available
- Configure upstream software repositories; will also use a local apt-mirror if available
- Enable playback of proprietary DVDs (when media package is selected)
- Install and remove software based on configurable package lists
- Organize GNOME application icons into folders
- Configure existing VMs for use with `virt-manager` (or headless if using a server as a host device)
- Create and mount an optional swapfile

### Recommended Usage

1. Write a "live" system image of a supported Linux distribution (see supported environments below) to your boot drive (should be approx 1-4 GiB)
    - **Disclaimer**: Use caution and seek assistance if necessary to do this; the authors take no responsibility for lost data
    - Recommendation: use a small-capacity, inexpensive flash drive and only copy the live system image to it (for cost and simplicity)
    - Internal or external storage media works fine
2. Copy the contents of this repository onto a separate primary ext4 partition
3. Adjust configuration files to suit your needs:
    - See `./cfg/README.sh` for a description of what each configuration file does
4. Execute the main script `./setup.sh`

Running this script within an unconfigured live Debian (or derivative) environment without any customization whatsoever would look like this:
```bash
sudo apt-get install -y git > /dev/null 2>&1 && \
git clone https://gitlab.com/jcstaudt/debian-live.git /tmp/debian-live > /dev/null 2>&1 && \
/tmp/debian-live/setup.sh
```

### System requirements

- Host x86_64 computer (to run natively or within a Virtual Machine)
- 8GB+ RAM recommended
- Debian Live system image
    - may be booted from external media (normal use) or natively on the host computer (by copying the rootfs partition to disk and booting to it)
    - Debian 10 and 11, GNOME desktop
    - Debian 10 and 11, standard (no graphical environment)

