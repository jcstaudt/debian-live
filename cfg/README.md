# Configuration

Freely modify variables and settings in this directory to configure the environment to suit your needs.

### Directory Listing

| FILE      | DESCRIPTION                                                         |
|:---------:|:-------------------------------------------------------------------:|
| cfg.sh    | High-level configuration                                            |
| device.sh | Implementation-specific device discovery                            |
| echo.sh   | Script output preferences                                           |
| gnome.sh  | GNOME desktop environment customizations                            |
| net.csv   | User-specific network configuration database (populates /etc/hosts) |
| pkg.csv   | Package configuration                                               |
| README.md | This README                                                         |
| vm.sh     | Virtual Machine profiles to link existing virtual disk images       |
| preinst/  | Scripts to configure the environment before a package is installed  |
| postinst/ | Scripts to configure a package following its installation           |
| postrm/   | Scripts to remove residual package artifacts after uninstallation   |

### net.csv Example

The CSV header describes the elements required to populate `/etc/hosts`.
Population of this file is only performed if the `$ADD_HOSTS` configuration variable is set to `true`.

At the time of this writing, this is the CSV header:
```
hostnames,platform,ipv4,mac,comment
```

If not all fields are used, each line must still be padded with spaces.

| Field    | Importance | Description                            |
|:--------:|:----------:|:--------------------------------------:|
| hostname | Required   | may include multiple hostnames         |
| platform | Optional   | enables multi-platform support         |
| ipv4     | Required   | links an IP address to a hostname      |
| mac      | Optional   | only to make the file more informative |
| comment  | Optional   | only to make the file more informative |

Here are a few examples of valid entries:
```
router,,192.168.1.1,,
router router.local,server,192.168.1.1,00:01:02:03:04:05,
router router.local,,192.168.1.1,00:01:02:03:04:05,Home router: ethernet port 1
nas,desktop laptop,192.168.1.2,11:22:33:44:55:66,Network Attached Storage
```
