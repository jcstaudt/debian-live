changed=false
if [ -f $HOME/.config/keepassx/keepassx2.ini ]; then
    rm $HOME/.config/keepassx/keepassx2.ini
    changed=true
fi
if [ -d $HOME/.config/keepassx ]; then
    rmdir --ignore-fail-on-non-empty $HOME/.config/keepassx 2> /dev/null
    changed=true
fi
if [ ! -d $HOME/.config/keepassx ] && $changed; then return $SUCCESS; else return $FAILURE; fi