unalias rdesktop 2> /dev/null
return $SUCCESS

changed=false
if [ $(alias rdesktop 2> /dev/null | wc -l) -eq 0 ]; then
    unalias rdesktop
    changed=true
fi
if $changed; then return $SUCCESS; else return $FAILURE; fi