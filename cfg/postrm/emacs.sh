changed=false
if [ -f $HOME/.emacs ]; then
    rm $HOME/.emacs
    changed=true
fi
if [ ! -f $HOME/.emacs ] && $changed; then return $SUCCESS; else return $FAILURE; fi