changed=false
if [ -f $HOME/.config/yelp/yelp.cfg ]; then
    rm $HOME/.config/yelp/yelp.cfg
    changed=true
fi
if [ -d $HOME/.config/yelp ]; then
    rmdir --ignore-fail-on-non-empty $HOME/.config/yelp 2> /dev/null
    changed=true
fi
if [ ! -d $HOME/.config/yelp ] && $changed; then return $SUCCESS; else return $FAILURE; fi