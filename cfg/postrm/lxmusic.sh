changed=false
if [ -f $HOME/.config/lxmusic/config ]; then
    rm $HOME/.config/lxmusic/config
    changed=true
fi
if [ -d $HOME/.config/lxmusic ]; then
    rmdir --ignore-fail-on-non-empty $HOME/.config/lxmusic 2> /dev/null
    changed=true
fi
if [ ! -d $HOME/.config/lxmusic ] && $changed; then return $SUCCESS; else return $FAILURE; fi