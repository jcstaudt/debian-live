changed=false
if [ $(alias battery 2> /dev/null | wc -l) -eq 0 ]; then
    unalias battery
    changed=true
fi
if $changed; then return $SUCCESS; else return $FAILURE; fi