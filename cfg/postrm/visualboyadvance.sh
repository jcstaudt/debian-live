changed=false
if [ -f $HOME/.VisualBoyAdvance.cfg ]; then
    rm $HOME/.VisualBoyAdvance.cfg
    changed=true
fi
if [ ! -f $HOME/.VisualBoyAdvance.cfg ] && $changed; then return $SUCCESS; else return $FAILURE; fi