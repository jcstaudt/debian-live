changed=false
if [ -f $HOME/.config/transmission/settings.json ]; then
    rm $HOME/.config/transmission/settings.json
    changed=true
fi
if [ -d $HOME/.config/transmission ]; then
    rmdir --ignore-fail-on-non-empty $HOME/.config/transmission 2> /dev/null
    changed=true
fi
if [ ! -d $HOME/.config/transmission ] && $changed; then return $SUCCESS; else return $FAILURE; fi