changed=false
if [ -f $HOME/.vimrc ]; then
    rm $HOME/.vimrc
    changed=true
fi
if [ ! -f $HOME/.vimrc ] && $changed; then return $SUCCESS; else return $FAILURE; fi