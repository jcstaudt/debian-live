changed=false
if [ -f $HOME/.config/octave/octave-gui.ini ]; then
    rm $HOME/.config/octave/octave-gui.ini
    changed=true
fi
if [ -d $HOME/.config/octave ]; then
    rmdir --ignore-fail-on-non-empty $HOME/.config/octave 2> /dev/null
    changed=true
fi
if [ ! -d $HOME/.config/octave ] && $changed; then return $SUCCESS; else return $FAILURE; fi