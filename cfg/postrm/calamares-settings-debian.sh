changed=false
if [ -f $HOME/Desktop/install-debian.desktop ]; then
    rm $HOME/Desktop/install-debian.desktop
    changed=true
fi
if [ ! -f $HOME/Desktop/install-debian.desktop ] && $changed; then return $SUCCESS; else return $FAILURE; fi