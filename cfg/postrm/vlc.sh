changed=false
if [ -f $HOME/.config/mimeapps.list ]; then
    rm $HOME/.config/mimeapps.list
    changed=true
fi
if [ ! -f $HOME/.config/mimeapps.list ] && $changed; then return $SUCCESS; else return $FAILURE; fi