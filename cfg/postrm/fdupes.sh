changed=false
if [ $(alias fdupes 2> /dev/null | wc -l) -eq 0 ]; then
    unalias fdupes
    changed=true
fi
if $changed; then return $SUCCESS; else return $FAILURE; fi