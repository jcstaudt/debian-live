changed=false
if [ -f $HOME/.config/mozc ]; then
    rm $HOME/.config/mozc
    changed=true
fi
if [ -f $HOME/.config/ibus-mozc-gnome-initial-setup-done ]; then
    rm $HOME/.config/ibus-mozc-gnome-initial-setup-done
    changed=true
fi
if [ ! -f $HOME/.config/mozc -a ! -f $HOME/.config/ibus-mozc-gnome-initial-setup-done ] && $changed; then return $SUCCESS; else return $FAILURE; fi