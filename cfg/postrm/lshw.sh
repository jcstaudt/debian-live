changed=false
if [ $(alias netinfo 2> /dev/null | wc -l) -eq 0 ]; then
    unalias netinfo
    changed=true
fi
if $changed; then return $SUCCESS; else return $FAILURE; fi