changed=false
if [ -f $HOME/.config/youtube-dl/config ]; then
    rm $HOME/.config/youtube-dl/config
    changed=true
fi
if [ -d $HOME/.config/youtube-dl ]; then
    rmdir --ignore-fail-on-non-empty $HOME/.config/youtube-dl 2> /dev/null
    changed=true
fi
if [ ! -d $HOME/.config/youtube-dl ] && $changed; then return $SUCCESS; else return $FAILURE; fi