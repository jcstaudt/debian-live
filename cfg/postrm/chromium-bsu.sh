changed=false
if [ -f $HOME/.chromium-bsu ]; then
    rm $HOME/.chromium-bsu
    changed=true
fi
if [ ! -f $HOME/.chromium-bsu ] && $changed; then return $SUCCESS; else return $FAILURE; fi