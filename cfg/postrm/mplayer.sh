changed=false
if [ $(alias asciivid 2> /dev/null | wc -l) -eq 0 ]; then
    unalias asciivid
    changed=true
fi
if $changed; then return $SUCCESS; else return $FAILURE; fi