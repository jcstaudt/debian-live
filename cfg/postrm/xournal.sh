changed=false
if [ -f $HOME/.xournal/config ]; then
    rm $HOME/.xournal/config
    changed=true
fi
if [ -d $HOME/.xournal ]; then
    rmdir --ignore-fail-on-non-empty $HOME/.xournal 2> /dev/null
    changed=true
fi
if [ ! -d $HOME/.xournal ] && $changed; then return $SUCCESS; else return $FAILURE; fi