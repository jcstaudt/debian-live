changed=false
if [ -f $HOME/.wgetrc ]; then
    rm $HOME/.wgetrc
    changed=true
fi
if [ ! -f $HOME/.wgetrc ] && $changed; then return $SUCCESS; else return $FAILURE; fi