changed=false
if [ -f $HOME/.abcde.conf ]; then
    rm $HOME/.abcde.conf
    changed=true
fi
if [ ! -f $HOME/.abcde.conf ] && $changed; then return $SUCCESS; else return $FAILURE; fi