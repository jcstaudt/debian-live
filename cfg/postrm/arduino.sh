changed=false
if [ -f $HOME/.arduino/preferences.txt ]; then
    rm $HOME/.arduino/preferences.txt
    changed=true
fi
if [ -d $HOME/.arduino ]; then
    rmdir --ignore-fail-on-non-empty $HOME/.arduino 2> /dev/null
    changed=true
fi
if [ ! -d $HOME/.arduino ] && $changed; then return $SUCCESS; else return $FAILURE; fi