# This script ONLY sets values. Please do not include any real logic in here.

# Description: Load which software repositories to read, based on distro
# Usage:       load_components
# Depends:     util/init.sh - print_distro_id, print_distro_release
load_components() {
    case $(print_distro_id) in
        "Debian")
            if [ $(print_distro_release) -ge 12 ]; then
                echo "main contrib non-free non-free-firmware"
            else
                echo "main contrib non-free"
            fi
            ;;
        "PureOS")
            echo "main"
            ;;
        *)
            echo "main"
            ;;
    esac
}

# Description: Sets values based on a target environment w/ opt platform override
# Usage:       load_platform_cfg <opt:desktop|laptop|server>
# Depends:     util/init.sh - initialize
load_platform_cfg() {

    if [ $# -eq 1 ]; then
        PLATFORM=$1
    fi

    ################################################################################
    # User configuration

    case $PLATFORM in
        "desktop"|"laptop")
            PRESET=basic

            IP_NAS_ETH=nas            # IP address or local hostname
            NAS_ROOT=/mnt/$IP_NAS_ETH
            NAS_SHARE_ROOT=/export    # root directory of NAS shares
                                      # ex: '/data' on NETGEAR, '/export' on OMV

            NTP_SERVER=time.nist.gov  # synchronize date/time from this server

            ###################################################################
            # Home partition

            # no need to modify for typical use
            HOME_NBD=/dev/nbd0

            HOME_PARTITION_LABEL="home" # if repo exists on a local partition (ex: "home")
            # TODO: What if /media/user/home already exists and device mounted at /media/user/home1?
            HOME_MNT_PATH=/media/$USER/$HOME_PARTITION_LABEL
            HOME_NAS_SHARE=""           # if repo exists on a NAS share (ex: "Home")
            HOME_VFS_FN=""              # virtual filesystem filename, relative to either root dir of local partition if HOME_PARTITION_LABEL provided, or NAS share if HOME_NAS_SHARE provided

            ###################################################################
            # Local mirror (using apt-mirror)

            # no need to modify for typical use
            REPO_MNT_PATH=/var/spool/apt-mirror # apt-mirror mount destination
            REPO_NBD=/dev/nbd1                  # reserved block device path

            # leave variables empty if not wanted
            REPO_PARTITION_LABEL="repo" # if repo exists on a local partition (ex: "repo")
            REPO_NAS_SHARE=""           # if repo exists on a NAS share (ex: "Disks")
            REPO_VFS_FN=""              # virtual filesystem filename, relative to either root dir of local partition if REPO_PARTITION_LABEL provided, or NAS share if REPO_NAS_SHARE provided (ex: "repo.qcow2")

            ###################################################################
            # Game libraries

            # no need to modify for typical use
            GAMES_NBD=/dev/nbd2

            GAMES_PARTITION_LABEL="Games" # if repo exists on a local partition (ex: "Games")
            GAMES_MNT_PATH=/media/$USER/$GAMES_PARTITION_LABEL
            GAMES_NAS_SHARE=""            # if repo exists on a NAS share (ex: "Games")
            GAMES_VFS_FN=""               # virtual filesystem filename, relative to either root dir of local partition if GAMES_PARTITION_LABEL provided, or NAS share if GAMES_NAS_SHARE provided

            # path to a game save repository, if applicable
            PC_GAMESAVE_MNT_PATH="" #"$GAMES_MNT_PATH/video-game-saves/save/pc"

            # steam games on the NAS are expected to exist here
            STEAM_LUN_PARTITION_LABEL=Steam
            STEAM_LUN_MNT_PATH=$NAS_ROOT/${STEAM_LUN_PARTITION_LABEL,,}
            # A virtual filesystem containing a network LUN doesn't make sense, so leaving out

            ###################################################################
            # Virtual Machines (QCOW2 image files are expected to exist here)

            VM_PARTITION_LABEL="VM" # if repo exists on a local partition (ex: "VM")
            VM_MNT_PATH=/var/lib/libvirt/images
            VM_NAS_SHARE=""  # if repo exists on a NAS share (ex: "VM")
            # A virtual filesystem containing VM images doesn't make sense, so leaving out

            ###################################################################
            # WayDroid

            WAYDROID_ROOT="$HOME_MNT_PATH/waydroid"
            WAYDROID_VAR="$WAYDROID_ROOT/var/lib/waydroid" #"$VM_MNT_PATH/waydroid"
            WAYDROID_LOCAL="$WAYDROID_ROOT/.local/share/waydroid"

            ###################################################################
            # Swap

            SWAPFILEPATH=$VM_MNT_PATH/swap.file
            SWAPFILESIZE=32 # GB

            ###################################################################
            # Personalizations

            BRIGHTNESS=0.35
            DESKTOP_BACKGROUND_THEME="none"
            DESKTOP_BACKGROUND_PRIMARY="#000000"
            DESKTOP_BACKGROUND_SECONDARY="#000000"
            DISPLAY_RES="1920 1080 60" # TODO: Unused
            DOMAIN=$(hostname)
            EMAIL="$USER@$DOMAIN"
            GPG_EMAIL=$EMAIL
            NAME="My Name"
            NEXTCLOUD_DOMAIN=$DOMAIN
            NEXTCLOUD_USER="$USER"
            NIGHT_LIGHT=true
            OSM_USERNAME="MyUsername"
            THEME="Adwaita-dark" # For list: ls -d /usr/share/themes/* | xargs -L 1 basename
            TIME_ZONE="America/Los_Angeles" # timedatectl list-timezones
            ANDROID_PHONE_NAME="phone" # name of Android phone to attach, if applicable

            ###################################################################
            # Networking

            WPA_SUPPLICANT_CFG="/etc/wpa_supplicant/wpa_supplicant.conf" # Where to place the wpa_supplicant cfg file
            ;;
        "server")
            PRESET=server

            IP_NAS_ETH=nas            # IP address or local hostname
            NAS_ROOT=/mnt/$IP_NAS_ETH
            NAS_SHARE_ROOT=/export    # root directory of NAS shares
                                      # ex: '/data' on NETGEAR, '/export' on OMV

            NTP_SERVER=time.nist.gov  # synchronize date/time from this server

            STEAM_LUN_PARTITION_LABEL=iSCSI
            STEAM_LUN_MNT_PATH=$NAS_ROOT/${STEAM_LUN_PARTITION_LABEL,,}

            VM_PARTITION_LABEL="VM" # if images exist on a local partition (ex: "VM")
            VM_MNT_PATH=/var/lib/libvirt/images
            VM_NAS_SHARE=""         # if images exist on a NAS share (ex: "VM")

            SWAPFILEPATH=$VM_MNT_PATH/swap.file
            SWAPFILESIZE=32 #GB
            ;;
        *)
            echo "FAILURE: Unsupported platform '$PLATFORM'"
            exit
            ;;
    esac
}

# Depends: . - load_platform_cfg
load_preset() {
    # Defaults
    SUITE="stable"                # High-level system config e.g. stable|testing
    COMPONENTS=$(load_components) # Software repositories from which to pull
    ADD_HOSTS=false               # add hosts to /etc/hosts
    CREATE_SWAPFILE=false         # create a swapfile
    NAS_WANTMOUNT=false           # mount a NAS
    SET_METERING=false            # set metered network flag
    SW_UPDATE=false               # needed to install software locally
    SW_INSTALL=false              # needed to install *any* software
    SW_INSTALL_DESIGN=false       # install design software
    SW_INSTALL_DEV=false          # install development software
    SW_INSTALL_DOCKER=false       # install docker.io
    SW_INSTALL_KEYS=false         # install extra software repos/keys
    SW_INSTALL_GAMES=false        # install game software
    SW_INSTALL_MEDIA=false        # install audio/graphic/video tools
    SW_INSTALL_MIRROR=false       # install local repository tools
    SW_INSTALL_OFFICE=false       # install office productivity tools
    SW_INSTALL_SOCIAL=false       # install chat applications
    SW_INSTALL_STEAM=false        # install Steam environment
    SW_INSTALL_THINCLIENT=false   # install lightweight thin-client resources
    SW_INSTALL_VM=false           # install VM tools
    SW_INSTALL_WAYDROID=false     # install local Android environment
    SW_PURGE=false                # fully remove installed software
    SW_UPGRADE=false              # run apt-get update
    USE_BLUETOOTH=false           # whether to enable Bluetooth
    PERSISTENT_HOME=false         # symlinks for persistent storage
    PERSONALIZE=false             # customize environment
    VM_AUTOSTART=false            # autostart VMs after linking
    VM_HEADLESS=false             # headless mode for VM server configurations

    # Override defaults
    case $PRESET in
        "basic")
            ADD_HOSTS=true
            NAS_WANTMOUNT=true
            SW_UPDATE=true
            SW_INSTALL=true
            SW_INSTALL_KEYS=true
            SW_INSTALL_OFFICE=true
            SW_PURGE=true
            SW_UPGRADE=true
            PERSISTENT_HOME=true
            PERSONALIZE=true
            ;;
        "minimal")
            PERSISTENT_HOME=true
            PERSONALIZE=true
            ;;
        "nothing")
            ;;
        "everything")
            SUITE="testing"
            ADD_HOSTS=true
            NAS_WANTMOUNT=true
            SET_METERING=true
            SW_UPDATE=true
            SW_INSTALL=true
            SW_INSTALL_DESIGN=true
            SW_INSTALL_DEV=true
            SW_INSTALL_KEYS=true
            SW_INSTALL_GAMES=true
            SW_INSTALL_MEDIA=true
            SW_INSTALL_MIRROR=true
            SW_INSTALL_OFFICE=true
            SW_INSTALL_SOCIAL=true
            SW_INSTALL_STEAM=true
            SW_INSTALL_VM=true
            SW_PURGE=true
            SW_UPGRADE=true
            USE_BLUETOOTH=true
            PERSISTENT_HOME=true
            PERSONALIZE=true
            ;;
        "server")
            ADD_HOSTS=true
            SW_UPDATE=true
            SW_INSTALL=true
            SW_INSTALL_VM=true
            SW_PURGE=true
            SW_UPGRADE=true
            VM_AUTOSTART=true
            VM_HEADLESS=true
            ;;
        "work")
            SW_UPDATE=true
            SW_INSTALL=true
            SW_INSTALL_DEV=true
            SW_INSTALL_KEYS=true
            SW_INSTALL_OFFICE=true
            SW_INSTALL_SOCIAL=true
            SW_PURGE=true
            SW_UPGRADE=true
            USE_BLUETOOTH=true
            PERSISTENT_HOME=true
            PERSONALIZE=true
            ;;
        *)
            echo "FAILURE: Unsupported preset value '$PRESET'"
            exit
            ;;
    esac
}
