# Depends: util/apt.sh - get_signing_key, task_update

get_signing_key \
    "dl-ssl.google.com/linux/linux_signing_key.pub" \
    "linux_signing_key.pub" \
    "http://dl.google.com/linux/chrome/deb" \
    "stable main" \
    "google-chrome.list"

task_update