# Depends: util/apt.sh - get_signing_key, task_update

get_signing_key \
    "packages.microsoft.com/keys/microsoft.asc" \
    "microsoft-edge.gpg" \
    "https://packages.microsoft.com/repos/edge" \
    "stable main" \
    "microsoft-edge.list"

task_update
