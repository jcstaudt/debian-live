# Depends: util/apt.sh - get_signing_key, task_update

get_signing_key \
    "packages.element.io/debian/element-io-archive-keyring.gpg" \
    "element-io-archive-keyring.gpg" \
    "https://packages.element.io/debian/" \
    "default main" \
    "element-io.list"

task_update