# Depends: util/apt.sh - get_signing_key, task_update

(task_install ca-certificates)
(task_install curl)
(task_install openssh-server)
(task_install perl)
(task_install postfix) # Optional: only required if sending emails via GitLab

curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash

