# Depends: util/apt.sh - get_signing_key, task_update

get_signing_key \
    "updates.signal.org/desktop/apt/keys.asc" \
    "signal-desktop-keyring.gpg" \
    "https://updates.signal.org/desktop/apt" \
    "xenial main" \
    "signal-xenial.list"

task_update
