# TODO: Install python3-pyclipper?
# Depends: util/apt.sh - get_signing_key, task_update

get_signing_key \
    "repo.waydro.id/waydroid.gpg" \
    "waydroid.gpg" \
    "https://repo.waydro.id/" \
    "bullseye main" \
    "waydroid.list"

task_update
