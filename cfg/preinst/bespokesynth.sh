# Depends: util/apt.sh - get_signing_key, task_update

get_signing_key \
    "download.opensuse.org/repositories/home:bespokesynth/Debian_12/Release.key" \
    "home_bespokesynth.gpg" \
    "http://download.opensuse.org/repositories/home:/bespokesynth/Debian_12/" \
    "/" \
    "home:bespokesynth.list"

task_update