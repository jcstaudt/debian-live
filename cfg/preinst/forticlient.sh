# Depends: util/apt.sh - get_signing_key, task_update

get_signing_key \
    "repo.fortinet.com/repo/forticlient/7.2/debian/DEB-GPG-KEY" \
    "repo.fortinet.com.gpg" \
    "https://repo.fortinet.com/repo/forticlient/7.2/debian/" \
    "stable non-free" \
    "repo.fortinet.com.list"

task_update