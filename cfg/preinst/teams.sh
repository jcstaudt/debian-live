# Depends: util/apt.sh - get_signing_key, task_update

get_signing_key \
    "packages.microsoft.com/keys/microsoft.asc" \
    "microsoft.gpg" \
    "https://packages.microsoft.com/repos/ms-teams" \
    "stable main" \
    "microsoft-teams.list"

task_update