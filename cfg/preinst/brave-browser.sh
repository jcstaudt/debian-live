# Depends: util/apt.sh - get_signing_key, task_update

get_signing_key \
    "brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg" \
    "brave-browser-archive-keyring.gpg" \
    "http://brave-browser-apt-release.s3.brave.com/" \
    "stable main" \
    "brave-browser-release.list"

task_update