# Depends: util/apt.sh - get_signing_key, task_update

# Spotify (instructions: https://www.spotify.com/us/download/linux)
get_signing_key \
    "download.spotify.com/debian/pubkey_7A3A762FAFD4A51F.gpg" \
    "spotify.gpg" \
    "http://repository.spotify.com" \
    "stable non-free" \
    "spotify.list"

task_update