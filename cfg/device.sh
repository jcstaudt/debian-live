# Depends: cfg/cfg.sh
# Depends: util/apt.sh
# Depends: util/device.sh - get_nas_share_list, resource_path_exists
# Depends: util/fs.sh     - list_length

# Set corresponding variable if the given device exists
# Usage:   check_exists <GAMES|HOME|NAS|REPO|STEAM_LUN|VM> <opt:IP_NAS>
# Depends: .              - dev_check_paths
# Depends: util/device.sh - nas_share_exists, resource_path_exists
check_exists() {
    label=$1_PARTITION_LABEL
    dev=$1_PARTITION_DEV
    nasshare=$1_NAS_SHARE
    virtpath=$1_VFS_PATH
    if [ $# -eq 2 ]; then ipnas=$2; else ipnas=""; fi
    eval $1_EXISTS=false
    if resource_label_exists "${!label}" $ipnas || \
       resource_path_exists "${!dev}" $ipnas || \
       [ -f "${!virtpath}" ]; then
        eval $1_EXISTS=true
    elif [ -z "${!virtpath}" ] && \
         [ -n "${!nasshare}" ] && \
         $(nas_share_exists $IP_NAS_ETH ${!nasshare}); then
        # resource is a NAS share path and NOT a device and NOT a virtual disk
        eval $1_EXISTS=true
    fi
    unset dev
    unset ipnas
    unset label
    unset nasshare
    unset virtpath
}

# Set corresponding variable of virtual filesystems (i.e. qcow2 images)
# Usage:   get_vfs_path <GAMES|HOME|NAS|REPO|STEAM_LUN|VM>
# Depends: cfg/cfg.sh     - NAS_ROOT, *_PARTITION_LABEL, *_NAS_SHARE, *_VFS_FN
# Depends: util/device.sh - get_mount_path_from_label
get_vfs_path() {
    local label=$1_PARTITION_LABEL
    local nasshare=$1_NAS_SHARE
    local vfsfn=$1_VFS_FN
    if [ -z "${!vfsfn}" ]; then
        return
    elif [ -n "${!label}" ]; then
        eval $1_VFS_PATH=$(get_mount_path_from_label ${!label})/${!vfsfn}
    elif [ -n "${!nasshare}" ]; then
        eval $1_VFS_PATH=$NAS_ROOT/${!nasshare,,}/${!vfsfn}
    fi
    #local vfspath=$1_VFS_PATH
    #echo ${!vfspath}
}

# nfs-common must be installed if any NAS shares are to be mounted
# Depends: util/fs.sh - list_not_empty
# Depends: cfg/cfg.sh - *_NAS_SHARE
# Depends: .          - dev_check_want_mounted (*_WANTMOUNT)
dev_check_nfs_wanted() {
    NFS_REQUESTED=false
    local str=""
    if $HOME_WANTMOUNT;  then str="$str $HOME_NAS_SHARE";  fi
    if $REPO_WANTMOUNT;  then str="$str $REPO_NAS_SHARE";  fi
    if $GAMES_WANTMOUNT; then str="$str $GAMES_NAS_SHARE"; fi

    # Assume NAS shares are to be mounted over NFS
    # whether or not the paths above reside on one
    if $NAS_WANTMOUNT || list_not_empty $str; then NFS_REQUESTED=true; fi
}

# nbd must be enabled if *any* virtual filesystems are to be mounted
# Depends: util/fs.sh - list_not_empty
# Depends: cfg/cfg.sh - *_VFS_FN
# Depends: .          - dev_check_want_mounted (*_WANTMOUNT)
dev_check_nbd_wanted() {
    NBD_REQUESTED=false
    local str=""
    if $HOME_WANTMOUNT;     then str="$str $HOME_VFS_FN";  fi
    if $REPO_WANTMOUNT;     then str="$str $REPO_VFS_FN";  fi
    if $GAMES_WANTMOUNT;    then str="$str $GAMES_VFS_FN"; fi
    if list_not_empty $str; then NBD_REQUESTED=true;       fi
}

# Depends: cfg/cfg.sh
# Depends: util/device.sh - get_device_path_from_label
# Depends: .              - get_vfs_path
dev_check_paths() {
    if [ $# -eq 2 ]; then local ipnas=$2; else local ipnas=""; fi

    get_vfs_path GAMES # set GAMES_VFS_PATH
    if [ -f "$GAMES_VFS_PATH" -a -n "$GAMES_NBD" ]; then
        GAMES_PARTITION_DEV=$GAMES_NBD
    else
        GAMES_PARTITION_DEV=$(get_device_path_from_label $GAMES_PARTITION_LABEL $ipnas 2> /dev/null)
    fi

    get_vfs_path HOME # set HOME_VFS_PATH
    if [ -f "$HOME_VFS_PATH" -a -n "$HOME_NBD" ]; then
        HOME_PARTITION_DEV=$HOME_NBD
    else
        HOME_PARTITION_DEV=$(get_device_path_from_label $HOME_PARTITION_LABEL $ipnas 2> /dev/null)
    fi

    get_vfs_path REPO # set REPO_VFS_PATH
    if [ -f "$REPO_VFS_PATH" -a -n "$REPO_NBD" ]; then
        REPO_PARTITION_DEV=$REPO_NBD
    else
        REPO_PARTITION_DEV=$(get_device_path_from_label $REPO_PARTITION_LABEL $ipnas 2> /dev/null)
    fi
    STEAM_LUN_PARTITION_DEV=$(get_device_path_from_label $STEAM_LUN_PARTITION_LABEL $ipnas 2> /dev/null)
    VM_PARTITION_DEV=$(get_device_path_from_label $VM_PARTITION_LABEL $ipnas 2> /dev/null)
}

# Depends: check_exists
dev_check_mounted() {
    if $GAMES_EXISTS     && dev_mounted $GAMES_PARTITION_DEV; then     GAMES_MOUNTED=true; else     GAMES_MOUNTED=false; fi
    if $HOME_EXISTS      && dev_mounted $HOME_PARTITION_DEV; then      HOME_MOUNTED=true; else      HOME_MOUNTED=false;  fi
    if $REPO_EXISTS      && dev_mounted $REPO_PARTITION_DEV; then      REPO_MOUNTED=true; else      REPO_MOUNTED=false;  fi
    if $STEAM_LUN_EXISTS && dev_mounted $STEAM_LUN_PARTITION_DEV; then STEAM_LUN_MOUNTED=true; else STEAM_LUN_MOUNTED=false; fi
    if $VM_EXISTS        && dev_mounted $VM_PARTITION_DEV; then        VM_MOUNTED=true; else        VM_MOUNTED=false;    fi
    # TODO: Use get_nas_share_list to avoid hard-coding these share names
    if dev_mounted $(get_device_path_from_label Audiobooks $IP_NAS_ETH) && \
       dev_mounted $(get_device_path_from_label Fitness $IP_NAS_ETH) && \
       dev_mounted $(get_device_path_from_label GitLab $IP_NAS_ETH) && \
       dev_mounted $(get_device_path_from_label Internet $IP_NAS_ETH) && \
       dev_mounted $(get_device_path_from_label Movies $IP_NAS_ETH) && \
       dev_mounted $(get_device_path_from_label Music $IP_NAS_ETH) && \
       dev_mounted $(get_device_path_from_label Other $IP_NAS_ETH) && \
       dev_mounted $(get_device_path_from_label Television $IP_NAS_ETH)
    then
        NAS_MOUNTED=true
    else
        NAS_MOUNTED=false
    fi
}

# Check if devices are mounted at the desired path
# Usage: dev_check_properly_mounted <device_path>
dev_check_properly_mounted() {
    # TODO: The additional checks for $*_EXISTS is required but is messy;
    #       there's got to be a better approach.
    GAMES_PROPERMOUNT=false
    if $GAMES_EXISTS && verify_mount_path $GAMES_PARTITION_DEV $GAMES_MNT_PATH; then GAMES_PROPERMOUNT=true; fi
    HOME_PROPERMOUNT=false
    if $HOME_EXISTS && verify_mount_path $HOME_PARTITION_DEV $HOME_MNT_PATH; then HOME_PROPERMOUNT=true; fi
    REPO_PROPERMOUNT=false
    if $REPO_EXISTS; then
        if [ -n "$REPO_PARTITION_LABEL" ] && verify_mount_path $REPO_PARTITION_DEV $REPO_MNT_PATH; then REPO_PROPERMOUNT=true; fi
        if [ -n "$REPO_VFS_FN" ]          && verify_mount_path $REPO_VFS_PATH $REPO_MNT_PATH;      then REPO_PROPERMOUNT=true; fi
    fi
    STEAM_LUN_PROPERMOUNT=false
    if $STEAM_LUN_EXISTS && verify_mount_path $STEAM_LUN_PARTITION_DEV $STEAM_LUN_MNT_PATH; then STEAM_LUN_PROPERMOUNT=true; fi
    VM_PROPERMOUNT=false
    if $VM_EXISTS; then
        if [ -n "$VM_PARTITION_LABEL" ] && verify_mount_path $VM_PARTITION_DEV $VM_MNT_PATH; then VM_PROPERMOUNT=true; fi
        if [ -n "$VM_NAS_SHARE" ] && verify_mount_path $(get_full_nas_share_path $VM_NAS_SHARE) $VM_MNT_PATH; then VM_PROPERMOUNT=true; fi
    fi

    NAS_PROPERMOUNT=false
    if $NAS_EXISTS; then
        local nas_share_list=$(get_nas_share_list $IP_NAS_ETH)
        if [ $(list_length $nas_share_list) -ne 0 ]; then
            NAS_PROPERMOUNT=true
            # Iterate through lower-case share names and see if all the paths exist
            while read line; do
                if [ ! -d "$NAS_ROOT/$(basename ${line,,})" ]; then NAS_PROPERMOUNT=false; fi
            done < <(echo "$nas_share_list")
        fi
    fi
}

# Description: Describe the cases in which each of the devices are mounted
# Depends:     cfg/cfg.sh
# Depends:     util/net.sh - net_survey (INTERNET_CONNECTED, METERED_NETWORK, NAS_EXISTS)
dev_check_want_mounted() {
    GAMES_WANTMOUNT=false
    HOME_WANTMOUNT=false
    #NAS_WANTMOUNT=false
    REPO_WANTMOUNT=false
    STEAM_LUN_WANTMOUNT=false
    VM_WANTMOUNT=false

    if $SW_INSTALL_GAMES || $SW_INSTALL_STEAM; then
        GAMES_WANTMOUNT=true
    fi

    if $PERSISTENT_HOME; then
        HOME_WANTMOUNT=true
    fi

    #if $NAS_EXISTS; then
    #    NAS_WANTMOUNT=true
    #fi
    if $SW_INSTALL_MIRROR || ( $INTERNET_CONNECTED && $METERED_NETWORK ) || ( ! $INTERNET_CONNECTED && ( $SW_UPDATE || $SW_INSTALL || $SW_UPGRADE ) ) ; then
        REPO_WANTMOUNT=true
    fi

    if $SW_INSTALL_STEAM; then
        STEAM_LUN_WANTMOUNT=true
    fi

    if $SW_INSTALL_VM; then
        VM_WANTMOUNT=true
    fi
}

# Connect a Network Block Device (nbd) if a virtual filesystem is provided
# Depends: cfg/cfg.sh     - *_NBD
# Depends: util/apt.sh
# Depends: util/device.sh - nbd_connect
# Depends: .              - get_vfs_path
dev_connect_if_nbd() {
    if [ -f "$GAMES_VFS_PATH" ]; then nbd_connect $GAMES_NBD $GAMES_VFS_PATH; fi
    if [ -f "$HOME_VFS_PATH" ];  then nbd_connect $HOME_NBD  $HOME_VFS_PATH;  fi
    if [ -f "$REPO_VFS_PATH" ];  then nbd_connect $REPO_NBD  $REPO_VFS_PATH;  fi
}

# Description: Connect/mount pre-requisites such as NAS shares and NBD
#              connections, then attempt to mount the filesystem
# Usage:       dev_connect_and_mount <GAMES|HOME|NAS|REPO|STEAM_LUN|VM>
dev_connect_and_mount() {
    local device=$1_PARTITION_DEV
    local label=$1_PARTITION_LABEL
    local mntpath=$1_MNT_PATH
    local nasshare=$1_NAS_SHARE
    local nbd=$1_NBD
    local vfspath=$1_VFS_PATH
    if [ -n "${!nasshare}" ] && nas_share_exists $IP_NAS_ETH ${!nasshare}; then
        mount_share $(get_nas_share_path $IP_NAS_ETH ${!nasshare}) $IP_NAS_ETH $NAS_ROOT
    fi
    if [ -n "${!label}" ]; then
        mount_if_not_mounted ${!device} ${!mntpath}
    elif [ -f "${!vfspath}" ]; then
        nbd_connect ${!nbd} ${!vfspath}
        mount_if_not_mounted ${!nbd} ${!mntpath}
    fi
}

# Description: Mount devices
# Depends:     util/device.sh - mount_if_not_mounted
# Depends:     util/fs.sh     - is_a_broken_symlink, update_symlink
# Depends:     .              - dev_connect_and_mount
dev_mount() {
    if $REPO_WANTMOUNT; then
        dev_connect_and_mount REPO

        # Not running apt-mirror, but this is where we placed the packages/lists
        update_symlink $REPO_MNT_PATH/var-lib-apt-lists /var/lib/apt/lists sudo
    fi
    # TODO: Organize this better
    # return to a folder if the symlink breaks for some reason
    if is_a_broken_symlink /var/lib/apt/lists; then
        sudo rm /var/lib/apt/lists
        sudo mkdir /var/lib/apt/lists
    fi

    if $HOME_WANTMOUNT; then
        dev_connect_and_mount HOME
    fi
    if $GAMES_WANTMOUNT; then
        dev_connect_and_mount GAMES
    fi
    if $STEAM_LUN_WANTMOUNT; then
        dev_connect_and_mount STEAM_LUN
    fi
    if $VM_WANTMOUNT; then
        dev_connect_and_mount VM
    fi
}

# Link personal directories into ones located on persistent storage
task_persistent_home() {
    if $HOME_PROPERMOUNT; then
        echo_blank -n "$_CONFIG Establishing a persistent home environmment"

        # The uid/gid may change if a different OS is used, but permissions
        # on persistent media does not, so they must be updated to avoid
        # permission issues.
        find "$HOME_MNT_PATH" -maxdepth 1 \
                              -not -path "$HOME_MNT_PATH/lost+found" \
                              -exec sudo chown -R $USER:$USER {} \;

        update_symlink $HOME_MNT_PATH/Desktop        $HOME/Desktop
        update_symlink $HOME_MNT_PATH/Documents      $HOME/Documents
        update_symlink $HOME_MNT_PATH/Downloads      $HOME/Downloads
        update_symlink $HOME_MNT_PATH/Music          $HOME/Music
        update_symlink $HOME_MNT_PATH/Pictures       $HOME/Pictures
        update_symlink $HOME_MNT_PATH/Public         $HOME/Public
        update_symlink $HOME_MNT_PATH/Templates      $HOME/Templates
        update_symlink $HOME_MNT_PATH/Videos         $HOME/Videos
        update_symlink $HOME_MNT_PATH/.vnc           $HOME/.vnc
        echo_success -e "$_CONFIG Established a persistent home environment"
    elif $HOME_WANTMOUNT; then
        echo_failure "$_CONFIG Persistent home failed... partition not mounted"
    fi
    if $GAMES_PROPERMOUNT; then
        echo_blank -n "$_CONFIG Establishing a persistent gaming environmment"
        update_symlink $GAMES_MNT_PATH $HOME/Games
        echo_success -e "$_CONFIG Established a persistent gaming environment"
    elif $GAMES_WANTMOUNT; then
        echo_failure "$_CONFIG Persistent gaming failed... partition not mounted"
    fi
}
