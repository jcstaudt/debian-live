# Depends: util/fs.sh - print_available_memory, print_percentage
# Depends: util/vm.sh

link_all_vm() {

    ############################################################################
    # Android
    #
    vm_link --arch="x86_64" \
            --boot="hd" \
            --description="64-bit" \
            --diskinterface="virtio" \
            --diskname="androidx86" \
            --disktype="qcow2" \
            --headless="yes" \
            --os-variant="generic" \
            --ram="$(print_percentage $(print_available_memory) 0.25)" \
            --static-ip="192.168.122.5" \
            --static-mac="52:54:00:64:77:9d" \
            --title="Android_x86" \
            --vcpus="$(nproc)"

    ############################################################################
    # CentOS
    #
    vm_link --arch="x86_64" \
            --boot="hd" \
            --description="64-bit" \
            --diskinterface="virtio" \
            --diskname="CentOS-7" \
            --disktype="qcow2" \
            --headless="no" \
            --os-variant="centos7.0" \
            --ram="$(print_percentage $(print_available_memory) 0.20)" \
            --title="CentOS_7" \
            --vcpus="$(nproc)"

    ############################################################################
    # Debian
    #
    vm_link --arch="x86_64" \
            --boot="hd" \
            --description="64-bit" \
            --diskinterface="virtio" \
            --diskname="Debian-10-Server" \
            --disktype="qcow2" \
            --headless="no" \
            --os-variant="debian10" \
            --ram="$(print_percentage $(print_available_memory) 0.20)" \
            --title="Debian_10_(Server)" \
            --vcpus="$(nproc)"

    vm_link --arch="x86_64" \
            --boot="hd" \
            --description="PureOS_VM" \
            --diskinterface="virtio" \
            --diskname="pureos" \
            --disktype="qcow2" \
            --headless="no" \
            --os-variant="debian10" \
            --ram="$(print_percentage $(print_available_memory) 0.25)" \
            --title="PureOS" \
            --vcpus="$(nproc)"

    vm_link --arch="aarch64" \
            --boot="hd" \
            --description="64-bit" \
            --diskinterface="virtio" \
            --diskname="debian-11-openstack-arm64" \
            --disktype="qcow2" \
            --headless="no" \
            --os-variant="debiantesting" \
            --ram="2048" \
            --title="Debian10ARM" \
            --vcpus="$(nproc)"

    # "Live boot" environment - no persistent storage
    vm_link --arch="x86_64" \
            --boot="hd" \
            --cdrom="/media/$USER/home/Downloads/pureos-10.3-gnome-live-20230614_amd64.iso" \
            --description="PureOS_Live_Image" \
            --diskname="pureos" \
            --diskinterface="virtio" \
            --headless="no" \
            --os-variant="debian10" \
            --ram="$(print_percentage $(print_available_memory) 0.33)" \
            --title="PureOS" \
            --vcpus="$(nproc)"

    # Work environment
    vm_link --arch="x86_64" \
            --boot="hd" \
            --description="PureOS_Work" \
            --diskname="pureos" \
            --disktype="qcow2" \
            --diskinterface="virtio" \
            --headless="no" \
            --os-variant="debian10" \
            --ram="$(print_percentage $(print_available_memory) 0.65)" \
            --title="PureOS" \
            --vcpus="$(nproc)"

    ############################################################################
    # Windows
    #
    vm_link --arch="x86_64" \
            --boot="hd" \
            --description="?-bit" \
            --diskinterface="ide" \
            --diskname="Windows-XP" \
            --disktype="qcow2" \
            --headless="no" \
            --os-variant="winxp" \
            --ram="1024" \
            --title="Windows_XP" \
            --vcpus="1"

    vm_link --arch="x86_64" \
            --boot="hd" \
            --description="64-bit" \
            --diskinterface="sata" \
            --diskname="Windows-7-Pro-64bit" \
            --disktype="qcow2" \
            --headless="no" \
            --os-variant="win7" \
            --ram="$(print_percentage $(print_available_memory) 0.25)" \
            --title="Windows_7_Pro" \
            --vcpus="$(nproc)"

    vm_link --arch="x86_64" \
            --boot="hd" \
            --description="64-bit" \
            --diskinterface="sata" \
            --diskname="Windows-10-Pro-64bit" \
            --disktype="qcow2" \
            --headless="no" \
            --os-variant="win10" \
            --ram="$(print_percentage $(print_available_memory) 0.25)" \
            --title="Windows_10_Pro" \
            --vcpus="$(nproc)"

    vm_link --arch="x86_64" \
            --boot="hd" \
            --description="64-bit" \
            --diskinterface="sata" \
            --diskname="win10" \
            --disktype="qcow2" \
            --headless="no" \
            --os-variant="win10" \
            --ram="$(print_percentage $(print_available_memory) 0.25)" \
            --title="Windows_10" \
            --vcpus="$(nproc)"

    vm_link --arch="x86_64" \
            --boot="uefi" \
            --description="64-bit" \
            --diskinterface="sata" \
            --diskname="win10.uefi" \
            --disktype="qcow2" \
            --headless="no" \
            --os-variant="win10" \
            --ram="$(print_percentage $(print_available_memory) 0.25)" \
            --title="Windows_10_(UEFI)" \
            --vcpus="$(nproc)"

    ############################################################################
    # macOS
    #
    # For working macOS xml import, please refer to https://github.com/foxlet/macOS-Simple-KVM
    # TODO: Configure CPU topology, NIC
    vm_link --arch="x86_64" \
            --boot="uefi" \
            --description="64-bit" \
            --diskinterface="sata" \
            --diskname="macos" \
            --disktype="qcow2" \
            --headless="no" \
            --os-variant="generic" \
            --ram="2048" \
            --title="macOS" \
            --vcpus=4

    ############################################################################
    # Servers
    #
    vm_link --arch="x86_64" \
            --boot="hd" \
            --description="64-bit" \
            --diskinterface="virtio" \
            --diskname="openwrt-15.05.1-x86-64-combined-ext4" \
            --disktype="qcow2" \
            --headless="yes" \
            --os-variant="debian10" \
            --ram="512" \
            --static-ip="192.168.122.1" \
            --static-mac="52:54:00:37:00:01" \
            --title="OpenWRT_Server" \
            --vcpus="2"

    vm_link --arch="x86_64" \
            --boot="hd" \
            --description="64-bit" \
            --diskinterface="virtio" \
            --diskname="Debian-9-Server" \
            --disktype="qcow2" \
            --headless="yes" \
            --os-variant="debian11" \
            --ram="$(print_percentage $(print_available_memory) 0.90)" \
            --static-ip="192.168.122.2" \
            --static-mac="52:54:00:37:5e:1e" \
            --title="Debian_9_Server" \
            --vcpus="$(nproc)"

    vm_link --arch="x86_64" \
            --boot="hd" \
            --description="64-bit" \
            --add-storage="/dev/disk/by-id/scsi-35000cca0d8cfb7e3,cache=none,io=native" \
            --add-storage="/dev/disk/by-id/scsi-35000cca0d8d07f1c,cache=none,io=native" \
            --add-storage="/dev/disk/by-id/scsi-35000cca0d8d121d4,cache=none,io=native" \
            --add-storage="/dev/disk/by-id/scsi-35000cca0d8d12800,cache=none,io=native" \
            --diskinterface="virtio" \
            --diskname="omv" \
            --disktype="qcow2" \
            --headless="yes" \
            --os-variant="debian11" \
            --ram="2048" \
            --static-ip="192.168.122.3" \
            --static-mac="52:54:00:37:00:03" \
            --title="OpenMediaVault" \
            --vcpus="$(nproc)"

    vm_link --arch="x86_64" \
            --boot="hd" \
            --description="64-bit" \
            --diskinterface="virtio" \
            --diskname="freedombox-bullseye-free_all-amd64" \
            --disktype="qcow2" \
            --headless="yes" \
            --os-variant="debian10" \
            --ram="$(print_percentage $(print_available_memory) 0.33)" \
            --static-ip="192.168.122.4" \
            --static-mac="52:54:00:37:00:04" \
            --title="FreedomBox_Server" \
            --vcpus="$(nproc)"
}