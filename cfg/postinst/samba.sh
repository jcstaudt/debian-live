if $PERSONALIZE; then
    # TODO: Share doesn't currently appear locally or in Windows
    local localip=$(hostname -I | cut -d' ' -f1)
    local smbpath="/etc/samba/smb.conf"
    append_unique_string_to_file "[gamesaverepo]" $smbpath sudo
    append_unique_string_to_file "comment = Git repo for video game saves" $smbpath sudo
    append_unique_string_to_file "guest ok = yes" $smbpath sudo
    append_unique_string_to_file "hosts allow = 127.0.0.1/8 ${localip%.*}.0/24" $smbpath sudo
    append_unique_string_to_file "path = $GAMES_MNT_PATH/video-game-saves" $smbpath sudo
    append_unique_string_to_file "browseable = yes" $smbpath sudo #TODO: this won't be placed because it's not unique in the file
    append_unique_string_to_file "read only = no" $smbpath sudo
    sudo systemctl restart smbd
    return $SUCCESS
else
    return $SKIP
fi