if $PERSONALIZE; then
    gsettings set org.gnome.gedit.plugins.terminal scrollback-lines 128
    return $SUCCESS
else
    return $SKIP
fi