if $PERSONALIZE; then
    if ! grep -q " $ANDROID_PHONE_NAME" /etc/hosts; then
        echo_failure -e "Phone $ANDROID_PHONE_NAME not found in /etc/hosts"
        return $FAILURE
    elif ! test_connection $ANDROID_PHONE_NAME; then
        echo_failure -e "Unable to detect phone $ANDROID_PHONE_NAME"
        return $FAILURE
    else
        #adb start-server > /dev/null # for USB-connected devices
        if ! adb devices | grep -q 2233; then
            if adb tcpip 2233 > /dev/null; then # for wireless connection
                press_enter_to_continue "About to issue phone prompt; press enter to continue"
                adb connect $ANDROID_PHONE_NAME:2233 > /dev/null
                press_enter_to_continue "Accept phone prompt, then press enter"
                return $SUCCESS
            fi
        fi
    fi
else
    return $SKIP
fi