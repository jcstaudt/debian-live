if $PERSONALIZE; then
    gsettings set org.gnome.meld indent-width                  4
    gsettings set org.gnome.meld insert-spaces-instead-of-tabs true
    gsettings set org.gnome.meld show-line-numbers             true
    return $SUCCESS
else
    return $SKIP
fi