if $PERSONALIZE && $GAMES_PROPERMOUNT; then
    if ! update_symlink "$GAMES_MNT_PATH/.PlayOnLinux" $HOME/.PlayOnLinux; then return $FAILURE; fi
    if ! update_symlink "$GAMES_MNT_PATH/.PlayOnLinux//wineprefix/" "$HOME/PlayOnLinux's virtual drives"; then return $FAILURE; fi
    return $SUCCESS
else
    return $SKIP
fi