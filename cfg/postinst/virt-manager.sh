# Traverse VM list and link ones with an existing virtual disk or iso image
vm_init
link_all_vm

# Configure virt-manager
gsettings set org.virt-manager.virt-manager         system-tray true
gsettings set org.virt-manager.virt-manager.console scaling     2