if $PERSONALIZE; then
    touch $HOME/.nanorc
    # TODO: Fix double-quote-within-variable resolution in append_unique_string_to_file
    #sed -i 's/# extendsyntax makefile tabgives "	"/extendsyntax makefile tabgives "	"/g' $HOME/.nanorc
    #append_unique_string_to_file "extendsyntax makefile tabgives '	'"                       $HOME/.nanorc
    #sed -i 's/# extendsyntax python tabgives "    "/extendsyntax python tabgives "    "/g'   $HOME/.nanorc
    #append_unique_string_to_file "extendsyntax python tabgives '    '"                       $HOME/.nanorc
    sed -i 's/# set atblanks/set atblanks/g'                                                 $HOME/.nanorc
    append_unique_string_to_file "set atblanks"                                              $HOME/.nanorc
    sed -i 's/# set casesensitive/set casesensitive/g'                                       $HOME/.nanorc
    append_unique_string_to_file "set casesensitive"                                         $HOME/.nanorc
    sed -i 's/# set indicator/set indicator/g'                                               $HOME/.nanorc
    append_unique_string_to_file "set indicator"                                             $HOME/.nanorc
    sed -i 's/# set linenumbers/set linenumbers/g'                                           $HOME/.nanorc
    append_unique_string_to_file "set linenumbers"                                           $HOME/.nanorc
    sed -i 's/# set mouse/set mouse/g'                                                       $HOME/.nanorc
    append_unique_string_to_file "set mouse"                                                 $HOME/.nanorc
    sed -i 's/# set noconvert/set noconvert/g'                                               $HOME/.nanorc
    append_unique_string_to_file "set noconvert"                                             $HOME/.nanorc
    sed -i 's/# set nonewlines/set nonewlines/g'                                             $HOME/.nanorc
    append_unique_string_to_file "set nonewlines"                                            $HOME/.nanorc
    sed -i 's/# set smarthome/set smarthome/g'                                               $HOME/.nanorc
    append_unique_string_to_file "set smarthome"                                             $HOME/.nanorc
    sed -i 's/# set softwrap/set softwrap/g'                                                 $HOME/.nanorc
    append_unique_string_to_file "set softwrap"                                              $HOME/.nanorc
    sed -i 's/# set suspendable/set suspendable/g'                                           $HOME/.nanorc
    append_unique_string_to_file "set suspendable"                                           $HOME/.nanorc
    sed -i 's/# set tabsize 8/set tabsize 4/g'                                               $HOME/.nanorc
    append_unique_string_to_file "set tabsize 4"                                             $HOME/.nanorc
    sed -i 's/# set tabstospaces/set tabstospaces/g'                                         $HOME/.nanorc
    append_unique_string_to_file "set tabstospaces"                                          $HOME/.nanorc
    return $SUCCESS
else
    return $SKIP
fi