if $PERSONALIZE && $HOME_PROPERMOUNT; then
    if $REPO_PROPERMOUNT; then
        (task_install $REPO_MNT_PATH/net.downloadhelper.coapp-1.6.3-1_amd64.deb)
    fi
    if ! update_symlink "$HOME_MNT_PATH/.mozilla"       $HOME/.mozilla;       then return $FAILURE; fi
    if ! update_symlink "$HOME_MNT_PATH/.cache/mozilla" $HOME/.cache/mozilla; then return $FAILURE; fi
    alias firefox="firefox --allow-downgrade"
    sudo sed -i 's/Exec=\/usr\/lib\/firefox-esr\/firefox-esr %u/Exec=\/usr\/lib\/firefox-esr\/firefox-esr --allow-downgrade %u/g' /usr/share/applications/firefox-esr.desktop


    # Avoid permissions issues as different live images use different usernames
    # Persistent data will not update on its own
    if [ -d $HOME/.mozilla ]; then
        sudo chown -R $USER $HOME/.mozilla
    else
        return $FAILURE
    fi
    if [ -d $HOME/.cache/mozilla ]; then
        sudo chown -R $USER $HOME/.cache/mozilla
    else
        return $FAILURE
    fi
    return $SUCCESS
else
    return $SKIP
fi