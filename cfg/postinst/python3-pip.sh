if $INTERNET_CONNECTED; then
    sudo pip3 install pyconnman > /dev/null
    sudo pip3 install pywinusb > /dev/null
    return $SUCCESS
else
    return $SKIP
fi