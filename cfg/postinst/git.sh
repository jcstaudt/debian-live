if $PERSONALIZE; then
    # Alternatively create a ~/.gitconfig file
    git config --global alias.br "branch"
    git config --global alias.branch "branch -vv"
    git config --global alias.ci "commit"
    git config --global alias.co "checkout"
    git config --global alias.revertlastcommit "revert HEAD~1 -m 1"
    git config --global alias.diff-nofm "-c core.fileMode=false diff"
    git config --global alias.fetch "fetch --all --prune"
    git config --global alias.push "push --set-upstream origin"
    git config --global alias.st "status --ignored"
    git config --global alias.l "log --graph --pretty=format=\"%C(yellow)%h%C(cyan)%d%Creset %s %C(white)- %an, %ar%Creset\""
    git config --global alias.ll "log --stat --abbrev-commit"
    git config --global alias.gr "log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) %C(bold green)%ar%C(reset) %C(white)%s%C(reset) %C(dim white)-%an%C(reset)%C(bold yellow)%d%C(reset)' --all"
    git config --global alias.gr1 "log --graph --full-history --all --color --pretty=format:\"%x1b[31m%h%x09%x1b[32m%d%x1b[0m%x20%s [%an]\""
    git config --global branch.autosetuprebase "always"
    git config --global color.branch "auto"
    git config --global color.diff "auto"
    git config --global color.interactive "auto"
    git config --global color.status "auto"
    git config --global color.ui "auto"
    git config --global color.vi "auto"
    git config --global core.editor "vi"
    git config --global core.fileMode "true"
    git config --global diff.tool "meld"
    git config --global difftool.prompt "false"
    #git config --global "difftool meld".cmd "meld $LOCAL $REMOTE"
    git config --global merge.tool "meld"
    git config --global pull.rebase false # merge (default) rather than rebase or fast-forward
    git config --global user.email $EMAIL
    git config --global user.name "$NAME"
    git config --global cola.spellCheck "false"

    if pkg_installed gpg; then
        # Sign commits with GPG
        gpgkey=$(gpg --list-secret-keys --keyid-format LONG $GPG_EMAIL | grep "sec " | cut -d'/' -f2 | cut -d' ' -f1)
        if [ -n "$gpgkey" ]; then
            git config --global user.signingkey $gpgkey
            git config --global commit.gpgsign true
        fi
        unset gpgkey
    else
        return $FAILURE
    fi
    return $SUCCESS
else
    return $SKIP
fi