if $PERSONALIZE; then
    # TODO: This will overwrite any changes from other applications;
    # TODO: Play well with others
    cat <<EOF > $HOME/.config/mimeapps.list
[Default Applications]
application/x-cd-image=vlc.desktop
video/mp4=vlc.desktop
video/x-matroska=vlc.desktop

[Added Associations]
application/x-cd-image=vlc.desktop;
video/mp4=vlc.desktop;
video/x-matroska=vlc.desktop;
EOF
    return $SUCCESS
else
    return $SKIP
fi