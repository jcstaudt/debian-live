if $PERSONALIZE; then
    gsettings set org.gnome.software first-run false
    return $SUCCESS
else
    return $SKIP
fi