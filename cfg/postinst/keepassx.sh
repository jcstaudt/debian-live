if $PERSONALIZE && $HOME_PROPERMOUNT; then
    mkdir -p $HOME/.config/keepassx
    if [ ! -d $HOME/.config/keepassx ]; then return $FAILURE; fi
    # TODO: Do not hard-code database file location
    cat <<EOF > $HOME/.config/keepassx/keepassx2.ini
[General]
AutoSaveAfterEveryChange=false
AutoSaveOnExit=false
AutoTypeEntryTitleMatch=true
GlobalAutoTypeKey=0
GlobalAutoTypeModifiers=0
LastDatabases=$HOME/Documents/keepassx.kdbx
LastDir=$HOME/Documents
LastKeyFiles=@Variant(\0\0\0\x1c\0\0\0\0)
LastOpenedDatabases=$HOME/Documents/keepassx.kdbx
MinimizeOnCopy=false
OpenPreviousDatabasesOnStartup=true
RememberLastDatabases=true
RememberLastKeyFiles=true
ShowToolbar=true
UseGroupIconOnEntryCreation=false

[GUI]
EntrySearchColumnSizes=@Invalid()
Language=system
MinimizeToTray=true
ShowTrayIcon=true
SplitterState=@Invalid()

[security]
autotypeask=true
clearclipboardtimeout=15
clearclipboard=true
lockdatabaseidlesec=120
lockdatabaseidle=true
passwordscleartext=false
EOF
    return $SUCCESS
else
    return $SKIP
fi