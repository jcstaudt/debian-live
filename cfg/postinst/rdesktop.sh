if $PERSONALIZE; then
    # Remote desktop with compression and local sound
    alias rdesktop="rdesktop -k en-us -f -N -z -rsound:local"
    return $SUCCESS
else
    return $SKIP
fi