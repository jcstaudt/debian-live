if $PERSONALIZE; then
    if [ -d "$GAMESAVE_MNT_PATH" ]; then
        if [ -d "$GAMESAVE_MNT_PATH/screenshots/gamegear" ]; then
            sed -i "s|ScreenshotPath       = '${HOME}'|ScreenshotPath       = '${GAMESAVE_MNT_PATH}/screenshots/gamegear'|g" $HOME/.osmose-emulator.ini
        fi
        if [ -d "$GAMESAVE_MNT_PATH/sstates/gamegear" ]; then
            sed -i "s|SaveStatePath        = '${HOME}'|SaveStatePath        = '${GAMESAVE_MNT_PATH}/sstates/gamegear'|g" $HOME/.osmose-emulator.ini
        fi
        return $SUCCESS
    fi
    return $FAILURE
else
    return $SKIP
fi
