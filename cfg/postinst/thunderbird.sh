if $PERSONALIZE && $HOME_PROPERMOUNT; then
    if ! update_symlink "$HOME_MNT_PATH/.thunderbird"       $HOME/.thunderbird;       then return $FAILURE; fi
    if ! update_symlink "$HOME_MNT_PATH/.cache/thunderbird" $HOME/.cache/thunderbird; then return $FAILURE; fi

    # Avoid permissions issues as different live images use different usernames
    # Persistent data will not update on its own
    sudo chown -R $USER ~/.thunderbird/
    sudo chown -R $USER ~/.cache/thunderbird/

    return $SUCCESS
else
    return $SKIP
fi
