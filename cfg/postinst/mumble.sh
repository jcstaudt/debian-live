if $PERSONALIZE && $HOME_PROPERMOUNT; then
    if ! update_symlink "$HOME_MNT_PATH/.config/Mumble" $HOME/.config/Mumble; then return $FAILURE; fi
    return $SUCCESS
else
    return $SKIP
fi