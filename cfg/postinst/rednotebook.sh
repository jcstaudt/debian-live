if $PERSONALIZE && $HOME_PROPERMOUNT; then
    if ! update_symlink "$HOME_MNT_PATH/.rednotebook" $HOME/.rednotebook; then return $FAILURE; fi
    cat <<EOF > $HOME/.rednotebook/configuration.cfg
autoSwitchMode=0
checkForNewVersion=0
closeToTray=0
cloudIgnoreList=filter, these, comma, separated, words, and, #tags, katelyn
cloudIncludeList=mtv, spam, work, job, play
cloudMaxTags=1000
dataDir=$HOME/.rednotebook/data
dateTimeString=%A, %x %X
exportDateFormat=%A, %x
firstStart=0
instantSearch=1
lastBackupDate=9999-12-31
leftDividerPosition=253
mainFont=Sans 9
mainFrameHeight=1016
mainFrameMaximized=0
mainFrameWidth=1393
mainFrameX=26
mainFrameY=23
previewFont=mingliu, MS Mincho, sans-serif
rightDividerPosition=705
showTagsPane=0
spellcheck=0
useInternalPreview=1
userDir=
weekNumbers=0
EOF
    return $SUCCESS
else
    return $SKIP
fi