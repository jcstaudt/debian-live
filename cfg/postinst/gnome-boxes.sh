if $VM_PROPERMOUNT; then
    boxes_images=$HOME/.local/share/gnome-boxes/images
    boxes_config=$HOME/.config/libvirt/qemu
    boxes_snapshot=$HOME/.config/libvirt/qemu/snapshot

    pureos_boxes_qcow2=$boxes_images/$pureos_name.$pureos_type
    pureos_boxes_config=$boxes_config/$pureos_name.xml
    pureos_boxes_snapshot="$boxes_snapshot/$pureos_name/Yet another snapshot.xml" # TODO: can't handle spaces

    win10_boxes_qcow2=$boxes_images/$win10_name.$win10_type
    win10_boxes_config=$boxes_config/$win10_name.xml
    win10_boxes_snapshot="$boxes_snapshot/$win10_name/Initial snapshot.xml" # TODO: can't handle spaces

    android_boxes_qcow2=$boxes_images/$android_name.$android_type
    android_boxes_config=$boxes_config/$android_name.xml

    # gnome-boxes wants to "import" the qcow2 by copying it to the
    # local directory. We only want to retain our existing image, so
    # convince the system that it is already there
    if [ -d $HOME/.local/share/gnome-boxes ]; then
        if ! update_symlink "$VM_MNT_PATH" $HOME/.local/share/gnome-boxes/images; then return $FAILURE; fi
    fi
    return $SUCCESS
else
    return $SKIP
fi