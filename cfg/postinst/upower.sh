if $PERSONALIZE; then
    # Display battery information
    #alias battery='upower -i /org/freedesktop/UPower/devices/battery_BAT1 | grep -E "state|time\ to\ empty|time\ to\ full|percentage"'
    alias battery='upower -i $(upower -e | grep "BAT")'
    return $SUCCESS
else
    return $SKIP
fi