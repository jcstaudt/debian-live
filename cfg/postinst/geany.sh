if $PERSONALIZE && $HOME_PROPERMOUNT; then
    mkdir -p $HOME/.config/geany
    if [ ! -d $HOME/.config/geany ]; then return $FAILURE; fi
    if ! update_symlink "$HOME_MNT_PATH/.config/geany/geany.conf" $HOME/.config/geany/geany.conf; then return $FAILURE; fi
    return $SUCCESS
else
    return $SKIP
fi