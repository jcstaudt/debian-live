if $PERSONALIZE; then
    cat <<EOF > $HOME/.vimrc
" .vimrc

set backspace=indent,eol,start

" c: automatically wrap comments
" q: allow rewrapping of comments with gq
" r: automatically insert comment character
" l: don't break previously long lines
" set formatoptions=cqrlo

" General
set ts=4                       " ts=tabstop; Size of a hard tabstop
set sw=4                       " sw=shiftwidth; Size of an indent
set softtabstop=4              " A combination of spaces and tabs are used to simulate tab stops at a width other than the (hard) tabstop
set cin
set et
set is
set list                       " Enables tab character replacement for visibility
set listchars=tab:>-           " Replace "tab" char with ">---"
set nu
set smarttab                   " Make 'tab' insert indents instead of tabs at the beginning of a line
set expandtab                  " Always use spaces instead of tabs
" set tw=80                      " tw=textwidth; Set the default text width. This enters a carriage return when the max width is reached.

" Folding
" set foldmethod=syntax
" let c_no_comment_fold = 1    " Uncomment to stop comments from folding
" let c_no_if0_fold = 1        " Uncomment to stop ifs from folding

" Backups
if has("vms")
    set nobackup
else
    set backup
    set backupdir=~/.vimbackups,~/tmp/,/tmp/
    set backupcopy=yes
    set viewdir=$VIMHOME/views/
endif

" Syntax highlighting, highlight last search
if &t_Co > 2 || has("gui_running")
    colorscheme desert
    syntax on
    set hlsearch
endif

" In many terminal emulators the mouse works just find, thus enable it.
if v:version >= 700
    set mouse=a
endif

" This is only for tabs.  It will nuber the tabs and report how many windows
" are in each as well as rename them better
" if has("gui")
"     set guioptions-=e
" endif
if exists("+showtabline")
    function! MyTabLine()
        let s = ''
        let t = tabpagenr()
        let last = tabpagenr('$')
        let groupsize = 4
        let i = 1
        let max = t/groupsize + 1
        let max = max * groupsize
        let tabsepleft='['
        let tabsepright=']'
        while i <= tabpagenr('$')
            let buflist = tabpagebuflist(i)
            let winnr = tabpagewinnr(i)
            let s .= '%' . i . 'T'
            let s .= (i == t ? '%1*' : '%2*')
            let s .= ' '
            let s .= (i == t ? '%#TabLineSel#' : '%#TabLine#')
            let s .= tabsepleft . i . ':'
            let bufnr = buflist[winnr - 1]
            let file = bufname(bufnr)
            let buftype = getbufvar(bufnr, 'buftype')
            if buftype == 'nofile'
                if file =~ '\/.'
                    let file = substitute(file, '.*\/\ze.', '', '')
                endif
            else
                let file = fnamemodify(file, ':p:t')
            endif
            if file == ''
                let file = '[No Name]'
            endif
            let s .= file
            let s .= tabsepright
            let i = i + 1
            if i > max
                break
            endif
        endwhile
        let s .= "%T%#TabLineFill#%="
        let s .= (tabpagenr('$') > 1 ? '%999XX' : 'X')
        return s
    endfunction

    set stal=2
    set tabline=%!MyTabLine()
    hi TablineSel ctermbg=blue ctermfg=white
    hi TabLineFill ctermbg=white ctermfg=black
endif
EOF
    return $SUCCESS
else
    return $SKIP
fi