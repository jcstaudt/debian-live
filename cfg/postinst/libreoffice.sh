if $PERSONALIZE; then
    # Create a user dictionary
    mkdir -p $HOME/.config/libreoffice/4/user/wordbook
    if [ ! -d $HOME/.config/libreoffice/4/user/wordbook ]; then return $FAILURE; fi
    cat <<EOF > $HOME/.config/libreoffice/4/user/wordbook/standard.dic
OOoUserDict1
lang: <none>
type: positive
---
EOF
    # echo NewWordInUserDictionary >> $HOME/.config/libreoffice/4/user/wordbook/standard.dic
    return $SUCCESS
else
    return $SKIP
fi