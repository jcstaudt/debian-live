if $PERSONALIZE; then
    if [ -d "$GAMESAVE_MNT_PATH" ]; then
        if [ -f "$GAMESAVE_MNT_PATH/memcards/psx/card1.mcd" ]; then
            sed -i "s|Mcd1 = ${HOME}/.pcsxr/memcards/card1.mcd|Mcd1 = ${GAMESAVE_MNT_PATH}/memcards/psx/card1.mcd|g" $HOME/.pcsxr/pcsxr.cfg
        fi
    else
        return $FAILURE
    fi
    return $SUCCESS
else
    return $SKIP
fi
