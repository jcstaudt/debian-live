if $PERSONALIZE; then
    cat <<EOF > $HOME/.wgetrc
adjust_extension = on
convert_links = on
mirror = on
page_requisites = on
span_hosts = on
timestamping = on
verbose = off
EOF
    return $SUCCESS
else
    return $SKIP
fi