if $PERSONALIZE && $HOME_PROPERMOUNT; then
    mkdir -p $HOME/.config/evolution
    if [ ! -d $HOME/.config/evolution ]; then return $FAILURE; fi
    if ! update_symlink "$HOME_MNT_PATH/.config/evolution"      $HOME/.config/evolution;      then return $FAILURE; fi
    if ! update_symlink "$HOME_MNT_PATH/.local/share/evolution" $HOME/.local/share/evolution; then return $FAILURE; fi
    return $SUCCESS
else
    return $SKIP
fi