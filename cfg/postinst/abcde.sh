if $PERSONALIZE; then
    # Configure for flac output and post-rip CD ejection
    echo FLACENCODERSYNTAX=default >> $HOME/.abcde.conf
    echo FLAC=flac                 >> $HOME/.abcde.conf
    echo METAFLAC=metaflac         >> $HOME/.abcde.conf
    echo OUTPUTTYPE=flac           >> $HOME/.abcde.conf
    echo EJECTCD=y                 >> $HOME/.abcde.conf
    return $SUCCESS
else
    return $SKIP
fi