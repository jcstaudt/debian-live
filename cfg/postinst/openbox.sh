if $PERSONALIZE; then
    cat <<EOF > $HOME/.dmrc
[Desktop]
Language=en_US.utf8
Session=openbox
EOF
    return $SUCCESS
else
    return $SKIP
fi