if $PERSONALIZE && $HOME_PROPERMOUNT; then
    mkdir -p $HOME/.config/keepassxc
    if [ ! -d $HOME/.config/keepassxc ]; then return $FAILURE; fi
    # TODO: Do not hard-code database file location
    cat <<EOF > $HOME/.config/keepassxc/keepassxc.ini
[General]
ConfigVersion=2

[Browser]
CustomProxyLocation=
Enabled=true

[GUI]
TrayIconAppearance=monochrome-light

[PasswordGenerator]
AdditionalChars=
ExcludedChars=

[Security]
LockDatabaseIdle=true
EOF
    mkdir -p $HOME/.cache/keepassxc
    if [ ! -d $HOME/.cache/keepassxc ]; then return $FAILURE; fi
    # TODO: Do not hard-code database file location
    cat <<EOF > $HOME/.cache/keepassxc/keepassxc.ini
[General]
LastActiveDatabase=$HOME/Documents/keepassx.kdbx
LastDatabases=$HOME/Documents/keepassx.kdbx
LastOpenedDatabases=$HOME/Documents/keepassx.kdbx
EOF
    return $SUCCESS
else
    return $SKIP
fi