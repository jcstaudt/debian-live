if $NAS_WANTMOUNT; then
    if ! mount_nas $IP_NAS_ETH $NAS_ROOT; then
        echo_skip -e "$_CONFIG Not mounting NAS... network unavailable when configuring nfs-common"
        return $FAILURE
    fi
    return $SUCCESS
else
    return $SKIP
fi
