if $PERSONALIZE; then
    cp -r "$REPOROOT/home/.tiemu" $HOME
    return $SUCCESS
else
    return $SKIP
fi