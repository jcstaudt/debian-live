if $PERSONALIZE; then
    mkdir -p $HOME/.config/lxmusic
    if [ ! -d $HOME/.config/lxmusic ]; then return $FAILURE; fi
    cat <<EOF > $HOME/.config/lxmusic/config
[Main]
width=1095
height=578
xpos=0
ypos=0
show_tray_icon=1
close_to_tray=1
play_after_exit=0
show_playlist=1
filter=0
EOF
    return $SUCCESS
else
    return $SKIP
fi