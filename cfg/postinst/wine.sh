if $PERSONALIZE && $GAMES_PROPERMOUNT; then
    if ! update_symlink "$GAMES_MNT_PATH/.wine" $HOME/.wine; then return $FAILURE; fi
    return $SUCCESS
else
    return $SKIP
fi