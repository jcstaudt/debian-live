# Depends: net_survey - util/net.sh
if [ $(get_ethernet_interfaces | wc -l) -eq 2 ]; then
    cat <<EOF | sudo tee /etc/network/interfaces 1> /dev/null
auto lo
iface lo inet loopback

#allow-hotplug $ETH_WAN_IF
#iface $ETH_WAN_IF inet dhcp

#allow-hotplug $ETH_LAN_IF
#iface $ETH_LAN_IF inet dhcp

auto bond0
iface bond0 inet dhcp
    bond-slaves $ETH_LAN_IF
    bond-mode active-backup
    bond-miimon 100
    bond-downdelay 200
    bond-updelay 200
EOF
    sudo systemctl restart networking
    return $SUCCESS
else
    return $SKIP
fi