if pkg_installed ufw; then
    # https://wiki.archlinux.org/title/Waydroid#Network
    sudo ufw allow 53 1> /dev/null
    sudo ufw allow 67 1> /dev/null
    sudo ufw default allow FORWARD 1> /dev/null
    sudo systemctl restart ufw
fi
if $PERSONALIZE && $SW_INSTALL_WAYDROID; then
    # Link
    #
    local waydroid_first_run=false
    if [ ! -d "$WAYDROID_LOCAL" ]; then
        local waydroid_first_run=true
        mkdir -p "$WAYDROID_LOCAL"

        # Google Play certification
        # https://docs.waydro.id/faq/google-play-certification
    fi
    if [ ! -d "$WAYDROID_VAR" ]; then
        local waydroid_first_run=true
        mkdir -p "$WAYDROID_VAR/images"
    fi
    if ! update_symlink "$WAYDROID_LOCAL" $HOME/.local/share/waydroid; then return $FAILURE; fi
    if ! update_symlink "$WAYDROID_VAR" /var/lib/waydroid sudo;        then return $FAILURE; fi

    # Init
    #
    if $waydroid_first_run; then
        # Select VANILLA or GAPPS
        sudo waydroid init -s GAPPS -f

        # Use software rendering
        sed -i 's/ro.hardware.gralloc=gbm/ro.hardware.gralloc=default/g' /var/lib/waydroid/waydroid_base.prop
        sed -i 's/ro.hardware.egl=mesa/ro.hardware.egl=swiftshader/g' /var/lib/waydroid/waydroid_base.prop
    fi

    # https://source.puri.sm/-/snippets/1198#tips-tricks
    # redirect adb port to make it accessible for other devices (such as your PC)
    sudo iptables -A PREROUTING -t nat -p tcp --dport 5555 -j DNAT --to 192.168.240.112:5555
    sudo iptables -A FORWARD -p tcp -d 192.168.240.112 --dport 5555 -j ACCEPT


    # Start
    #
    # Helpful usages: https://wiki.archlinux.org/title/Waydroid#Usage
    sudo waydroid container start &
    waydroid session start &
    waydroid prop set persist.waydroid.suspend true

    # Launch the GUI
    #
    #waydroid show-full-ui

    return $SUCCESS
else
    return $SKIP
fi