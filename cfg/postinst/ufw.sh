if $PERSONALIZE; then
    sudo ufw limit 22/tcp 1> /dev/null
    sudo ufw allow 80/tcp 1> /dev/null
    sudo ufw allow 443/tcp 1> /dev/null
    sudo ufw default deny incoming 1> /dev/null
    sudo ufw default allow outgoing 1> /dev/null
    sudo ufw enable 1> /dev/null
    return $SUCCESS
else
    return $SKIP
fi