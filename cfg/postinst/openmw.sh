if $GAMES_PROPERMOUNT; then
    # TODO: Abstract these paths
    if ! update_symlink "$GAMES_MNT_PATH/Morrowind" /usr/games/Morrowind sudo; then return $FAILURE; fi
    if ! update_symlink "$GAMES_MNT_PATH/video-game-saves/save/pc/The Elder Scrolls III: Morrowind/openmw/config" $HOME/.config/openmw; then return $FAILURE; fi
    if ! update_symlink "$GAMES_MNT_PATH/video-game-saves/save/pc/The Elder Scrolls III: Morrowind/openmw/saves" $HOME/.local/share/openmw/saves; then return $FAILURE; fi
    return $SUCCESS
else
    return $SKIP
fi