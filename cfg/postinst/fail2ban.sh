if $PERSONALIZE; then
    cat <<EOF | sudo tee /etc/fail2ban/jail.conf 1> /dev/null
[DEFAULT]
ignoreip = 127.0.0.1/8 ::1
bantime = 3600
findtime = 600
maxretry = 5

[sshd]
enabled = true
EOF
    cat <<EOF | sudo tee /etc/fail2ban/jail.d/wordpress.conf 1> /dev/null
[wordpress]
enabled = true
filter = wordpress
logpath = /var/log/auth.log
maxretry = 3
port = http,https
bantime = 300
EOF
    sudo systemctl enable --now fail2ban > /dev/null 2>&1
    return $SUCCESS
else
    return $SKIP
fi