if $PERSONALIZE; then
    cat <<EOF > $HOME/.emacs
;;(load "vhdl_emacs.el")

;;(require 'smart-tab)
;;(global-smart-tab-mode 1)
;;(require 'smooth-scroll)
;;(smooth-scroll-mode t)

(global-set-key [f5] 'goto-line)
(global-set-key [f6] 'query-replace)
(global-set-key [f10] 'hexl-insert-hex-char)

(custom-set-variables
  ;; custom-set-variables was added by Custom -- don't edit or cut/paste it!
  ;; Your init file should contain only one such instance.
 '(indent-tabs-mode nil)
 '(vhdl-clock-edge-condition (quote function))
 '(vhdl-reset-active-high t)
 '(inhibit-startup-screen t))

(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 )
(setq column-number-mode t) ;; Enable column headers, right of line numbers
(setq c-default-style "linux" c-basic-offset 2)

;; MATLAB mode
(autoload 'matlab-mode "matlab" "MATLAB Editing Mode" t)
(add-to-list
 'auto-mode-alist
 '("\\.m$" . matlab-mode))
(setq matlab-indent-function t)
(setq matlab-shell-command "matlab")

(add-hook 'before-save-hook 'delete-trailing-whitespace)

(setq-default indent-tabs-mode nil) ;; Insert spaces instead of tabs
EOF
    return $SUCCESS
else
    return $SKIP
fi