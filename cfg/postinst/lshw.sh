if $PERSONALIZE; then
    # Show network hardware information
    alias netinfo='sudo lshw -class network'
    return $SUCCESS
else
    return $SKIP
fi