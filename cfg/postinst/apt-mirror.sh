if $PERSONALIZE && $REPO_PROPERMOUNT; then
    case $(print_distro_id) in
        "Debian")
            download_if_missing_from_mirror https updates.signal.org/desktop/apt/dists/xenial/main/binary-amd64/Packages
            download_if_missing_from_mirror http $UPSTREAM_MIRROR_PATH/dists/stable/main/i18n/Translation-en.bz2
            download_if_missing_from_mirror http $UPSTREAM_MIRROR_PATH/dists/testing/main/i18n/Translation-en.bz2
            download_if_missing_from_mirror http $UPSTREAM_MIRROR_PATH/dists/stable/main/Contents-all.gz
            download_if_missing_from_mirror http $UPSTREAM_MIRROR_PATH/dists/testing/main/Contents-all.gz
            download_if_missing_from_mirror http $UPSTREAM_MIRROR_PATH/dists/stable/contrib/i18n/Translation-en.bz2
            #download_if_missing_from_mirror http $UPSTREAM_MIRROR_PATH/dists/testing/contrib/i18n/Translation-en.xz
            download_if_missing_from_mirror http $UPSTREAM_MIRROR_PATH/dists/stable/contrib/Contents-all.gz
            download_if_missing_from_mirror http $UPSTREAM_MIRROR_PATH/dists/testing/contrib/Contents-all.gz
            download_if_missing_from_mirror http $UPSTREAM_MIRROR_PATH/dists/stable/non-free/i18n/Translation-en.bz2
            #download_if_missing_from_mirror http $UPSTREAM_MIRROR_PATH/dists/testing/non-free/i18n/Translation-en.xz
            download_if_missing_from_mirror http $UPSTREAM_MIRROR_PATH/dists/stable/non-free/Contents-all.gz
            download_if_missing_from_mirror http $UPSTREAM_MIRROR_PATH/dists/testing/non-free/Contents-all.gz

            # These values may differ from what you want the system to use
            mirror_components="main contrib non-free non-free-firmware"
            cat <<EOF | sudo tee /etc/apt/mirror.list 1> /dev/null
############# config ##################
#
# set base_path    $REPO_MNT_PATH
#
# set mirror_path  $base_path/mirror
# set skel_path    $base_path/skel
# set var_path     $base_path/var
# set cleanscript $var_path/clean.sh
# set defaultarch  <running host architecture>
# set postmirror_script $var_path/postmirror.sh
# set run_postmirror 0
set nthreads 20
set _tilde   0
#
############# end config ##############

# External repositories
deb http://dl.google.com/linux/chrome/deb/ stable main
deb https://packages.element.io/debian/ default main
deb https://packages.microsoft.com/repos/ms-teams stable main
deb https://packages.microsoft.com/repos/edge stable main
deb http://repository.spotify.com stable non-free
deb http://repository.spotify.com testing non-free
deb https://repo.fortinet.com/repo/forticlient/7.2/debian/ stable non-free
deb https://updates.signal.org/desktop/apt xenial main
deb https://repo.waydro.id/ bullseye main
deb https://brave-browser-apt-release.s3.brave.com/ stable main
deb http://download.opensuse.org/repositories/home:/bespokesynth/Debian_12/ /

deb http://$UPSTREAM_MIRROR_PATH stable $mirror_components
deb http://$UPSTREAM_MIRROR_PATH-security/ stable-security $mirror_components
deb http://$UPSTREAM_MIRROR_PATH stable-updates $mirror_components
#deb http://$UPSTREAM_MIRROR_PATH testing $mirror_components
#deb-src http://$UPSTREAM_MIRROR_PATH stable $mirror_components
#deb-src http://$UPSTREAM_MIRROR_PATH testing $mirror_components

# mirror additional architectures
#deb-alpha http://$UPSTREAM_MIRROR_PATH stable $mirror_components
#deb-alpha http://$UPSTREAM_MIRROR_PATH testing $mirror_components
deb-amd64 http://$UPSTREAM_MIRROR_PATH stable $mirror_components
#deb-amd64 http://$UPSTREAM_MIRROR_PATH testing $mirror_components
deb-arm64 http://$UPSTREAM_MIRROR_PATH stable $mirror_components
#deb-arm64 http://$UPSTREAM_MIRROR_PATH testing $mirror_components
#deb-armel http://$UPSTREAM_MIRROR_PATH stable $mirror_components
#deb-armel http://$UPSTREAM_MIRROR_PATH testing $mirror_components
deb-armhf http://$UPSTREAM_MIRROR_PATH stable $mirror_components
#deb-armhf http://$UPSTREAM_MIRROR_PATH testing $mirror_components
#deb-hppa http://$UPSTREAM_MIRROR_PATH stable $mirror_components
#deb-hppa http://$UPSTREAM_MIRROR_PATH testing $mirror_components
deb-i386 http://$UPSTREAM_MIRROR_PATH stable $mirror_components
#deb-i386 http://$UPSTREAM_MIRROR_PATH testing $mirror_components
#deb-ia64 http://$UPSTREAM_MIRROR_PATH stable $mirror_components
#deb-ia64 http://$UPSTREAM_MIRROR_PATH testing $mirror_components
#deb-m68k http://$UPSTREAM_MIRROR_PATH stable $mirror_components
#deb-m68k http://$UPSTREAM_MIRROR_PATH testing $mirror_components
#deb-mips http://$UPSTREAM_MIRROR_PATH stable $mirror_components
#deb-mips http://$UPSTREAM_MIRROR_PATH testing $mirror_components
#deb-mipsel http://$UPSTREAM_MIRROR_PATH stable $mirror_components
#deb-mipsel http://$UPSTREAM_MIRROR_PATH testing $mirror_components
#deb-powerpc http://$UPSTREAM_MIRROR_PATH stable $mirror_components
#deb-powerpc http://$UPSTREAM_MIRROR_PATH testing $mirror_components
#deb-s390 http://$UPSTREAM_MIRROR_PATH stable $mirror_components
#deb-s390 http://$UPSTREAM_MIRROR_PATH testing $mirror_components
#deb-sparc http://$UPSTREAM_MIRROR_PATH stable $mirror_components
#deb-sparc http://$UPSTREAM_MIRROR_PATH testing $mirror_components

clean http://$UPSTREAM_MIRROR_PATH
EOF
            ;;
        "PureOS")
            # i386 not supported

            # These values may differ from what you want the system to use
            mirror_components="main"
            cat <<EOF | sudo tee /etc/apt/mirror.list 1> /dev/null
############# config ##################
#
# set base_path    $REPO_MNT_PATH
#
# set mirror_path  $base_path/mirror
# set skel_path    $base_path/skel
# set var_path     $base_path/var
# set cleanscript $var_path/clean.sh
# set defaultarch  <running host architecture>
# set postmirror_script $var_path/postmirror.sh
# set run_postmirror 0
set nthreads 20
set _tilde   0
#
############# end config ##############

# External repositories
deb https://packages.microsoft.com/repos/ms-teams stable main
deb http://repository.spotify.com stable non-free
deb http://repository.spotify.com testing non-free
deb https://repo.fortinet.com/repo/forticlient/7.2/debian/ stable non-free
deb https://updates.signal.org/desktop/apt xenial main
deb http://dl.google.com/linux/chrome/deb/ stable main

deb http://$UPSTREAM_MIRROR_PATH byzantium $mirror_components
deb-amd64 http://$UPSTREAM_MIRROR_PATH byzantium $mirror_components
deb-arm64 http://$UPSTREAM_MIRROR_PATH byzantium $mirror_components

clean http://$UPSTREAM_MIRROR_PATH
EOF
            ;;
        *)
            return $FAILURE
            ;;
    esac

    # TODO: Make the target path configurable
    if ! update_symlink /var/lib/apt/lists $REPO_MOUNT_PATH/var-lib-apt-lists sudo; then return $FAILURE; fi

    return $SUCCESS
fi
return $SKIP