if $PERSONALIZE && $HOME_PROPERMOUNT; then
    mkdir -p $HOME/.ssh
    if [ ! -d $HOME/.ssh ]; then return $FAILURE; fi
    if ! update_symlink $HOME_MNT_PATH/.ssh/id_rsa     $HOME/.ssh/id_rsa;     then return $FAILURE; fi
    if ! update_symlink $HOME_MNT_PATH/.ssh/id_rsa.pub $HOME/.ssh/id_rsa.pub; then return $FAILURE; fi
    sudo systemctl enable --now ssh > /dev/null 2>&1
    return $SUCCESS
else
    return $SKIP
fi