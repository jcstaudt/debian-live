if $PERSONALIZE; then
    sudo systemctl start docker
    echo "TODO: docker.io config: automatically log in to docker container registry"
    return $SUCCESS
else
    return $SKIP
fi