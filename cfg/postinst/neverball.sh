if $PERSONALIZE && $GAMES_PROPERMOUNT; then
    if ! update_symlink "$PC_GAMESAVE_MNT_PATH/Neverball" $HOME/.neverball; then return $FAILURE; fi
    return $SUCCESS
else
    return $SKIP
fi