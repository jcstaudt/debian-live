if $PERSONALIZE; then
    # Copy files with stats
    alias rsync='rsync -chavzP --append-verify --stats'

    # Don't need to compress, etc, files when xferring locally
    alias rsync-local='rsync --archive --checksum --delete --force --partial --progress --stats --verbose'
    return $SUCCESS
else
    return $SKIP
fi