if $PERSONALIZE && $HOME_PROPERMOUNT; then
    mkdir -p $HOME/.kp
    if [ ! -d $HOME/.kp ]; then return $FAILURE; fi
    # TODO: Do not hard-code database file location
    cat <<EOF > $HOME/.kp/config.ini
[default]
KEEPASSDB=$HOME/Documents/keepassx.kdbx
KEYPASSDB_TIMEOUT=45
EOF
    return $SUCCESS
else
    return $SKIP
fi