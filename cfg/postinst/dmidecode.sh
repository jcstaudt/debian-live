if $PERSONALIZE; then
    # Check if ECC memory installed
    alias chkeccmem="sudo dmidecode --type memory | grep 'Correction Type'"

    product=$(sudo dmidecode -t system | grep Product)
    if [ -z "$product" ]; then return $FAILURE; fi
    case "${product##*: }" in
        "a64")
            # Aliases specific to the OLIMEX A64-OLinuXino

            # Output CPU temp (A64-OLinuXino)
            alias temp="cat /sys/devices/virtual/thermal/thermal_zone0/temp"
            ;;
        "librem_13v4")
            # Aliases specific to the Purism Librem 13
            alias temp="cat /sys/class/hwmon/hwmon0/temp1_input && cat /sys/class/hwmon/hwmon1/temp1_input"

            # Check/set laptop monitor brightness (may not work for ext. monitor)
            blpath="/sys/class/backlight/intel_backlight/brightness"
            alias bright="cat $blpath"
            ;;
        "teres")
            # Aliases specific to the OLIMEX TERES-I

            # Backlight for TERES-I only works as of Linux 5.2 kernel
            blpath="/sys/class/backlight/backlight/brightness"
            alias bright="cat $blpath"
            alias bright0="echo 0 > $blpath"
            alias bright1="echo 1 > $blpath"
            alias bright2="echo 2 > $blpath"
            alias bright3="echo 3 > $blpath"
            alias bright4="echo 4 > $blpath"
            alias bright5="echo 5 > $blpath"
            alias bright6="echo 6 > $blpath"
            alias bright7="echo 7 > $blpath"
            alias bright8="echo 8 > $blpath"
            alias bright9="echo 9 > $blpath"
            alias bright10="echo 10 > $blpath"
            ;;
    esac
    return $SUCCESS
else
    return $SKIP
fi