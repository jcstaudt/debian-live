# Depends: util/fs.sh - check_substring_exists
if $PERSONALIZE && check_substring_exists $COMPONENTS "non-free"; then
    # Enable playback of proprietary DVDs
    if ! (task_install automake); then
        return $FAILURE
    fi
    if ! (task_install git); then
        return $FAILURE
    fi
    if ! (task_install libtool); then
        return $FAILURE
    fi

    destpath=/tmp/libdvdcss.tmp
    if $REPO_PROPERMOUNT && [ -d "$REPO_MNT_PATH/libdvdcss.git" ]; then
        git clone "$REPO_MNT_PATH/libdvdcss.git" $destpath > /dev/null 2>&1
    elif $INTERNET_CONNECTED; then
        git clone https://code.videolan.org/videolan/libdvdcss.git $destpath > /dev/null 2>&1
    fi
    if [ ! -d $destpath ]; then
        return $FAILURE
    fi

    pushd $destpath > /dev/null
    make clean > /dev/null 2>&1
    git checkout tags/1.4.3 > /dev/null 2>&1
    autoreconf -i > /dev/null 2>&1
    ./configure --prefix=/usr > /dev/null
    make > /dev/null
    sudo make install > /dev/null
    popd > /dev/null
    rm -rf $destpath
    echo "REMINDER: Please restart brasero between rips to improve success rates"
    return $SUCCESS
else
    return $SKIP
fi