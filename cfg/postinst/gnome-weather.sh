if $PERSONALIZE; then
    gsettings set org.gnome.Weather locations "[<(uint32 2, <('Portland', 'KPDX', true, [(0.79571014457688405, -2.1397785149603687)], [(0.7945341242735976, -2.1411037260081156)])>)>, <(uint32 2, <('San Antonio', 'KSSF', true, [(0.51206021192714668, -1.7186548090774469)], [(0.5135478084084989, -1.7190381008344775)])>)>]"
    return $SUCCESS
else
    return $SKIP
fi