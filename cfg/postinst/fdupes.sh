if $PERSONALIZE; then
    alias fdupes="fdupes -rS" # Recursive; show size
    return $SUCCESS
else
    return $SKIP
fi