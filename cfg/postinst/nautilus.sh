if $PERSONALIZE; then
    # Set configurations
    gsettings set org.gnome.nautilus.compression default-compression-format "tar.xz"
    gsettings set org.gnome.nautilus.preferences show-hidden-files          true
    gsettings set org.gnome.nautilus.preferences default-folder-viewer      "list-view"
    gsettings set org.gnome.nautilus.preferences executable-text-activation "launch"
    gsettings set org.gnome.nautilus.preferences recursive-search           "always"
    gsettings set org.gnome.nautilus.preferences show-create-link           true
    gsettings set org.gnome.nautilus.preferences show-delete-permanently    true
    gsettings set org.gnome.nautilus.preferences show-directory-item-counts "always"
    gsettings set org.gnome.nautilus.preferences show-image-thumbnails      "always"
    #gsettings set org.gnome.nautilus.preferences sort-folders-first         true
    gsettings set org.gnome.nautilus.list-view   default-zoom-level         "small"

    # Add bookmarks
    local cfgpath="$HOME/.config/gtk-3.0/bookmarks"
    if $GAMES_PROPERMOUNT; then
        append_unique_string_to_file "file://$GAMES_MNT_PATH" $cfgpath
    fi
    if $VM_PROPERMOUNT; then
        append_unique_string_to_file "file://$VM_MNT_PATH" $cfgpath
    fi
    if [ -d "$NAS_ROOT" ]; then
        append_unique_string_to_file "file://$NAS_ROOT" $cfgpath
    fi
    return $SUCCESS
else
    return $SKIP
fi