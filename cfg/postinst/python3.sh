if $PERSONALIZE; then
    cat <<EOF > $HOME/.pythonrc
# ~/.pythonrc
# enable syntax completion
try:
    import readline
except ImportError:
    print("Module readline not available.")
else:
    import rlcompleter
    readline.parse_and_bind("tab: complete")
EOF
    export PYTHONSTARTUP=$HOME/.pythonrc # Used for python auto-completion
    return $SUCCESS
else
    return $SKIP
fi