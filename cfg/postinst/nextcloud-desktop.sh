if $PERSONALIZE && $HOME_PROPERMOUNT; then
    if ! update_symlink "$HOME_MNT_PATH/Nextcloud" $HOME/Nextcloud; then return $FAILURE; fi
    mkdir -p $HOME/.config/autostart
    if [ ! -d $HOME/.config/autostart ]; then return $FAILURE; fi
    cat <<EOF > $HOME/.config/autostart/com.nextcloud.desktopclient.nextcloud.desktop
[Desktop Entry]
Name=Nextcloud
GenericName=File Synchronizer
Exec="/usr/bin/nextcloud" --background
Terminal=false
Icon=Nextcloud
Categories=Network
Type=Application
StartupNotify=false
X-GNOME-Autostart-enabled=true
EOF
    if $NAS_PROPERMOUNT; then
        # TODO: Doesn't seem to work yet; may not have this var at this point
        if ! update_symlink "/run/$USER/1000/gvfs/dav:host=$NEXTCLOUD_DOMAIN,ssl=true,user=$NEXTCLOUD_USER,prefix=%2Fnextcloud%2Fremote.php%2Fwebdav" $HOME/Nextcloud.webdav; then return $FAILURE; fi
    fi
    return $SUCCESS
else
    return $SKIP
fi