if $PERSONALIZE; then
    gsettings set org.gnome.gnome-system-monitor show-dependencies true
    return $SUCCESS
else
    return $SKIP
fi