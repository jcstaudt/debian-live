if $PERSONALIZE; then
    if pkg_installed lxde; then
        lxpanelprofile=LXDE
        lxpanelicon=/usr/share/lxde/images/lxde-icon.png
    else
        lxpanelprofile=default
        lxpanelicon=/usr/share/lxpanel/images/cpufreq-icon.png
    fi

    mkdir -p $HOME/.config/lxpanel/$lxpanelprofile/panels
    if [ ! -d $HOME/.config/lxpanel/$lxpanelprofile/panels ]; then return $FAILURE; fi

    cat <<EOF > $HOME/.config/lxpanel/launchtaskbar.cfg
[special_cases]
synaptic=synaptic-pkexec
soffice.bin=libreoffice
x-terminal-emulator=lxterminal
EOF
    cat <<EOF > $HOME/.config/lxpanel/$lxpanelprofile/config
[Command]
Logout=lxde-logout
EOF
    cat <<EOF > $HOME/.config/lxpanel/$lxpanelprofile/panels/panel
# lxpanel <profile> config file. Manually editing is not recommended.
# Use preference dialog in lxpanel to adjust config when you can.

Global {
  align=left
  alpha=255
  autohide=0
  background=0
  backgroundfile=/usr/share/lxpanel/images/background.png
  edge=bottom
  fontcolor=#ffffff
  fontsize=10
  height=16
  heightwhenhidden=0
  iconsize=16
  margin=0
  setdocktype=1
  setpartialstrut=1
  tintcolor=#000000
  transparent=1
  usefontcolor=1
  usefontsize=0
  width=100
  widthtype=percent
}
Plugin {
  type=menu
  Config {
    image=$lxpanelicon
    system {
    }
    separator {
    }
    item {
      command=run
    }
    separator {
    }
    item {
      image=gnome-logout
      command=logout
    }
  }
}
Plugin {
  type=launchbar
  Config {
    Button {
      id=pcmanfm.desktop
    }
    Button {
      id=lxde-x-terminal-emulator.desktop
    }
    Button {
      id=lxde-x-www-browser.desktop
      image=/usr/share/icons/hicolor/128x128/apps/firefox-esr.png
    }
  }
}
Plugin {
  type=pager
  Config {
  }
}
Plugin {
  type=taskbar
  expand=1
  Config {
    tooltips=-1
    IconsOnly=0
    AcceptSkipPager=1
    ShowIconified=1
    ShowMapped=1
    ShowAllDesks=0
    UseMouseWheel=0
    UseUrgencyHint=1
    FlatButton=0
    MaxTaskWidth=150
    spacing=1
    GroupedTasks=1
  }
}
Plugin {
  type=weather
  Config {
    alias=London, UK
    city=London
    state=England
    country=United Kingdom
    units=c
    interval=20
    enabled=0
    latitude=51.507336
    longitude=-0.127650
    provider=openweathermap
  }
}
Plugin {
  type=tray
  Config {
  }
}
Plugin {
  type=space
  Config {
  }
  expand=0
}
Plugin {
  type=volume
  Config {
  }
}
Plugin {
  type=thermal
  Config {
    NormalColor=#00ff00
    Warning1Color=#fff000
    Warning2Color=#ff0000
    AutomaticLevels=1
    Warning1Temp=100
    Warning2Temp=105
    AutomaticSensor=1
  }
}
Plugin {
  type=space
  Config {
  }
}
Plugin {
  type=monitors
  Config {
    DisplayCPU=1
    DisplayRAM=1
    CPUColor=#00FF00
    RAMColor=#FF0000
  }
}
Plugin {
  type=netstatus
  Config {
    iface=wlp1s0
    configtool=nm-connection-editor
  }
}
Plugin {
  type=dclock
  Config {
    ClockFmt=%R
    TooltipFmt=%A %x
    BoldFont=0
    IconOnly=0
    CenterText=0
  }
}
Plugin {
  type=launchbar
  Config {
    Button {
      id=lxde-screenlock.desktop
    }
    Button {
      id=lxde-logout.desktop
    }
  }
}
Plugin {
  type=wincmd
  Config {
    Button1=iconify
    Button2=shade
  }
}
EOF
    return $SUCCESS
else
    return $SKIP
fi