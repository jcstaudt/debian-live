if $PERSONALIZE; then
    # Configure time zone
    sudo ln -fs /usr/share/zoneinfo/$TIME_ZONE /etc/localtime
    sudo dpkg-reconfigure --frontend=noninteractive tzdata > /dev/null 2>&1
    return $SUCCESS
else
    return $SKIP
fi