if $PERSONALIZE; then
    if $ENABLE_ETH; then
        alias scaneth="sudo arp-scan --interface=$ETH_LAN_IF --localnet"
    elif $ENABLE_WIFI; then
        alias scaneth="sudo arp-scan --interface=$WIFI_IF --localnet"
    else
        return $FAILURE
    fi
    return $SUCCESS
else
    return $SKIP
fi