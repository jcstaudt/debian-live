if $PERSONALIZE; then
    if $NAS_PROPERMOUNT; then
        prep="sudo"
        outputroot="$NAS_ROOT/internet/youtube.com"
        rmdir $HOME/Downloads/yt-dlp 2> /dev/null # Created during installation
    else
        prep=""
        outputroot="$HOME/Downloads/yt-dlp"
    fi
    $prep mkdir -p $outputroot
    mkdir -p $HOME/.config/yt-dlp
    if [ ! -d $HOME/.config/yt-dlp ]; then return $FAILURE; fi
    cat <<EOF > $HOME/.config/yt-dlp/config
## General
--ignore-errors                                       # Skip unavailable videos on a playlist

## Filesystem
--batch-file $outputroot/yt-dlp.urls                  # List of videos to download

# Download all playlists of YouTube channel/user keeping each playlist in separate directory:
# --output '%(uploader)s/%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s'
--output '$outputroot/%(uploader)s/%(title)s.%(ext)s' # Download video under directory of uploader

--no-overwrites                                       # Do not overwrite files
--continue                                            # Resume partial files if possible
--write-description                                   # Write metadata to a .description file
--write-info-json                                     # Write metadata to a .info.json file
--write-annotations                                   # Create an .annotations.xml file
--cookies $outputroot/youtube.com_cookies.txt         # Use Chrome's "Get Cookies" extension

## Thumbnail
--write-all-thumbnails                                # Download all thumbnail image formats

## Video format
# Download best webm (video-only+audio-only) format available
# OR       best webm video+audio format available
# OR       any other best if nothing previous available
--format 'bestvideo[vcodec=vp9]+bestaudio[acodec=opus]/best[ext=webm]/best'

# yt-dlp --list-formats <video-url> # List available formats

## Subtitle
--all-subs                                            # Download all available subtitles

## Post-processing
--no-post-overwrites                                  # Do not overwrite post-processed files
--embed-subs                                          # Embed subtitles in mp4/webm/mkv
--embed-thumbnail                                     # Embed thumbnail in audio as cover art
--add-metadata                                        # Write metadata to the video file

# To purge yt-dlp cache (if it won't redownload a file because it exists):
# yt-dlp --rm-cache-dir --ignore-config

## References
# https://github.com/ytdl-org/youtube-dl#format-selection
EOF
    unset prep
    unset outputroot
    return $SUCCESS
else
    return $SKIP
fi