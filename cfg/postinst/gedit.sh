if $PERSONALIZE; then
    gsettings set org.gnome.gedit.preferences.ui     statusbar-visible       false
    gsettings set org.gnome.gedit.preferences.editor display-right-margin    true
    gsettings set org.gnome.gedit.preferences.editor restore-cursor-position false
    gsettings set org.gnome.gedit.preferences.editor bracket-matching        false
    gsettings set org.gnome.gedit.preferences.editor scheme                  "oblivion"
    gsettings set org.gnome.gedit.preferences.editor editor-font             "Monospace 12"
    gsettings set org.gnome.gedit.preferences.editor insert-spaces           true
    gsettings set org.gnome.gedit.preferences.editor tabs-size               "uint32 4"
    gsettings set org.gnome.gedit.preferences.editor ensure-trailing-newline false
    gsettings set org.gnome.gedit.preferences.editor auto-indent             false
    gsettings set org.gnome.gedit.preferences.editor max-undo-actions        2048
    return $SUCCESS
else
    return $SKIP
fi