if $INTERNET_CONNECTED; then
    sudo ntpdate $NTP_SERVER > /dev/null
    return $SUCCESS
else
    echo_failure -e "$_CONFIG Unable to connect to NTP server \"$NTP_SERVER\"... no internet connection"
    return $FAILURE
fi
