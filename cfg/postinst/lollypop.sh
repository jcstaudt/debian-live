if $PERSONALIZE && $HOME_PROPERMOUNT; then
    if ! update_symlink "$HOME_MNT_PATH/.local/share/lollypop" $HOME/.local/share/lollypop; then return $FAILURE; fi
    if ! update_symlink "$HOME_MNT_PATH/.cache/lollypop"       $HOME/.cache/lollypop;       then return $FAILURE; fi

    # Avoid permissions issues as different live images use different usernames
    # Persistent data will not update on its own
    sudo chown -R $USER ~/.local/share/lollypop/
    sudo chown -R $USER ~/.cache/lollypop/

    return $SUCCESS
else
    return $SKIP
fi