if $PERSONALIZE; then
    gsettings set org.gnome.Maps osm-username $OSM_USERNAME
    return $SUCCESS
else
    return $SKIP
fi