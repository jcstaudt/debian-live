# Depends: cfg/echo.sh - PRINT_BLANK, PRINT_FAILURE, PRINT_SKIP, PRINT_SUCCESS

case $TERM in
    "linux")
        # Cannot print '𐄂' or '✓'
        CHAR_NO="N"
        CHAR_YES="Y"
        ;;
    *)
        CHAR_NO="𐄂"  # '\U10102'
        CHAR_YES="✓" # '\U2713'
        ;;
esac

init_echo_prepends() {
    _CONFIG="   CONFIG:"
    _DOWNLOAD=" DOWNLOAD:"
    _INSTALL="  INSTALL:"
    _NETWORK="  NETWORK:"
    _UNINSTALL="UNINSTALL:"
    _UPDATE="   UPDATE:"
    _UPGRADE="  UPGRADE:"
}

################################################################################
# Echo functions

echo_usage() {
    echo "USAGE: $0 wlan_ssid wlan_passphrase"
}

# Usage: echo_script_cfg_error <opt:${FUNCNAME[0]}> <opt:description>
echo_script_cfg_error() {
    >&2 echo -n "ERROR: Script configuration error"
    if [ $# -gt 0 ]; then >&2 echo -n " in function $1"; else >&2 echo ""; fi
    if [ $# -gt 1 ]; then >&2 echo -n "... $2"; else >&2 echo ""; fi
}

echo_blank() {
    if ! $PRINT_BLANK; then
        return $SKIP
    fi
    case $# in
        1)
            # Standard echo
            echo "[ ] $1"
            ;;
        2)
            # Could want either -e or -n flags
            case $1 in
                "-e")
                    echo_delete_line
                    echo "[ ] $2"
                    ;;
                "-n")
                    echo -n "[ ] $2"
                    ;;
                *)
                    return $FAILURE
                    ;;
            esac
            ;;
        3)
            # Want both -e and -n flags
            echo -e -n "[ ] $3"
            ;;
        *)
            return $FAILURE
            ;;
    esac
    return $SUCCESS
}

# Clear the current output line
echo_delete_line() {
    echo -e -n "\033[2K\r"
}

echo_failure() {
    if ! $PRINT_FAILURE; then
        return $SKIP
    fi
    case $# in
        1)
            # Standard echo
            >&2 echo "[$CHAR_NO] $1"
            ;;
        2)
            # Could want either -e or -n flags
            case $1 in
                "-e")
                    echo_delete_line
                    >&2 echo -e "[$CHAR_NO] $2"
                    ;;
                "-n")
                    >&2 echo -n "[$CHAR_NO] $2"
                    ;;
                *)
                    return $FAILURE
                    ;;
            esac
            ;;
        3)
            # Want both -e and -n flags
            >&2 echo -e -n "[$CHAR_NO] $3"
            ;;
        *)
            return $FAILURE
            ;;
    esac
    return $SUCCESS
}

echo_skip() {
    if ! $PRINT_SKIP; then
        return $SKIP
    fi
    case $# in
        1)
            # Standard echo
            echo "[-] $1"
            ;;
        2)
            # Could want either -e or -n flags
            case $1 in
                "-e")
                    echo_delete_line
                    echo -e "[-] $2"
                    ;;
                "-n")
                    echo -n "[-] $2"
                    ;;
                *)
                    return $FAILURE
                    ;;
            esac
            ;;
        3)
            # Want both -e and -n flags
            echo -e -n "[-] $3"
            ;;
        *)
            return $FAILURE
            ;;
    esac
    return $SUCCESS
}

echo_success() {
    if ! $PRINT_SUCCESS; then
        return $SKIP
    fi
    case $# in
        1)
            # Standard echo
            echo "[$CHAR_YES] $1"
            ;;
        2)
            # Could want either -e or -n flags
            case $1 in
                "-e")
                    echo_delete_line
                    echo -e "[$CHAR_YES] $2"
                    ;;
                "-n")
                    echo -n "[$CHAR_YES] $2"
                    ;;
                *)
                    return $FAILURE
                    ;;
            esac
            ;;
        3)
            # Want both -e and -n flags
            echo -e -n "[$CHAR_YES] $3"
            ;;
        *)
            return $FAILURE
            ;;
    esac
    return $SUCCESS
}

echo_graphic() {
    if [ -z "$1" ]; then
        echo -n "[!]"
    elif $1; then
        echo -n "[$CHAR_YES]"
    else
        echo -n "[$CHAR_NO]"
    fi
}

print_menu() {
    echo "STORAGE [EXISTS] [WANT MOUNTED] [MOUNTED] [MOUNTED AT DESIRED LOCATION]"

    echo -n  "-> $(echo_graphic $GAMES_EXISTS)$(echo_graphic $GAMES_WANTMOUNT)$(echo_graphic $GAMES_MOUNTED)$(echo_graphic $GAMES_PROPERMOUNT) "
    echo -en "LABEL=\"$GAMES_PARTITION_LABEL\"\t"
    echo -en "DEVICE=\"$GAMES_PARTITION_DEV\"\t\t"
    echo -e  "MOUNTPOINT=\"$GAMES_MNT_PATH\""

    echo -n  "-> $(echo_graphic $HOME_EXISTS)$(echo_graphic $HOME_WANTMOUNT)$(echo_graphic $HOME_MOUNTED)$(echo_graphic $HOME_PROPERMOUNT) "
    echo -en "LABEL=\"$HOME_PARTITION_LABEL\"\t"
    echo -en "DEVICE=\"$HOME_PARTITION_DEV\"\t\t"
    echo -e  "MOUNTPOINT=\"$HOME_MNT_PATH\""

    echo -n  "-> $(echo_graphic $REPO_EXISTS)$(echo_graphic $REPO_WANTMOUNT)$(echo_graphic $REPO_MOUNTED)$(echo_graphic $REPO_PROPERMOUNT) "
    echo -en "LABEL=\"$REPO_PARTITION_LABEL\"\t"
    echo -en "DEVICE=\"$REPO_PARTITION_DEV\"\t\t"
    echo -e  "MOUNTPOINT=\"$REPO_MNT_PATH\""

    echo -n  "-> $(echo_graphic $STEAM_LUN_EXISTS)$(echo_graphic $STEAM_LUN_WANTMOUNT)$(echo_graphic $STEAM_LUN_MOUNTED)$(echo_graphic $STEAM_LUN_PROPERMOUNT) "
    echo -en "LABEL=\"$STEAM_LUN_PARTITION_LABEL\"\t"
    echo -en "DEVICE=\"$STEAM_LUN_PARTITION_DEV\"\t\t"
    echo -e  "MOUNTPOINT=\"$STEAM_LUN_MNT_PATH\""

    echo -n  "-> $(echo_graphic $VM_EXISTS)$(echo_graphic $VM_WANTMOUNT)$(echo_graphic $VM_MOUNTED)$(echo_graphic $VM_PROPERMOUNT) "
    echo -en "LABEL=\"$VM_PARTITION_LABEL\"\t"
    echo -en "DEVICE=\"$VM_PARTITION_DEV\"\t\t"
    echo -e  "MOUNTPOINT=\"$VM_MNT_PATH\""

    echo -e "\nNETWORK"
    echo -e "-> Connection: internet\t\t$(echo_graphic $INTERNET_CONNECTED)"
    echo -e "|  -> Connection method\t\t$INTERNET_CONNECTION_METHOD"
    case $INTERNET_CONNECTION_METHOD in
        "ethernet")
                    echo -e "|  -> Interface (WAN)\t\t$ETH_WAN_IF"
                    echo -e "|  -> Interface (LAN)\t\t$ETH_LAN_IF"
                    ;;
        "wireless")
                    echo -e "|  -> Interface\t\t\t$WIFI_IF"
                    echo -e "|  -> Active SSID\t\t\"$ACTIVE_SSID\""
                    ;;
    esac
    if [ "$INTERNET_CONNECTION_METHOD" = "wireless" ]; then
        echo -e "|  -> Active SSID\t\t\"$ACTIVE_SSID\""
    fi
    echo -e  "-> NAS\t\t\t\t$(echo_graphic $NAS_EXISTS)$(echo_graphic $NAS_WANTMOUNT)$(echo_graphic $NAS_MOUNTED)$(echo_graphic $NAS_PROPERMOUNT)"
    echo -e  "-> Metered network\t\t$(echo_graphic $METERED_NETWORK)"
    echo -e  "-> Enable Bluetooth\t\t$(echo_graphic $USE_BLUETOOTH)"
    echo -en "-> Enable WiFi"
    if [ -n "$WIFI_IF" ]; then echo -en " ($WIFI_IF)"; else echo -en "\t"; fi
    echo -e  "\t\t$(echo_graphic $ENABLE_WIFI)"
    if $ENABLE_WIFI; then
        echo -e "-> Configure networking\t\t$(echo_graphic $config_wlan)"
        if $config_wlan; then
            echo -e "  -> WLAN SSID\t\t\t\"$wlan_ssid\""
            echo -e "  -> WLAN passphrase\t\t\"$wlan_pass\""
        fi
    fi
    echo ""

    echo SOFTWARE
    echo -e "-> Preset\t\t\t\"$PRESET\""
    echo -e "-> Use repositories\t\t\"$COMPONENTS\" in \"$SUITE\""
    echo -e "-> Update\t\t\t$(echo_graphic $SW_UPDATE)"
    echo -e "-> Install\t\t\t$(echo_graphic $SW_INSTALL)"
    echo -e "|  -> apt-mirror\t\t$(echo_graphic $SW_INSTALL_MIRROR)"
    echo -e "|  -> Design\t\t\t$(echo_graphic $SW_INSTALL_DESIGN)"
    echo -e "|  -> Dev\t\t\t$(echo_graphic $SW_INSTALL_DEV)"
    echo -e "|  -> docker\t\t\t$(echo_graphic $SW_INSTALL_DOCKER)"
    echo -e "|  -> Games\t\t\t$(echo_graphic $SW_INSTALL_GAMES)"
    echo -e "|  |   -> steam\t\t\t$(echo_graphic $SW_INSTALL_STEAM)"
    echo -e "|  -> Media\t\t\t$(echo_graphic $SW_INSTALL_MEDIA)"
    echo -e "|  -> Social\t\t\t$(echo_graphic $SW_INSTALL_SOCIAL)"
    echo -e "|  -> VM environment\t\t$(echo_graphic $SW_INSTALL_VM)"
    echo -e "|      -> Headless\t\t$(echo_graphic $VM_HEADLESS)"
    echo -e "->Remove\t\t\t$(echo_graphic $SW_PURGE)"
    echo -e "->Upgrade\t\t\t$(echo_graphic $SW_UPGRADE)"
    echo    ""

    echo PERSONALIZATION
    echo -e "-> Persistent home\t\t$(echo_graphic $PERSISTENT_HOME)"
    echo -e "-> Personalize\t\t\t$(echo_graphic $PERSONALIZE)"
    if $PERSONALIZE; then
        echo -e "  -> Name\t\t\t\"$NAME\""
        echo -e "  -> Email\t\t\t\"$EMAIL\""
        echo -e "  -> Brightness (0.0-1.0)\t$BRIGHTNESS"
        echo -e "  -> Night light\t\t$NIGHT_LIGHT"
        echo -e "  -> Time zone\t\t\t\"$TIME_ZONE\""
    fi
    echo ""
}
