# Depends: cfg/cfg.sh
# Depends: util/echo.sh - echo_script_cfg_error
# Depends: util/init.sh - initialize

# Usage: add_arch <architecture>
add_arch() {
    echo_blank -n "$_CONFIG Adding architecture \"$1\"..."
    sudo dpkg --add-architecture $1
    echo_success -e "$_CONFIG Added architecture \"$1\""
}

# Usage: append_unique_string_to_file <string-to-insert> <filepath> <opt:sudo>
append_unique_string_to_file() {
    if [ $# -eq 3 ] && [ "$3" = "sudo" ]; then
        prep="sudo"
    else
        prep=""
    fi
    if ! grep -q "$1" $2; then
        echo $1 | $prep tee -a $2 > /dev/null
        unset prep
        return $SUCCESS
    else
        unset prep
        return $FAILURE
    fi
}

# Display
# TODO: Test this function on a variety of computers
calibrate_display() {
    displayname="$(xrandr -q | grep " connected" | cut -d' ' -f1)"
    modeline=$(cvt $DISPLAY_RES | grep Modeline | cut -d' ' -f2-)
    #echo $displayname
    #echo $modeline
    xrandr --newmode $modeline
    xrandr --addmode $displayname $(echo $modeline | cut -d' ' -f1)
    unset displayname modeline
}

# Usage: create_custom_command <command name>
create_custom_command() {
    if [ $# -ne 1 ]; then
        echo_script_cfg_error ${FUNCNAME[0]}
        return $FAILURE
    fi

    echo_blank -n "$_CONFIG Creating command $1..."
    destpath=/usr/bin/$1
    if [ -f $destpath ]; then
        echo_skip -e "$_CONFIG Not creating command $1... already exists"
        unset destpath
        return $FAILURE
    fi
    case $1 in
        "blankscreen")
            cmdstr='setterm --blank=force; read ans; setterm --blank=poke'
            ;;
        *)
            echo_failure -e "$_CONFIG No custom command configured for $1"
            unset destpath
            return $FAILURE
            ;;
    esac
    echo -e '#!/bin/bash\n'$cmdstr | sudo tee $destpath > /dev/null
    unset cmdstr
    if [ -f $destpath ]; then
        sudo chmod 755 $destpath
        unset destpath
        echo_success -e "$_CONFIG Created custom command $1"
        return $SUCCESS
    else
        unset destpath
        echo_failure -e "$_CONFIG Failed to create custom command $1"
        return $FAILURE
    fi
}

# Usage:   create_swapfile <swapfile-path> <capacity-in-GB>
# Purpose: Enable swapfile to mitigate low memory concerns
# NOTE:    When booting a live image, this file must reside on a mounted drive
#          if > ~3GB
create_swapfile() {
    echo_blank -n "$_CONFIG Creating swapfile ($2G) at path \"$1\"..."
    if [ $# -ne 2 ]; then
        echo_script_cfg_error ${FUNCNAME[0]}
        return $FAILURE
    fi
    if [ -f $1 ]; then
        sudo rm $1
    fi
    sudo mkdir -p $(dirname $1)
    sudo fallocate -l $2G $1
    sudo chmod 600 $1
    sudo mkswap $1 1> /dev/null
    # TODO: Check for success before asserting that it was properly created
    echo_success -e "$_CONFIG Created swapfile ($2G) at path \"$1\""
}

# Usage: enable_swapfile <swap_file_path>
enable_swapfile() {
    echo_blank -n "$_CONFIG Enabling swapfile (\"$1\")..."
    sudo swapon "$1"
    echo_success -e "$_CONFIG Enabled swapfile (\"$1\")"
}

# Check if a substring exists within a string
# Usage: check_substring_exists <string> <substring>
check_substring_exists() {
    if [ -z "${1##*$2*}" ]; then return $SUCCESS; else return $FAILURE; fi
}

is_a_broken_symlink() {
    if [ -L $1 ] && [ ! -d $1/ ]; then return $SUCCESS; else return $FAILURE; fi
}

# Check to see if a string exists within a list of strings
# Usage: list_contains <list> <query>
list_contains() {
    if [ $# -lt 2 ]; then
        echo_script_cfg_error ${FUNCNAME[0]}
        return $FAILURE
    fi
    list=("$@")
    # TODO: Maybe there is a better way to ignore the last element in a list
    query="${list[@]:(-1)}"
    unset 'list[${#list[@]}-1]'
    for i in "${list[@]}"; do
        if [ "$query" = "$i" ]; then
            unset list query
            return $SUCCESS
        fi
    done
    unset list query
    return $FAILURE
}

# Description: Get the number of elements in a list i.e. var=( a b c )
# Usage:       list_length <list>
list_length() {
    list=("$@")
    echo ${#list[@]}
    unset list
}

# Description: Check if there are any non-null elements in a list
list_not_empty() {
    list=("$@")
    for i in "${list[@]}"; do
        if [ -n "$i" ]; then
            unset list
            return $SUCCESS # may return on the first positive match
        fi
    done
    unset list
    return $FAILURE
}

# Description: Pause the script until <return> is pressed (w/o using `read` cmd)
# Usage: press_enter_to_continue <opt: string-to-print-before-waiting>
press_enter_to_continue() {
    if [ $# -eq 1 ]; then echo -n "$1"; fi
    head -n 1 > /dev/null
}

print_available_memory() {
    free -m | grep Mem | awk '{print $7}'
}

print_total_memory() {
    free -m | grep Mem | awk '{print $2}'
}

# Usage print_percentage <original-value> <percentage>
print_percentage() {
    echo $1 $2 | awk '{ printf "%d", $1 * $2 }'
}

# Remove a symlink if it exists and update its path
# usage update_symlink <data source> <symlink dest path> <opt:sudo>
# WARNING: BE CAREFUL!!! This function CAN DESTROY DATA!!!
update_symlink() {
    # Don't do anything if the data source does not exist
    if [ ! -e "$1" ]; then return $FAILURE; fi

    # Prepend "sudo" if requested
    if [ $# -eq 3 ] && [ "$3" = "sudo" ]; then
        prep="sudo "
    else
        prep=""
    fi

    # Remove the destination if not already a link
    if [ ! -L "$2" ]; then
        $prep rm -rf "$2"
    fi

    # Create/overwrite the symlink
    $prep ln -fns "$1" "$2"

    unset prep
    return $SUCCESS
}

# Checks to see if a variable is null
# Usage: var_is_defined VAR_NAME
var_is_defined() {
    if [ -z "${!1}" ]; then
        return $FAILURE
    fi
    return $SUCCESS
}