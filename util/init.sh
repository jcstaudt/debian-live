print_architecture() {
    dpkg --print-architecture
}
# Example: "bookworm"
print_distro_codename() {
    lsb_release -cs 2> /dev/null
}

# Example: "Debian"
print_distro_id() {
    lsb_release -is 2> /dev/null
}

# Example: "12"
print_distro_release() {
    lsb_release -rs 2> /dev/null
}

# Detect if running a graphical environment or a server (headless) one
print_hw_type() {
    if [ -z "$DISPLAY" ]; then
        echo server
    elif [ -d /proc/acpi/button/lid ]; then
        echo laptop
    else
        echo desktop
    fi
}

# Global configuration, regardless of environment
initialize() {
    ARCH=$(print_architecture)
    PLATFORM=$(print_hw_type)

    # Input arguments to this script
    ARGC=$#
    ARGV=("$@")

    # Return codes
    SUCCESS=0
    FAILURE=1
    SKIP=2
}

