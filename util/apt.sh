# Depends: cfg/cfg.sh
# Depends: cfg/net.sh   - IP_NAS_ETH, test_connection, ACTIVE_SSID
# Depends: util/init.sh - DISTRO
# Depends: util/vm.sh

case $(print_distro_id) in
    "Debian")
        UPSTREAM_MIRROR_PATH="deb.debian.org/debian"
        ;;
    "PureOS")
        UPSTREAM_MIRROR_PATH="repo.puri.sm/pureos"
        ;;
esac

# Disable automatic software updates
# This may be useful, since gnome-software uses the packagekit service to update
# package lists in the background, which locks apt and prevents these scripts
# from performing package-based tasks.
# Usage: disable_automatic_update
# TODO: Doesn't currently work
disable_automatic_update() {
    echo_blank -n "$_CONFIG Temporarily disabling automatic updates..."
    sudo systemctl mask    --now apt-daily.timer         > /dev/null 2>&1
    sudo systemctl disable --now apt-daily-upgrade.timer > /dev/null 2>&1
    sudo systemctl disable --now packagekit              > /dev/null 2>&1
    sudo systemctl disable --now unattended-upgrades     > /dev/null 2>&1
    echo_success -e "$_CONFIG Temporarily disabled automatic updates"
}

# Usage: enable_automatic_update
# TODO: Doesn't currently work
enable_automatic_update() {
    echo_blank -n "$_CONFIG Resuming automatic updates..."
    sudo systemctl unmask --now apt-daily.timer         > /dev/null 2>&1
    sudo systemctl enable --now apt-daily-upgrade.timer > /dev/null 2>&1
    sudo systemctl enable --now packagekit              > /dev/null 2>&1
    sudo systemctl enable --now unattended-upgrades     > /dev/null 2>&1
    echo_success -e "$_CONFIG Resumed automatic updates"
}

# Usage: download_if_not_exist <url> <dest_dir>
download_if_not_exist() {
    if [ $# -ne 2 ]; then
        return $FAILURE
    fi
    local filename="$2/$(basename $1)"
    if [ ! -f $filename ]; then
        echo_blank -n "$_DOWNLOAD Downloading \"$1\" to \"$filename\""
        curl --location --silent --url "$1" --output "$filename"
        if [ ! -f "$filename" ]; then
            echo_failure -e "$_DOWNLOAD Failed to download \"$1\" to \"$filename\""
            return $FAILURE
        fi
        echo_success -e "$_DOWNLOAD Downloaded \"$1\" to \"$filename\""
        return $SUCCESS
    fi
}

# Usage: download_if_missing_from_mirror <http|https> <url>
download_if_missing_from_mirror() {
    if [ $# -ne 2 ]; then
        return $FAILURE
    fi

    # Strip last period and everything following it
    #local dest_filename="${2%.*}"

    local dest_filename="$2"

    # Download potentially missing files from mirror,
    # as a workaround to apt-mirror not pulling all necessary filetypes
    if [ ! -f $REPO_MNT_PATH/mirror/$dest_filename ]; then
        if ! $REPO_PROPERMOUNT; then echo "WARN: Not downloading \"$2\"; repo not properly mounted..." && return $FAILURE; fi
        #if ! $INTERNET_CONNECTED; then echo "WARN: Not downloading \"$1\"; offline..." && return $FAILURE; fi
        if ! pkg_installed curl; then echo "WARN: Not downloading \"$2\"; curl not installed..." && return $FAILURE; fi
        echo_blank -n "$_DOWNLOAD Downloading \"$2\""
        sudo curl --location --url "$1://$2" --output "$REPO_MNT_PATH/mirror/$dest_filename" > /dev/null 2>&1
        if [ ! -f $REPO_MNT_PATH/mirror/$dest_filename ]; then
            echo_failure -e "$_DOWNLOAD Failed to download \"$2\""
            return $FAILURE
        fi
        sudo chown root:root $REPO_MNT_PATH/mirror/$dest_filename
        echo_success -e "$_DOWNLOAD Downloaded \"$2\""
        return $SUCCESS
    fi
}

pkg_installed() {
    # MUST compare to 0... NOT arbitrary YES/NO value
    # If testing, replace $(basename $1 | cut -d'_' -f1) with $1
    if [ $# -eq 0 ]; then
        return $FAILURE
    fi
    if dpkg-query -W -f='${Status}' $(basename $1 | cut -d'_' -f1) 2> /dev/null | grep -q "ok installed"; then
        return $SUCCESS
    fi
    return $FAILURE
}

# Check if "apt-get update" has ever been run
apt_repo_updated() {
    if [ $(HISTTIMEFORMAT="%d/%m/%y %T " history | grep "[a]pt-get update | wc -l") -gt 0 ]; then
        return $SUCCESS
    fi
    return $FAILURE
}

# Download a package and its dependencies; move them to a given destination
# Usage: apt_download <pkg-name> <target-destination>
apt_download() {
    if [ $# -ne 2 ]; then
        return $FAILURE
    fi
    sudo apt-get clean
    sudo apt-get update
    sudo apt-get --download-only --yes reinstall $1
    sudo mkdir -p $2/$1
    sudo mv /var/cache/apt/archives/*.deb $2/$1/
}

# Install package via apt-get if not installed
# Usage: install_apt_pkg <pkg-name>
install_apt_pkg() {
    if ! pkg_installed $1; then
        echo_blank -n "$_INSTALL Installing \"$1\"..."
        sudo apt-get -qq --yes install $1 > /dev/null 2>&1
        if ! pkg_installed $1; then
            echo_failure -e "$_INSTALL \"$1\" failed to install"
            return $FAILURE
        elif [ -f "$1" ]; then
            # Is a local file and not a package in a repository
            echo_success -e "$_INSTALL Installed local package \"$1\""
        else
            echo_success -e "$_INSTALL Installed  \"$1\""
        fi
    else
        echo_skip -e "$_INSTALL Skipping \"$1\"... already installed"
    fi
    return $SUCCESS
}

# Uninstall package via apt-get if installed
# Usage: uninstall_apt_pkg <pkg-name>
uninstall_apt_pkg() {
    if pkg_installed $1; then
        echo_blank -n "$_UNINSTALL Uninstalling \"$1\"..."
        sudo apt-get --auto-remove --purge -qq --yes remove $1 > /dev/null 2>&1
        if pkg_installed $1; then
            echo_failure -e "$_UNINSTALL \"$1\" failed to uninstall"
            return $FAILURE
        else
            echo_success -e "$_UNINSTALL Uninstalled \"$1\""
        fi
    else
        echo_skip -e "$_UNINSTALL Skipping \"$1\"... not installed"
    fi
    return $SUCCESS
}

task_update() {
    echo_blank -n "$_UPDATE Updating software repositories..."
    sudo apt-get autoclean 1> /dev/null
    sudo apt-get --yes update 1> /dev/null
    echo_success -e "$_UPDATE Updated software repositories"
}

# Description: Install package via apt-get if not installed, and configure the
#              environment if installing a specific package
# Note:        This function may be called recursively (e.g. as a preinst task
#              when running a task) BUT the child but be run in a sub-shell to
#              avoid variable conflicts
# Usage:       task_install <pkglist_array>
task_install() {
    pkglist=("$@")
    rtn_code=$SUCCESS
    for pkg in "${pkglist[@]}"; do
        changed=false
        case $pkg in
            "draw.io")
                download_if_not_exist "https://github.com/jgraph/drawio-desktop/releases/download/v20.8.16/drawio-amd64-20.8.16.deb" /tmp
                if $REPO_PROPERMOUNT && ! install_apt_pkg $REPO_MNT_PATH/drawio-amd64-20.8.16.deb; then
                    rtn_code=$FAILURE
                elif ! install_apt_pkg /tmp/drawio-amd64-20.8.16.deb; then
                    rtn_code=$FAILURE
                fi
                ;;
            "eclipse")
                download_if_not_exist "https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/2022-12/R/eclipse-cpp-2022-12-R-linux-gtk-x86_64.tar.gz&r=1" /tmp
                echo_blank -n "$_INSTALL Installing \"$pkg\"..."
                sudo tar zxf "/tmp/eclipse-cpp-2022-12-R-linux-gtk-x86_64.tar.gz&r=1" -C /opt
                sudo chown -R root:root /opt/eclipse
                sudo chmod -R +r /opt/eclipse
                sudo touch /usr/bin/eclipse
                sudo chmod 755 /usr/bin/eclipse
                cat <<'EOF' | sudo tee /usr/bin/eclipse 1> /dev/null
#!/bin/sh
export ECLIPSE_HOME="/opt/eclipse"
$ECLIPSE_HOME/eclipse $*
EOF
                cat <<EOF | sudo tee /usr/share/applications/eclipse.desktop 1> /dev/null
[Desktop Entry]
Encoding=UTF-8
Name=Eclipse
Comment=Eclipse IDE
Exec=eclipse
Icon=/opt/eclipse/icon.xpm
Terminal=false
Type=Application
Categories=GNOME;Application;Development;
StartupNotify=true
EOF
                echo_success -e "$_INSTALL Installed  \"$pkg\""
                ;;
            "steam")
                add_arch i386
                if $SW_UPDATE; then
                    task_update
                fi

                # can't use install_apt_pkg due to TUI prompts
                sudo apt-get -qq --yes install $pkg

                if pkg_installed $pkg; then
                    echo_blank -n "$_CONFIG Configuring $pkg "
                    if [ -d $GAMES_MNT_PATH ]; then
                        update_symlink "$GAMES_MNT_PATH/.steam" $HOME/.steam
                        update_symlink "$GAMES_MNT_PATH/.steam/sdk32/steam" $HOME/.steampath
                        update_symlink "$GAMES_MNT_PATH/.steam/steam.pid" $HOME/.steampid
                        update_symlink "$GAMES_MNT_PATH/.wine" $HOME/.wine
                    else
                        echo_failure -e "\"$GAMES_MNT_PATH\" not found"
                    fi
                    echo_success -e "$_CONFIG Configured $pkg"
                    if $NAS_EXISTS; then
                        echo_blank -n "$_CONFIG Configuring iSCSI "
                        mount_scsi $IP_NAS_ETH \
                                   3260 \
                                   "iqn.1994-11.com.netgear:readynas:ff5a9ec6:games" \
                                   $STEAM_LUN_PARTITION_LABEL \
                                   $STEAM_LUN_MNT_PATH
                        echo_success -e "$_CONFIG Configured iSCSI"
                    fi
                else
                    echo_failure -e "$_CONFIG Failed to configure $pkg... package not installed"
                    rtn_code=$FAILURE
                fi
                ;;
            *)
                # if a filepath is included rather than a package name, we
                # the package name must first be parsed from it
                if check_substring_exists $pkg /; then
                    temp=${pkg##*/} # everything after last '/'
                    pkg=${temp%%-*} # everything before first '-'
                    unset temp
                fi

                if [ -f "$REPOROOT/cfg/preinst/$pkg.sh" ]; then
                    . "$REPOROOT/cfg/preinst/$pkg.sh"
                    # if $? -eq $SKIP, changed=false
                    if [ $? -eq $SUCCESS ]; then
                        changed=true
                    else
                        rtn_code=$FAILURE
                    fi
                fi
                if ! install_apt_pkg $pkg; then
                    rtn_code=$FAILURE
                elif [ -f "$REPOROOT/cfg/postinst/$pkg.sh" ]; then
                    . "$REPOROOT/cfg/postinst/$pkg.sh"
                    if [ $? -eq $SUCCESS ]; then
                        changed=true
                    fi
                fi
                ;;
        esac
        if $changed; then
            echo_success -e "$_CONFIG Configured \"${pkg//\*}\""
        fi
    done
    unset changed
    unset pkglist
    unset pkg
    if [ $rtn_code -eq $SUCCESS ]; then
        unset rtncode
        return $SUCCESS
    else
        unset rtncode
        return $FAILURE
    fi
}

# Uninstall software
task_uninstall() {
    pkglist=("$@")
    rtn_code=$SUCCESS
    for pkg in "${pkglist[@]}"; do
        if ! uninstall_apt_pkg $pkg; then
            rtn_code=$FAILURE
        elif [ -f "$REPOROOT/cfg/postrm/${pkg//\*}.sh" ]; then
            . "$REPOROOT/cfg/postrm/${pkg//\*}.sh"
            if [ $? -eq $SUCCESS ]; then
                echo_success -e "$_CONFIG Cleaned \"${pkg//\*}\""
            fi
        fi
    done
    unset pkglist
    unset pkg
    if [ $rtn_code -eq $SUCCESS ]; then
        unset rtncode
        return $SUCCESS
    else
        unset rtncode
        return $FAILURE
    fi
}

task_upgrade() {
    echo_blank -n "$_UPGRADE Upgrading software..."
    sudo apt-get --yes upgrade > /dev/null 2>&1
    echo_success -e "$_UPGRADE Upgraded software" # TODO: Handle failures
}

task_dist_upgrade() {
    echo_blank -n "$_UPGRADE Upgrading software..."
    sudo apt-get --yes dist-upgrade > /dev/null 2>&1
    echo_success -e "$_UPGRADE Upgraded software" # TODO: Handle failures
}

# Usage: get_signing_key <key-url> <keyring-dest> <deb-url> <components> <list-path>
get_signing_key() {
    if [ $# -ne 5 ]; then
        echo_script_cfg_error ${FUNCNAME[0]} "insufficient number of parameters"
        return $FAILURE
    fi
    local key_url="$1"
    local keyring_dest="/usr/share/keyrings/$2"
    local deb_url="$3"
    local components="$4"
    local list_path="/etc/apt/sources.list.d/$5"
    local sign_path="signed-by=$keyring_dest"

    # TODO: create signing key in repo, if missing AND able to download
    #wget -O - https://$key_url | sudo tee $REPO_MNT_PATH/signing-keys/...

    if [ -f "$REPO_MNT_PATH/signing-keys/$key_url" ]; then
        sudo cp "$REPO_MNT_PATH/signing-keys/$key_url" "$keyring_dest"
    elif $INTERNET_CONNECTED && pkg_installed curl; then
        curl -sS https://$key_url | gpg --dearmor | sudo tee "$keyring_dest" 1> /dev/null
    else
        echo_failure "$_CONFIG Unable to locate \"$key_url\" signing key and curl is not installed"
    fi
    if [ -n "$deb_url" -a -n "$list_path" ]; then
        echo "deb [arch=$ARCH $sign_path] $deb_url $components" | sudo tee "$list_path" 1> /dev/null
    fi


    if $REPO_WANTMOUNT && $REPO_PROPERMOUNT; then
        # comment all lines
        sudo sed -i 's/^#*/#/' "$list_path"

        # remove http/https: from the url (now looks like url with preceding //)
        local deb_url_cropped=$(echo $deb_url | cut -d":" -f2)

        # prepend line to sources.list so local repo takes precedence against
        # external packages matching same name+version
        sudo sed -i "1i deb [arch=$ARCH $sign_path trusted=yes] file:$REPO_MNT_PATH/mirror/${deb_url_cropped:2} $components" $list_path
    fi
}

# Requires: curl
task_apt_sources() {

    # `curl -sS` is functionally equivalent to `wget -O-`

    echo_blank -n "$_CONFIG Including \"$COMPONENTS\" in \"/etc/apt/sources.list\"..."

    case $(print_distro_id) in
        "Debian")
            # Official Debian sources
            cat <<EOF | sudo tee /etc/apt/sources.list 1> /dev/null
# Source: https://wiki.debian.org/SourcesList#Example_sources.list
deb http://deb.debian.org/debian $SUITE $COMPONENTS
deb http://deb.debian.org/debian-security/ $SUITE-security $COMPONENTS
deb http://deb.debian.org/debian $SUITE-updates $COMPONENTS
EOF
            ;;
        "PureOS")
            cat <<EOF | sudo tee /etc/apt/sources.list 1> /dev/null
# Main package repository for PureOS
deb https://repo.pureos.net/pureos byzantium main

# Important security updates
deb https://repo.pureos.net/pureos byzantium-security main

# Other updates for resolving non-security issues
deb https://repo.pureos.net/pureos byzantium-updates main
EOF
            ;;
        "Ubuntu")
            cat <<EOF | sudo tee /etc/apt/sources.list 1> /dev/null
deb cdrom:[Ubuntu 22.04.2 LTS _Jammy Jellyfish_ - Release amd64 (20230223)]/ jammy main restricted
deb http://archive.ubuntu.com/ubuntu/ jammy main restricted
deb http://security.ubuntu.com/ubuntu/ jammy-security main restricted
deb http://archive.ubuntu.com/ubuntu/ jammy-updates main restricted
EOF
            ;;
    esac

    # Zoom
    # TODO: Doesn't work yet
    #curl -sS https://zoom.us/linux/download/pubkey | gpg --dearmor | sudo tee /usr/share/keyrings/zoom.gpg 1> /dev/null
    #sudo curl --url https://zoom.us/client/latest/zoom_$ARCH.deb --output $REPO_MNT_PATH/zoom_$ARCH.deb

    # TODO: The repo can be mounted but not at /var/spool/apt-mirror
    if $REPO_WANTMOUNT && $REPO_PROPERMOUNT; then
        sudo sed -i 's/^#*/#/' /etc/apt/sources.list
        sudo sed -i "1i deb file:$REPO_MNT_PATH/mirror/$UPSTREAM_MIRROR_PATH $SUITE $COMPONENTS" /etc/apt/sources.list
    fi
    echo_success -e "$_CONFIG \"$COMPONENTS\" repos written to \"/etc/apt/sources.list\""
}
