# Depends: cfg/cfg.sh   - IP_NAS_ETH
# Depends: util/echo.sh - echo_script_cfg_error

################################################################################
# Device functions
# TODO: Make these work for mounted NAS shares - not only block devices
# TODO: Make these work with virtual filesystems (.img,.qcow2,etc)
# TODO: Make these work for LUKS encrypted drives

# Description: Check if a device partition name or NAS share name exists
# Usage:       resource_label_exists "<label>" <opt:nas_ip>
# Output:      SUCCESS if a given device label exists or FAILURE if it does not
# Depends:     nfs-common
resource_label_exists() {
    if [ -z "$1" ]; then
        # Nothing exists
        return $FAILURE
    elif lsblk -l -n -o LABEL -p | grep -q "$1"; then
        # Block device label
        return $SUCCESS
    elif [ $# -eq 2 ] && pkg_installed nfs-common && nas_share_exists $2 "$1"; then
        # NAS share name
        return $SUCCESS
    fi
    return $FAILURE
}

# Check if a local device path or NAS share (format=hostname:/share/path) exists
# Usage: resource_path_exists <device_path> <opt:nas_ip>
# Output: SUCCESS if a given device path exists or FAILURE if it does not
resource_path_exists() {
    if [ -z "$1" ]; then
        # Nothing exists
        return $FAILURE
    elif lsblk -l -n -o NAME -p | grep -q "$1"; then
        # Block device path
        return $SUCCESS
    elif [ $# -eq 2 ] && pkg_installed nfs-common && nas_share_exists $2 "${1##*/}"; then
        # NAS share name
        return $SUCCESS
    fi
    return $FAILURE
}

# Usage: device_mountpoint_exists <mount_point>
# Output: SUCCESS if a given mount point exists or FAILURE if it does not
device_mountpoint_exists() {
    if [ -z "$1" ]; then
        return $FAILURE
    elif lsblk -l -n -o MOUNTPOINT -p | grep -q "$1"; then
        return $SUCCESS
    fi
    return $FAILURE
}

# Description: Print a list of NAS shares to stdout
# Usage:       get_nas_share_list <nas_ip>
# Output:      A list of available NAS shares listed as absolute paths ("$NAS_SHARE_ROOT/Share")
# Depends:     nfs-common
get_nas_share_list() {
    sudo showmount --no-headers -e $1 | grep -v /run | cut -d' ' -f1
}

# Description: Print a list of NAS shares to stdout that match a query
# Usage:       get_nas_share_path <nas_ip> <share_name>
# Output:      Absolute paths of NAS shares matching a query ("$NAS_SHARE_ROOT/share_name")
get_nas_share_path() {
    get_nas_share_list $1 | grep "/$2"
}

# Description: Print a list of NAS shares, including server, to stdout that match a query
# Usage:       get_full_nas_share_path <nas_ip> <share_name>
# Output:      Absolute paths of NAS shares matching a query ("nas:$NAS_SHARE_ROOT/share_name")
get_full_nas_share_path() {
    echo $1:$(get_nas_share_list $1 | grep "/$2")
}

# Usage: nas_share_exists <nas_ip> <share_name>
nas_share_exists() {
    #nas_share_list=$(get_nas_share_list $2)
    #if list_contains "${nas_share_list[@]}" $1; then
    #    unset nas_share_list
    #    return $SUCCESS
    #fi
    if [ -n "$(get_nas_share_path $1 $2)" ]; then
        return $SUCCESS
    fi
    return $FAILURE
}

# Input:   get_device_path_from_label <label> <opt:nas_ip>
# Output:  device_path (e.g. "/dev/sda1") or "" if one does not exist
# Depends: cfg/cfg.sh  - IP_NAS_ETH
# Depends: util/net.sh - NAS_EXISTS
# Depends: .           - nas_share_exists
get_device_path_from_label() {
    path=$(lsblk -l -n -o NAME,LABEL -p | grep "$1" | cut -d' ' -f 1)
    if [ -n "$path" ] && [ $(echo $path | wc -w) -gt 1 ]; then
        # TODO: Only include devices with lables exactly matching query - not just containing it
        >&2 echo "$_CONFIG More than one device containing device label \"$1\" in lsblk"
        >&2 echo "$_CONFIG (Received \"$path\")"
        exit $FAILURE
    elif [ $# -eq 2 ]; then
        path=$(get_full_nas_share_path $2 $1)
    fi
    echo $path
    unset path
}

# Input:  get_resource_label_from_path <device_path>
# Output: device_label (e.g. "home") or "" if one does not exist
get_resource_label_from_path() {
    #lsblk -n -o LABEL -p $1 2> /dev/null
    df 2> /dev/null | grep "$1" | cut -d' ' -f1
}

# Input:  get_mount_path_from_label <label>
# Output: mount_path (e.g. "/media/user/home") or "" if one does not exist
get_mount_path_from_label() {
    output=$(df 2> /dev/null | grep "$1")
    echo ${output##* } # everything after last ' '
    unset output
}

# Input:  get_mount_path_from_device_path <device_path>
# Output: mount_path (e.g. "/media/user/home") or "" if one does not exist
get_mount_path_from_device_path() {
    # TODO: What happens if mounted twice? (e.g. /media/user/repo AND /var/spool/apt-mirror)?
    lsblk $1 -n -o MOUNTPOINT 2> /dev/null | grep /
}

# Usage: dev_mounted <device_path>
dev_mounted() {
    # The space following $1 is intentional to avoid false positives
    # TODO: Doesn't seem to work for /dev/nbd*
    if grep -qs "$1 " /proc/mounts; then
        return $SUCCESS
    fi
    return $FAILURE
}

# Verify that the device_path and mount path are in the same entry
# Usage: verify_mount_path <device_path> <desired_mount_path>
verify_mount_path() {
    if grep -qs -e "$1 " -e "$2 " /proc/mounts; then
        return $SUCCESS
    fi
    return $FAILURE
}

# Usage:   mount_if_not_mounted <device_path> <mount_path>
# Depends: cfg/device.sh - dev_check_paths
# TODO:    Does not work with nfs mounts (parsing the server:path format)
mount_if_not_mounted() {
    # If a block device path is null, it doesn't exist (i.e. not connected)
    # Null string variables don't count as input arguments and must be checked
    if [ $# -ne 2 ]; then
        echo_failure -e "$_CONFIG Unable to mount (mount_if_not_mounted: args=\"$@\")... is this device connected?"
        return $FAILURE
    fi
    echo_blank -n "$_CONFIG Trying to mount $1 at \"$2\"..."
    # Check if the device path or NAS share name (the device path sans 'hostname:') exists
    if ! resource_path_exists ${1##*:} $IP_NAS_ETH; then
        echo_failure -e "$_CONFIG Failed to mount $1 at \"$2\"... ${1##*:} not in lsblk or NAS share list"
        return $FAILURE
    elif verify_mount_path $1 "$2"; then
        echo_delete_line # Formatting otherwise wrong if not printing skips
        echo_skip -e "$_CONFIG $1 already mounted at \"$2\"... skipping"
        return $SUCCESS
    fi

    echo_delete_line # clear previous echo_blank
    if dev_mounted $1; then
        current_mount_path=$(get_mount_path_from_device_path $1)
        echo_blank -n "$_CONFIG Re-mounting $1 from \"$current_mount_path\" to \"$2\"..."
        sudo umount $current_mount_path
    else
        current_mount_path=""
        echo_blank -n "$_CONFIG Mounting $1 at \"$2\"..."
    fi

    if [ ! -d "$2" ]; then
        sudo mkdir -p "$2"
        sudo chown $USER:$USER "$2"
    fi
    sudo mount $1 "$2"

    if verify_mount_path $1 "$2"; then
        if [ -n "$current_mount_path" ]; then
            echo_success -e "$_CONFIG Re-mounted $1 from \"$current_mount_path\" to \"$2\""
        else
            echo_success -e "$_CONFIG Mounted device $1 at \"$2\""
        fi
        unset current_mount_path
        return $SUCCESS
    fi
    unset current_mount_path
    echo_failure -e "$_CONFIG Failed to mount $1 at \"$2\""
    return $FAILURE
}

# Usage:   mount_scsi <portal> <port> <target> <partition label> <mount path>
# Depends: open-iscsi installed
# Depends: ntfs-3g    installed
#          iSCSI drive MUST be MBR/NTFS
mount_scsi() {
    if ! pkg_installed open-iscsi; then
        echo_failure -e "$_CONFIG Cannot configure iscsi... open-iscsi not installed"
        return $FAILURE
    elif ! pkg_installed ntfs-3g; then
        echo_failure -e "$_CONFIG Cannot configure iscsi... ntfs-3g not installed"
        return $FAILURE
    fi
    sudo systemctl start iscsid
    sudo mkdir -p $5
    sudo iscsiadm --mode discovery -t sendtargets --portal $1:$2 1> /dev/null
    sudo iscsiadm --mode node --targetname $3 --portal $1:$2 --login 1> /dev/null
    sudo mount -t ntfs $(get_device_path_from_label $4 2> /dev/null) $5
}

# Usage:   mount_share <nas share(s)> <ip_nas> <mount_root_dir>
# TODO:    Remove dependency on IP address (figure out how to have the IP
#          address as the 1st input argument and the NAS share list be all the
#          subsequent arguments.
mount_share() {
    if [ $# -lt 3 ]; then
        echo_script_cfg_error ${FUNCNAME[0]}
        return $FAILURE
    fi
    args=("$@")
    rtn_code=$SUCCESS

    # TODO: Maybe there is a better way to ignore the last element in a list
    mount_root_dir="${args[@]:(-1)}"
    unset 'args[${#args[@]}-1]'

    # TODO: Maybe there is a better way to ignore the last element in a list
    ipnas="${args[@]:(-1)}"
    unset 'args[${#args[@]}-1]'

    for i in "${args[@]}"; do
        lower=${i,,}                      # $NAS_SHARE_ROOT/Movies -> $NAS_SHARE_ROOT/movies
        destpath="$mount_root_dir/${lower#/*/}" # $mount_root_dir/<$NAS_SHARE_ROOT/movies->movies>
        sudo mkdir -p $destpath
        if ! df 2> /dev/null | grep -q $destpath; then
            # TODO: Use functions above rather than $ipnas:$i
            sudo mount -t nfs $ipnas:$i $destpath
            if dev_mounted $ipnas:$i; then
                echo_success -e "$_CONFIG Mounted share \"$i\" at \"$destpath\""
            else
                echo_failure -e "$_CONFIG Failed to mount \"$i\" to desired path \"$destpath\""
                rtn_code=$FAILURE
            fi
        else
            echo_skip -e "$_CONFIG Not mounting \"$i\" to \"$destpath\"... already mounted"
        fi
    done
    if [ $rtn_code -eq $SUCCESS ]; then
        unset args destpath ipnas lower mount_root_dir rtn_code
        return $SUCCESS
    else
        unset args destpath ipnas lower mount_root_dir rtn_code
        return $FAILURE
    fi
}

# Description: Mount every share available on the NAS
# Usage:       mount_nas <ip_nas> <mount_root_dir>
# Depends:     util/net.sh - NAS_EXISTS
mount_nas() {
    if ! $NAS_EXISTS; then
        return $FAILURE
    fi
    rtn_code=$SUCCESS # Keep track of several return codes (1 for ea share)
    nas_share_list=$(get_nas_share_list $1)

    while read line; do
        if ! (mount_share $line $1 $2); then rtn_code=$FAILURE; fi
    done < <(echo "$nas_share_list")

    if [ $rtn_code -eq $SUCCESS ]; then
        unset rtn_code nas_share_list
        return $SUCCESS
    else
        unset rtn_code nas_share_list
        return $FAILURE
    fi
}

# Description: Unlock a LUKS-encrypted drive
# Usage:       luks_unlock <device_path>
luks_unlock() {
    # TODO: Modify this to support >1 connected LUKS devices
    if [ -d /dev/dm-0 ]; then
        echo_skip -e "$_CONFIG Not unlocking LUKS device $1... already unlocked"
        return $SKIP
    elif ! pkg_installed udisks2; then
        echo_failure -e "$_CONFIG Cannot unlock $1... udisks2 not installed"
        return $FAILURE
    fi
    echo_blank -n "$_CONFIG Unlocking LUKS device $1..."
    sudo udisksctl unlock -b $1
    # TODO: The path is not always /dev/dm-0; could be /dev/dm-1, etc.
    if ! resource_path_exists /dev/dm-0; then
        echo_failure -e "$_CONFIG Failed to unlock $1"
        return $FAILURE
    fi
    echo_success -e "$_CONFIG Unlocked LUKS device $1"
    return $SUCCESS
}

# Description: Lock a LUKS-encrypted drive
# Usage:       luks_lock <device_path>
luks_lock() {
    # TODO: Check for a mounted filesystem before locking
    # TODO: Modify this to support >1 connected LUKS devices
    if [ ! -d /dev/dm-0 ]; then
        echo_skip -e "$_CONFIG Not locking LUKS device $1... already locked"
        return $SKIP
    elif ! pkg_installed udisks2; then
        echo_failure -e "$_CONFIG Cannot lock $1... udisks2 not installed"
        return $FAILURE
    fi
    echo_blank -n "$_CONFIG Locking LUKS device $1..."
    sudo udisksctl lock -b $1
    # TODO: The path is not always /dev/dm-0; could be /dev/dm-1, etc.
    if resource_path_exists /dev/dm-0; then
        echo_failure -e "$_CONFIG Failed to lock $1"
        return $FAILURE
    fi
    echo_success -e "$_CONFIG Locked LUKS device $1"
    return $SUCCESS
}

# Description: Power off a LUKS-encrypted drive
# Usage:       luks_poweroff <device_path>
luks_poweroff() {
    # TODO: Modify this to support >1 connected LUKS devices
    if ! pkg_installed udisks2; then
        echo_failure -e "$_CONFIG Cannot power off $1... udisks2 not installed"
        return $FAILURE
    fi
    echo_blank -n "$_CONFIG Powering off LUKS device $1..."
    sudo udisksctl power-off -b $1
    if ! resource_path_exists $1; then
        echo_success -e "$_CONFIG Locked LUKS device $1"
        return $SUCCESS
    fi
}

# Description: Check if a nbd is in use
# Usage:       nbd_connected <nbd-device-path>
nbd_connected() {
    # An nbd is not connected if the qemu-nbd process is not running
    # TODO: Need to not only check that it is connected, but verify that the proper image is connected
    if ps m -C qemu-nbd | grep -q qemu-nbd && \
       [ -f "/sys/devices/virtual/block/$(basename /dev/nbd1)/pid" ]; then
        return $SUCCESS
    fi
    return $FAILURE
}

# Description: Assign a virtual filesystem to a Network Block Device (nbd)
#              device path
# Usage:       nbd_connect <device path> <virtual filesystem filepath>
nbd_connect() {
    echo_blank -n "$_CONFIG Connecting \"$2\" to $1..."
    if ! pkg_installed qemu-utils; then
        echo_failure -e "$_CONFIG Cannot connect \"$2\" to $1... qemu-utils not installed"
        return $FAILURE
    elif [ ! -f "$2" ]; then
        echo_failure -e "$_CONFIG Failed to connect \"$2\" to $1... path \"$2\" not found"
        return $FAILURE
    elif nbd_connected $1; then
        echo_delete_line # Formatting otherwise wrong if not printing skips
        echo_skip -e "$_CONFIG Not connecting \"$2\" to $1... already connected"
        return $SUCCESS
    else
        sudo qemu-nbd --connect=$1 "$2" 2> /dev/null
        if nbd_connected $1; then
            echo_success -e "$_CONFIG Connected \"$2\" to $1"
            return $SUCCESS
        else
            echo_failure -e "$_CONFIG Failed to connect \"$2\" to $1... is \"$2\" already in use by another device?"
            return $FAILURE
        fi
    fi
}

# Description: Unassign a virtual filesystem from a Network Block Device (nbd)
#              device path
# Usage:       nbd_disconnect <device path>
nbd_disconnect() {
    echo_blank -n "$_CONFIG Disconnecting $1..."
    if ! resource_path_exists $1 $IP_NAS_ETH; then
        echo_delete_line # Formatting otherwise wrong if not printing skips
        echo_skip -e "$_CONFIG Not disconnecting $1... already not connected"
        return $SUCCESS
    elif ! pkg_installed qemu-utils; then
        # Not sure how this is possible, but... just in case?
        echo_failure -e "$_CONFIG Cannot disconnect $1... qemu-utils not installed"
        return $FAILURE
    else
        sudo qemu-nbd --disconnect $1
        if ! resource_path_exists $1 $IP_NAS_ETH; then
            echo_success -e "$_CONFIG Disconnected $1"
            return $SUCCESS
        else
            echo_failure -e "$_CONFIG Failed to disconnect $1"
            return $FAILURE
        fi
    fi
}

# Description: Check if nbd kernel module is enabled
# Usage:       nbd_is_disabled (Ex: "if nbd_is_enabled; then")
nbd_is_enabled() {
    # Alternatively can use the following, but runtime is longer
    # if lsmod | grep -q nbd; then
    if [ $(ls /dev/nbd* 2> /dev/null | wc -l) -gt 0 ]; then
        return $SUCCESS
    fi
    return $FAILURE
}

# Description: Enable support for Network Block Devices (nbd)
# Usage:       nbd_enable
nbd_enable() {
    echo_blank -n "$_CONFIG Enabling module nbd..."
    if nbd_is_enabled; then
        echo_delete_line # Formatting otherwise wrong if not printing skips
        echo_skip -e "$_CONFIG Not enabling module nbd... already enabled"
        return $SUCCESS
    else
        sudo modprobe nbd max_part=8
        if nbd_is_enabled; then
            echo_success -e "$_CONFIG Enabled module nbd"
            return $SUCCESS
        else
            echo_failure -e "$_CONFIG Failed to enable module nbd"
            return $FAILURE
        fi
    fi
}

# Description: Disable support for Network Block Devices (nbd)
# Usage:       nbd_disable
nbd_disable() {
    if ! nbd_is_enabled; then
        echo_skip -e "$_CONFIG Not disabling module nbd... already disabled"
        return $FAILURE
    fi
    nbd_disconnect /dev/nbd*
    echo_blank -n "$_CONFIG Disabling nbd module..."
    sudo rmmod nbd
    if ! nbd_is_enabled; then
        echo_success -e "$_CONFIG Disabled nbd module"
        return $SUCCESS
    else
        echo_failure -e "$_CONFIG Failed to disable module nbd"
        return $FAILURE
    fi
}

# Description: Disable laptop lid closure suspend event
# Usage:       lid_close_suspend_disable
lid_close_suspend_disable() {
    echo_blank -n "$_CONFIG Configuring systemd to ignore laptop lid closure event..."
    # Disable sleep/suspend/hibernate/hybrid services
    #sudo systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target > /dev/null
    sudo sed -i 's/#HandleLidSwitch=suspend/HandleLidSwitch=ignore/g' /etc/systemd/logind.conf
    sudo sed -i 's/#HandleLidSwitchExternalPower=suspend/HandleLidSwitchExternalPower=ignore/g' /etc/systemd/logind.conf
    sudo systemctl restart systemd-logind
    echo_success -e "$_CONFIG Configured systemd to ignore laptop lid closure event"
}
