# Depends: cfg/cfg.sh
# Depends: util/echo.sh
# Depends: util/net.sh - get_ethernet_interfaces

vm_init() {
    # libvirtd needs to be running in order to connect to the socket
    sudo systemctl enable --now libvirtd
    sudo virsh net-autostart --network default > /dev/null 2>&1
    sudo virsh net-start default > /dev/null 2>&1

    # Only doing this to avoid permissions issues when reading VM images
    # TODO: This is not good practice
    sudo chmod --quiet 777 /media/$USER
    sudo chmod --quiet 777 $VM_MNT_PATH
}

# Usage: vm_autostart <vm name>
vm_autostart() {
    echo_blank -n "$_CONFIG Starting $1"
    sudo virsh autostart $1 1> /dev/null
    sudo virsh start $1 1> /dev/null
    echo_success -e "$_CONFIG Started $1"
}

# Construct the virt-install command
# Usage: Not intended to be called directly, but through vm_link
vm_create_cmd() {
    # Set defaults if left unconfigured
    if [ -z "$VM_ARCH" ]; then        VM_ARCH="x86_64"; fi
    if [ -z "$VM_BOOT" ]; then        VM_BOOT="hd"; fi
    if [ -z "$VM_DESCRIPTION" ]; then VM_DESCRIPTION="Auto-configured"; fi
    if [ -z "$VM_NAME" ]; then        VM_NAME="default"; fi
    if [ -z "$VM_OSVARIANT" ]; then   VM_OSVARIANT="generic"; fi
    if [ -z "$VM_RAM" ]; then         VM_RAM="4096"; fi
    if [ -z "$VM_TITLE" ]; then       VM_TITLE="Default"; fi
    if [ -z "$VM_VCPUS" ]; then       VM_CPUS="$(nproc)"; fi

    # Construct the virt-install command string
    VIRT_INSTALL_CMD="                  sudo virt-install"
    VIRT_INSTALL_CMD="$VIRT_INSTALL_CMD --arch $VM_ARCH"
    VIRT_INSTALL_CMD="$VIRT_INSTALL_CMD --boot $VM_BOOT"
    VIRT_INSTALL_CMD="$VIRT_INSTALL_CMD --connect qemu:///system"
    VIRT_INSTALL_CMD="$VIRT_INSTALL_CMD --metadata description=\"$VM_DESCRIPTION\""

    if [ -z "$VM_CDROM" ]; then
        # Import existing disk image

        if [ -z "$VM_DISK_INTERFACE" ]; then VM_DISK_INTERFACE="virtio"; fi
        if [ -z "$VM_FILETYPE" ]; then       VM_FILETYPE="qcow2"; fi

        # Establish absolute path to the VM disk image
        VM_DISK_PATH="$VM_MNT_PATH/$VM_NAME.$VM_FILETYPE"

        VIRT_INSTALL_CMD="$VIRT_INSTALL_CMD --disk $VM_DISK_PATH,bus=$VM_DISK_INTERFACE,device=disk,format=$VM_FILETYPE,sparse=no"
        VIRT_INSTALL_CMD="$VIRT_INSTALL_CMD --import"

    else
        # Load live boot environment (no persistent virtual OS disk)

        VIRT_INSTALL_CMD="$VIRT_INSTALL_CMD --cdrom $VM_CDROM"
        VIRT_INSTALL_CMD="$VIRT_INSTALL_CMD --disk none"
    fi

    VIRT_INSTALL_CMD="$VIRT_INSTALL_CMD $VM_ADDSTORAGE"
    VIRT_INSTALL_CMD="$VIRT_INSTALL_CMD --events on_crash=restart"
    VIRT_INSTALL_CMD="$VIRT_INSTALL_CMD --graphics spice"
    VIRT_INSTALL_CMD="$VIRT_INSTALL_CMD --hvm"
    VIRT_INSTALL_CMD="$VIRT_INSTALL_CMD --name $VM_NAME"

    # Configure network based on whether the VM is a:
    # - client (default network), or
    # - server (direct macvtap connection)
    if $VM_SERVER; then
        vm_net_register $VM_NAME $VM_NET_MAC $VM_NET_IP
        VIRT_INSTALL_CMD="$VIRT_INSTALL_CMD --network type=direct,source=$ETH_LAN_IF,source.mode=bridge,model=virtio,mac=$VM_NET_MAC"
    else
        VIRT_INSTALL_CMD="$VIRT_INSTALL_CMD --network network=default"
    fi

    VIRT_INSTALL_CMD="$VIRT_INSTALL_CMD --noautoconsole"
    VIRT_INSTALL_CMD="$VIRT_INSTALL_CMD --noreboot"
    VIRT_INSTALL_CMD="$VIRT_INSTALL_CMD --os-variant detect=on,name=$VM_OSVARIANT"
    VIRT_INSTALL_CMD="$VIRT_INSTALL_CMD --ram $VM_RAM"
    VIRT_INSTALL_CMD="$VIRT_INSTALL_CMD --sound default"
    VIRT_INSTALL_CMD="$VIRT_INSTALL_CMD --metadata title=$VM_TITLE"
    VIRT_INSTALL_CMD="$VIRT_INSTALL_CMD --vcpus $VM_VCPUS"
    VIRT_INSTALL_CMD="$VIRT_INSTALL_CMD --virt-type=kvm"
}

# Usage: Not intended to be called directly, but through vm_link
vm_read_cfg() {

    LIBVIRT_IMAGES=/var/lib/libvirt/images
    LIBVIRT_CONFIG=/etc/libvirt/qemu
    LIBVIRT_SNAPSHOT=/var/lib/libvirt/qemu/snapshot

    # Don't persist variables through VM instances
    VM_ADDSTORAGE=""     # Any extra disks to add to the VM
    VM_ARCH=""           # Ex: aarch64 | x86_64 | ...
    VM_BOOT=""           # Ex: hd | uefi | ...
    VM_CDROM=""          # path to an ISO image, or URL to access a boot image
    VM_DESCRIPTION=""    # Description of the VM
    VM_DISK_INTERFACE="" # Ex: ide | sata | virtio | ...
    VM_DISK_PATH=""      # Absolute path to the VM disk image
    VM_FILETYPE=""       # VM disk image filetype
    VM_NAME=""           # VM disk image filename (no filetype)
    VM_NET_IP=""         # Static virtual IP address for server images
    VM_NET_MAC=""        # Static MAC address for server images
    VM_OSVARIANT=""      # Ex: centos7.0 | debian10 | winxp | win10 | ...
    VM_RAM=""            # Ex: 1024 | 4096 | ...
    VM_SERVER=""         # Cfg network as server/headless if yes, else client
    VM_TITLE=""          # Title of the VM
    VM_VCPUS=""          # Ex: 1 | 4 | $(nproc)

    # Parse input arguments
    for i in "$@"; do
        case $i in
          --add-storage=*)
              # Append extra disks
              VM_ADDSTORAGE="$VM_ADDSTORAGE --disk ${i#*=}"
              shift # past argument=value
              ;;
          --arch=*)
              VM_ARCH="${i#*=}"
              shift # past argument=value
              ;;
          --boot=*)
              VM_BOOT="${i#*=}"
              shift # past argument=value
              ;;
          --cdrom=*)
              VM_CDROM="${i#*=}"
              shift # past argument=value
              ;;
          --description=*)
              VM_DESCRIPTION="${i#*=}"
              shift # past argument=value
              ;;
          --diskinterface=*)
              VM_DISK_INTERFACE="${i#*=}"
              shift # past argument=value
              ;;
          --diskname=*)
              VM_NAME="${i#*=}"
              shift # past argument=value
              ;;
          --disktype=*)
              VM_FILETYPE="${i#*=}"
              shift # past argument=value
              ;;
          --headless=*)
              # Respect global configuration
              if [ $VM_HEADLESS -a "${i#*=}" = "yes" ]; then VM_SERVER=true; else VM_SERVER=false; fi
              shift # past argument=value
              ;;
          --os-variant=*)
              VM_TITLE="${i#*=}"
              shift # past argument=value
              ;;
          --ram=*)
              VM_RAM="${i#*=}"
              shift # past argument=value
              ;;
          --static-ip=*)
              VM_NET_IP="${i#*=}"
              shift # past argument=value
              ;;
          --static-mac=*)
              VM_NET_MAC="${i#*=}"
              shift # past argument=value
              ;;
          --title=*)
              VM_TITLE="${i#*=}"
              shift # past argument=value
              ;;
          --vcpus=*)
              VM_VCPUS="${i#*=}"
              shift # past argument=value
              ;;
          --*)
              echo "vm_read_cfg: Undefined option $i"
              exit 1
              ;;
          *)
              # Ignore value
              ;;
        esac
    done
}

# Usage: vm_net_register <vm name> <vm mac> <vm ip addr>
vm_net_register() {
    # Print: sudo virsh net-dhcp-leases default
    sudo virsh net-update default add ip-dhcp-host "<host mac='$2' name='$1' ip='$3' />" --live --config > /dev/null 2>&1
}

vm_link() {
    vm_read_cfg "$@"
    vm_create_cmd

    if [ -n "$VM_CDROM" -a ! -e $VM_CDROM ]; then
        echo_failure -e "$_CONFIG Not linking \"$VM_NAME\"; iso \"$VM_CDROM\" not found..."
    elif [ ! -e $VM_DISK_PATH ]; then
        echo_failure -e "$_CONFIG Not linking \"$VM_NAME\"; disk \"$VM_DISK_PATH\" not found..."
    elif ! $(sudo virsh list --all | grep -q " $VM_NAME "); then
        echo_blank -n "$_CONFIG Linking VM \"$VM_NAME\"..."

        # Link the VM using the constructed command string
        ${VIRT_INSTALL_CMD} 1> /dev/null

        if [ ! -d $LIBVIRT_SNAPSHOT_DIR ]; then
            sudo mkdir -p $LIBVIRT_SNAPSHOT_DIR
        fi
        # TODO: Don't specify r220; the snapshot may not be used on that system
        if [ -d "$REPOROOT/r220/snapshot/$VM_NAME" ]; then
            sudo cp -rp "$REPOROOT/r220/snapshot/$VM_NAME" $LIBVIRT_SNAPSHOT_DIR
        fi
        if $(sudo virsh list --all | grep -q " $VM_NAME "); then
            echo_success -e "$_CONFIG Linked VM \"$VM_NAME\""
        else
            echo_failure -e "$_CONFIG Unable to link VM \"$VM_NAME\""
        fi
    else
        echo_skip -e "$_CONFIG \"$VM_NAME\" already linked... skipping"
    fi

    if $VM_AUTOSTART; then vm_autostart $VM_NAME; fi
}