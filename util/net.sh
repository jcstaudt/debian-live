# Depends: cfg/cfg.sh   - load_platform_cfg
# Depends: util/echo.sh - echo_script_cfg_error, echo_failure, echo_skip, echo_success
# Depends: util/fs.sh   - append_unique_string_to_file, check_substring_exists, var_is_defined

# TODO: A lot of this logic can be automated, e.g. detect if connected to ethernet
#       and prefer that over wifi, etc.

################################################################################
# Networking functions

# Description: Read entries from cfg/net.csv and add uniques to /etc/hosts
# Usage:       add_hosts_from_csv <opt: /path/to/csv>
# Depends:     util/fs.sh - append_unique_string_to_file, check_substring_exists
add_hosts_from_csv() {
    # set a default path
    if [ $# -ne 1 ]; then
        path=$REPOROOT/cfg/net.csv
    else
        path=$1
    fi

    # read the csv file but skip the header
    while IFS="," read -r hostnames platform ipv4 mac comment
    do
        # skip empty required fields
        if [ -z "$hostnames" -o -z "$ipv4" ]; then continue; fi

        # support multiple platforms with the same dataset
        # if PLATFORM exists within platform, or is empty, assign it
        # if the plaform does not match, skip it
        if [ -z "$platform" ] || check_substring_exists "$platform" "$PLATFORM"; then
            append_unique_string_to_file "$ipv4 $hostnames" /etc/hosts sudo
        fi

    done < <(tail -n +2 $path)
    unset path hostnames platform ipv4 mac comment
}

# Description: Get string of wired network interface(s)
# Usage:       get_ethernet_interfaces
get_ethernet_interfaces() {
    ip r | grep default | grep dev | sed -e 's/^.*dev \([^ ]*\) .*$/\1/'
}

# Description: Get string of the wired network interface for WAN use
# Usage:       get_ethernet_wan_interface
# Depends:     get_ethernet_interfaces
get_ethernet_wan_interface() {
    get_ethernet_interfaces | awk "NR==1"
}

# Description: Get string of the wired network interface for LAN use
# Usage:       get_ethernet_lan_interface
# Depends:     get_ethernet_interfaces
get_ethernet_lan_interface() {
    case $(get_ethernet_interfaces | wc -l) in
        1)
            get_ethernet_interfaces | awk "NR==1"
            ;;
        *)
            get_ethernet_interfaces | awk "NR==2"
            ;;
    esac
}

# Description: Get string of wireless network interface
# Usage:       get_wireless_interface_name
get_wireless_interface_name() {
    ip a | grep " wl" | grep : | cut -d':' -f2 | cut -d' ' -f2

    #wifi_path=$(find /sys/class/net -type l | grep -v en | grep -v lo | grep -v usb | grep -v virbr | grep -v vnet)
    #if [ -n "$wifi_path" ]; then
    #    WIFI_IF="$(basename $wifi_path)"
    #else
    #    WIFI_IF=""
    #fi
    #unset wifi_path
}

# Description: Get the active wireless network SSID
# Usage:       get_wireless_ssid
# Depends:     nmcli (network-manager package)
# Depends:     util/apt.sh - pkg_installed
# TODO: what if nmcli is not installed? Any other methods?
get_wireless_ssid() {
    if pkg_installed network-manager; then
        nmcli -t -f active,ssid dev wifi | grep '^yes' | cut -d':' -f2
    fi
}

# Description: Enable the wireless radio
# Usage:       enable_wireless_radio
# Depends:     nmcli (network-manager package)
# Depends:     util/apt.sh  - pkg_installed
# Depends:     util/echo.sh - echo_failure, echo_success
# TODO: what if nmcli is not installed? Any other methods?
enable_wireless_radio() {
    echo_blank -n "$_NETWORK Enabling the wireless radio..."
    if pkg_installed network-manager; then
        nmcli radio wifi on
        echo_success -e "$_NETWORK Enabled the wireless radio"
    else
        echo_failure -e "$_NETWORK Failed to enable the wireless radio... is network-manager installed?"
    fi
}

# Description: Disable the wireless radio
# Usage:       disable_wireless_radio
# Depends:     nmcli (network-manager package)
# Depends:     util/apt.sh  - pkg_installed
# Depends:     util/echo.sh - echo_failure, echo_success
# TODO: what if nmcli is not installed? Any other methods?
disable_wireless_radio() {
    echo_blank -n "$_NETWORK Enabling the wireless radio..."
    if pkg_installed network-manager; then
        nmcli radio wifi off
        echo_success -e "$_NETWORK Disabled the wireless radio"
    else
        echo_failure -e "$_NETWORK Failed to disable the wireless radio... is network-manager installed?"
    fi
}

# Usage: get_mac_from_ip <IP address>
get_mac_from_ip() {
    if [ $# -ne 1 ]; then
        echo_script_cfg_error ${FUNCNAME[0]}
    fi
    test_connection $1 && /usr/sbin/arp -a | grep $1 | cut -d' ' -f4
}

# Usage: test_connection <IP address>
# Usage: test_connection <IP address> <interface>
# Usage: test_connection <IP address> <send N packets> <wait N seconds>
# Usage: test_connection <IP address> <interface> <send N packets> <wait N seconds>
test_connection() {
    case $# in
        0)
            return $SKIP
            ;;
        1)
            ping -c 1 $1 > /dev/null 2>&1
            ;;
        2)
            ping -c 1 $1 -I $2 > /dev/null 2>&1
            ;;
        3)
            ping -c $3 -i $4 $1 > /dev/null 2>&1
            ;;
        4)
            ping -c $3 -I $2 -i $4 $1 > /dev/null 2>&1
            ;;
        *)
            echo_script_cfg_error ${FUNCNAME[0]}
            return $FAILURE
            ;;
    esac
    if [ $? -eq 0 ]; then return $SUCCESS; else return $FAILURE; fi
}

# Usage: wait_for_internet_connection <interface> <send N packets> <wait N seconds>
wait_for_internet_connection() {
    test_connection debian.org $1 $2 $3
    if [ $? -eq 0 ]; then return $SUCCESS; else return $FAILURE; fi
}

# TODO: Revise this
net_load_cfg() {
    # config_wlan: automatic WiFi configuration (only relevant when WiFi enabled)
    case $ARGC in
        0)
            config_wlan=false
            ;;
        1)
            if ! $ENABLE_WIFI; then
                config_wlan=false
                echo_skip "$_NETWORK Not configuring WLAN as WIFI is selected to be turned off"
            else
                echo_usage
                wlan_ssid=${ARGV[0]}
                read -p "$_NETWORK Passphrase for WLAN (SSID = ${ARGV[0]}): " wlan_pass
                if [ -z "$wlan_pass" ]; then
                    config_wlan=false
                fi
                config_wlan=true
            fi
            ;;
        2)
            if ! $ENABLE_WIFI; then
                config_wlan=false
                echo_skip "$_NETWORK Not configuring WLAN as WIFI is selected to be turned off"
            else
                wlan_ssid=${ARGV[0]}
                wlan_pass=${ARGV[1]}
                config_wlan=true
            fi
            ;;
        *)
            echo_usage
            exit $FAILURE
            ;;
    esac
    return $SUCCESS
}

generate_wpa() {
    echo_blank -n "$_NETWORK Generating \"$WPA_SUPPLICANT_CFG\"..."

    is_bssid=0

    if [ -f "$WPA_SUPPLICANT_CFG" ]; then
        sudo mv $WPA_SUPPLICANT_CFG $WPA_SUPPLICANT_CFG.bak
    fi
    # Generate config file
    wpa_passphrase "$wlan_ssid" "$wlan_pass" | sudo tee $WPA_SUPPLICANT_CFG 1> /dev/null

    # Remove commented line containing plaintext passphrase
    sudo sed -i /#/d $WPA_SUPPLICANT_CFG

    #sudo sed -i '1 i ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=wheel' $WPA_SUPPLICANT_CFG # 'wheel' group must exist to use this
    #sudo sed -i "$i        scan_ssid=|r /dev/stdin" $WPA_SUPPLICANT_CFG <<<$is_bssid
    sudo sed -i '$i\        proto=RSN' $WPA_SUPPLICANT_CFG
    sudo sed -i '$i\        key_mgmt=WPA-PSK' $WPA_SUPPLICANT_CFG
    sudo sed -i '$i\        group=CCMP' $WPA_SUPPLICANT_CFG
    sudo sed -i '$i\        pairwise=CCMP' $WPA_SUPPLICANT_CFG
    sudo sed -i '$i\        priority=10' $WPA_SUPPLICANT_CFG

    echo_success -e "$_NETWORK Generated \"$WPA_SUPPLICANT_CFG\""
}

task_bluetooth() {
    if ! var_is_defined USE_BLUETOOTH; then
        echo_skip "$_NETWORK Bluetooth preferences not set... skipping"
        return $FAILURE
    elif ! $USE_BLUETOOTH; then
        echo_blank -n "$_NETWORK Disabling bluetooth..."
        sudo systemctl stop bluetooth
        echo_success -e "$_NETWORK Disabled bluetooth"
        return $SUCCESS
    else
        echo_blank -n "$_NETWORK Enabling bluetooth..."
        sudo systemctl start bluetooth
        echo_success -e "$_NETWORK Enabled bluetooth"
        # I don't know how to autoconnect a bluetooth device... sounds complex
        return $SUCCESS
    fi
}

# TODO: Refactor this
wlan_connect() {
    # Check for undefined variables
    if ! var_is_defined ENABLE_WIFI; then
        echo_script_cfg_error ${FUNCNAME[0]}
        return $FAILURE
    fi
    if ! var_is_defined config_wlan; then
        echo_script_cfg_error ${FUNCNAME[0]}
        return $FAILURE
    fi

    # TODO: Can't do this; circular dependency as ENABLE_WIFI is set if WiFi works
    if ! $ENABLE_WIFI; then
        echo_skip "$_NETWORK WLAN not configured... WiFi not selected"
        return $SUCCESS
    fi
    if ! $config_wlan; then
        echo_skip "$_NETWORK WLAN not configured... config_wlan said to skip"
        return $SUCCESS
    fi
    generate_wpa
    echo_blank -n "$_NETWORK Restarting wpa_supplicant..."
    sudo systemctl restart wpa_supplicant
    echo_success -e "$_NETWORK Restarted wpa_supplicant"

    echo_blank -n "$_NETWORK Connecting wpa_supplicant..."
    sudo wpa_supplicant -B -i$WIFI_IF -c $WPA_SUPPLICANT_CFG -Dnl80211,wext 1> /dev/null
    echo_success -e "$_NETWORK Connected wpa_supplicant"

    echo_blank -n "$_NETWORK Enabling DHCP on interface $WIFI_IF..."
    sudo dhclient $WIFI_IF
    echo_success -e "$_NETWORK Enabled DHCP on interface $WIFI_IF"

    if ! $METERED_NETWORK; then
        # TODO: Wait for active internet connection before proceeding; continue
        #       upon the 1st successful ping
        echo_blank -n "$_NETWORK Establishing internet connection..."
        wait_for_internet_connection $(get_wireless_interface_name) 12 5
        echo_success -e "$_NETWORK Established internet connection"
    fi
}

# Determine what network connections are available and establish priority
# for sake of simplicity, assuming: ethernet > wireless > offline
# Depends: . - get_ethernet_interfaces, get_wireless_interface_name, test_connection
net_survey() {
    ETH_LAN_IF=$(get_ethernet_lan_interface)  # string name of ethernet LAN interface
    ETH_WAN_IF=$(get_ethernet_wan_interface)  # string name of ethernet WAN interface
    WIFI_IF=$(get_wireless_interface_name)    # string name of wireless interface
    INTERNET_CONNECTED=false                  # if an external site can be reached
    INTERNET_CONNECTION_METHOD=none           # best connection to reach an external site
    ENABLE_ETH=false                          # if ethernet is the best available connection
    ENABLE_WIFI=false                         # if wifi is the best available connection
    if [ -n "$ETH_WAN_IF" ] && test_connection debian.org $ETH_WAN_IF; then
        INTERNET_CONNECTED=true
        INTERNET_CONNECTION_METHOD=ethernet
        ENABLE_ETH=true
        METERED_NETWORK=false
    elif [ -n "$WIFI_IF" ] && test_connection debian.org $WIFI_IF; then
        ACTIVE_SSID="$(get_wireless_ssid)" # Depends: nmcli installed
        INTERNET_CONNECTED=true
        INTERNET_CONNECTION_METHOD=wireless
        ENABLE_WIFI=true
    fi

    # TODO: consider refactoring NAS_EXISTS into a utility function?
    # Note that ETH_LAN_IF and WIFI_IF may be active at the same time
    NAS_EXISTS=false
    if [ -n "$ETH_LAN_IF" ] && test_connection $IP_NAS_ETH $ETH_LAN_IF; then
        NAS_EXISTS=true
        ENABLE_ETH=true
    fi
    if [ -n "$WIFI_IF" ] && test_connection $IP_NAS_ETH $WIFI_IF; then
        NAS_EXISTS=true
        ENABLE_WIFI=true
    fi

    if [ "$INTERNET_CONNECTION_METHOD" = "wireless" ]; then
        # List metered networks; everything else is unmetered
        case "$ACTIVE_SSID" in
            "MyMeteredNetwork")
                METERED_NETWORK=true
                ;;
            *)
                METERED_NETWORK=false
                ;;
        esac
    fi
}

# set metered flag based on active SSID
# Depends: nmcli installed
set_network_metering() {
    if $INTERNET_CONNECTED; then # TODO: May need to do: if [ -n "$ACTIVE_SSID" ]
        echo_blank -n "$_NETWORK Setting network metering..."
        if $METERED_NETWORK; then
            ismetered="yes"
        else
            ismetered="no"
        fi
        nmcli connection modify $ACTIVE_SSID connection.metered $ismetered
        echo_success -e "$_NETWORK Set network metering"
    else
        echo_skip "$_NETWORK Network metering not set... we are offline"
    fi
    unset ismetered
}

# Determine if the active internet connection is metered
# TODO: Improve this before use
#network_is_metered() {
#    status=$(nmcli -f connection.metered connection show $ACTIVE_SSID)
#    case ${status##* } in
#        "unknown")
#            METERED_NETWORK=false
#            unset status
#            return $FAILURE # TODO: Revise this
#            ;;
#        "yes")
#            METERED_NETWORK=true
#            unset status
#            return $SUCCESS # TODO: Revise this
#            ;;
#        "no")
#            METERED_NETWORK=false
#            unset status
#            return $FAILURE # TODO: Revise this
#            ;;
#    esac
#}
