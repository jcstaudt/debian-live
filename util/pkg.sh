# Read entries from cfg/pkg.csv and return a package list matching a query
# Usage get_pkglist_from_csv <opt: /path/to/csv>
# Depends: util/fs.sh - check_substring_exists
get_pkglist_from_csv() {
    # set a default path
    local path=$REPOROOT/cfg/pkg.csv

    local list=()

    # Parse input arguments
    for i in "$@"; do
        case $i in
          --action=*)
              # install|uninstall
              local wantaction="${i#*=}"
              shift # past argument=value
              ;;
          --arch=*)
              # target architecture (likely to be current architecture)
              local wantarch="${i#*=}"
              shift # past argument=value
              ;;
          --category=*)
              # general category of package e.g. gaming|office|development, etc.
              local wantcategory="${i#*=}"
              shift # past argument=value
              ;;
          --csv=*)
              # path of input csv file
              local path="${i#*=}"
              shift # past argument=value
              ;;
          --exclcategory=*)
              # general category to ignore
              local exclcategory="${i#*=}"
              shift # past argument=value
              ;;
          --graphics=*)
              # cli|gui
              local wantgraphics="${i#*=}"
              shift # past argument=value
              ;;
          --platform=*)
              # desktop|laptop|server
              local wantplatform="${i#*=}"
              shift # past argument=value
              ;;
          *)
              echo "Unsupported input argument \"$i\" into get_pkglist_from_csv"
              return
              ;;
        esac
    done

    if [ ! -f "$path" ]; then echo "ERROR: CSV file not found: \"$path\"" && return; fi

    # read the csv file but skip the header
    while IFS="," read -r pkgname platform graphics category arch action comment
    do
        # skip empty required fields
        if [ -z "$pkgname" ]; then continue; fi

        # assume desired arch is current arch
        # support multiple architectures with the same dataset
        # if ARCH exists within arch, or is empty, assign it
        # if the arch does not match, skip it
        if [ -n "$wantarch"     ] && ! check_substring_exists "$arch"     "$wantarch"    ; then continue; fi
        if [ -n "$wantcategory" ] && ! check_substring_exists "$category" "$wantcategory"; then continue; fi
        if [ -n "$wantplatform" ] && ! check_substring_exists "$platform" "$wantplatform"; then continue; fi

        if [ -n "$exclcategory" ] && [ "$exclcategory" = "$category" ]; then continue; fi
        if [ -n "$wantaction"   ] && [ "$wantaction"   != "$action"   ]; then continue; fi
        if [ -n "$wantgraphics" ] && [ "$wantgraphics" != "$graphics" ]; then continue; fi

        list+=($pkgname) # could also just echo $pkgname, but I'm keeping it in a list for now in case it's needed

    done < <(tail -n +2 $path)

    for i in "${list[@]}"; do
        echo $i
    done
}
